#pragma once
#include "TrackDetector.h"
#include "Calibration.h"
#include "SelectImages.h"
#include "DistCal.h"

using std::vector;
using cv::Point2f;
using cv::Point3f;
using cv::Mat;
using cv::Size;

double computeReprojectionErr(const vector<vector<cv::Point3f> >& objectPoints,
	const vector<vector<cv::Point2f> >& imagePoints,
	const vector<Mat>& rvecs, const vector<Mat>& tvecs,
	const Mat& cameraMatrix, const Mat& distCoeffs)
{
	vector<cv::Point2f> imagePoints2;
	int i, totalPoints = 0;
	double totalErr = 0, err;	

	for (i = 0; i < (int)objectPoints.size(); ++i)
	{
		projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i], cameraMatrix,
			distCoeffs, imagePoints2);
		err = norm(imagePoints[i], imagePoints2, cv::NORM_L2);

		int n = (int)objectPoints[i].size();
		//perViewErrors[i] = (float)std::sqrt(err*err / n);
		totalErr += err*err;		
		totalPoints += n;
	}

	return std::sqrt(totalErr / totalPoints);
}

int calReprojErr(const Mat &cameraMatrix, const Mat &distCoeffs, const vector<IMGINFO>& imginfos, bool& ifuse)
{	
	float tcAvgErr = 0.0f, tcerr = 0.0f;
	std::size_t n = 0;
	for (vector<IMGINFO>::const_iterator it = imginfos.cbegin(); it != imginfos.cend(); ++it)
	{
		tcerr += it->rms4comp;
		//cout << it->rms4comp << endl;
		n += it->points4comp_img.size();
		//cout << it->points4comp_img.size() << endl;
	}
	tcAvgErr = sqrt((tcerr + 0.0f) / n);
	std::cout << "tcAvgErr:" << tcAvgErr << std::endl;

	vector<vector<cv::Point3f>> objectPoints;
	vector<vector<cv::Point2f>> pointsOnImg;
	vector<Mat> rvecs, tvecs;
	for (size_t i = 0; i < imginfos.size(); i++)
	{
		Mat rvec, tvec;
		vector<Point3f> objectPointsOneImg;
		for (auto it = imginfos[i].points4comp_templ.cbegin(); it != imginfos[i].points4comp_templ.cend(); ++it)
		{
			objectPointsOneImg.push_back(Point3f(it->x, it->y, 0.0f));
		}
		if (solvePnP(objectPointsOneImg, imginfos[i].points4comp_img, cameraMatrix, distCoeffs, rvec, tvec))
		{
			objectPoints.push_back(objectPointsOneImg);
			pointsOnImg.push_back(imginfos[i].points4comp_img);
			rvecs.push_back(rvec);
			tvecs.push_back(tvec);
		};
	}

	if (rvecs.size() < 1 || rvecs.size() != tvecs.size())
	{
		ifuse = false;
		return -1;
	};
	
	double totalAvgErr = computeReprojectionErr(objectPoints, pointsOnImg, rvecs, tvecs,
		cameraMatrix, distCoeffs);
	std::cout << "totalAvgErr:" << totalAvgErr << std::endl;
	ifuse = totalAvgErr < tcAvgErr ? true : false;
	return 0;
}

bool runCalibration(const int &flag, const Size &imageSize, Mat &cameraMatrix, Mat &distCoeffs, \
	const vector<IMGINFO>& imginfos	)
{
	cameraMatrix = Mat::eye(3, 3, CV_64F);
	if (flag & CV_CALIB_FIX_ASPECT_RATIO)
		cameraMatrix.at<double>(0, 0) = 1.0;

	distCoeffs = Mat::zeros(8, 1, CV_64F);

	vector<vector<Point3f>> objectPoints;
	vector<vector<Point2f>> imgPoints;
	for (size_t i = 0; i < std::min(imginfos.size(), CAL_TIF_COUNT); i++)
	{
		vector<Point3f> objectPointsOneImg;
		for (auto it = imginfos[i].points4cal_templ.cbegin(); it != imginfos[i].points4cal_templ.cend(); ++it)
		{
			objectPointsOneImg.push_back(Point3f(it->x, it->y, 0.0f));
		}
		objectPoints.push_back(objectPointsOneImg);
		imgPoints.push_back(imginfos[i].points4cal_img);
	}
		
	vector<Mat> rvecs, tvecs;
	//std::cout << "image size:" << imageSize.width << "\t" << imageSize.height << std::endl;
	double rms = calibrateCamera(objectPoints, imgPoints, imageSize, cameraMatrix,
		distCoeffs, rvecs, tvecs, flag);

	//cout << "rms:" << rms << endl;

	bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);
	//totalAvgErr = calReprojErr(cameraMatrix, distCoeffs, imginfos, rvecs, tvecs);

	return ok;	
}


int runCalibrationOnImgs(const vector<IMGINFO>& imginfos, const cv::Size& imgSize, Mat& cameraMatrix, Mat& distCoeffs)
{
	//At least should > 2 images for calibration
    if (imginfos.empty() || imginfos.size() < 2)
	{
		std::cout << "[Error:] runCalibration: There should be at least 2 images for calibration!" << std::endl;
		return -1;
	}

	int flag = CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 | CV_CALIB_FIX_K6;//| CV_CALIB_FIX_ASPECT_RATIO;
	//Mat cameraMatrix, distCoeffs;
	vector<Mat> rvecs, tvecs;
	vector<float> reprojErrs;
	//double totalAvgErr;
	std::cout << "Begin running calibration .............." << std::endl;
	//cout << "Begin running calibration .............." << endl;
	if (!runCalibration(flag, imgSize, cameraMatrix, distCoeffs, \
		imginfos))
	{
		std::cout << "[Error:] Calibration failed!" << std::endl;
		//cerr << "Calibration failed!" << endl;
		return -1;
	}
	std::cout<< "Calibration succeed!" << std::endl;
	//cout << cameraMatrix << "\n";
	//cout << distCoeffs << endl;	
	
	return 0;
}


int calDistortedDev(const Mat& cameraMatrix, const Mat& distCoeffs, const Size& imgSize, float& avg_dev)
{
	if (cameraMatrix.size() != cv::Size(3, 3) || distCoeffs.size() != cv::Size(1, 5))
	{
		std::cout << "[Error:] cameraMatrix size should be:3*3   distCoeffs size should be: 5*1" << std::endl;
		//std::cerr << "cameraMatrix size should be:3*3   distCoeffs size should be: 5*1" << std::endl;
		return -1;
	}
	double u0 = cameraMatrix.at<double>(0, 2), v0 = cameraMatrix.at<double>(1, 2);
	double fx = cameraMatrix.at<double>(0, 0), fy = cameraMatrix.at<double>(1, 1);

	double k1 = ((double*)distCoeffs.data)[0];
	double k2 = ((double*)distCoeffs.data)[1];
	double p1 = ((double*)distCoeffs.data)[2];
	double p2 = ((double*)distCoeffs.data)[3];
	double k3 = distCoeffs.cols + distCoeffs.rows - 1 >= 5 ? ((double*)distCoeffs.data)[4] : 0.;
	double k4 = distCoeffs.cols + distCoeffs.rows - 1 >= 8 ? ((double*)distCoeffs.data)[5] : 0.;
	double k5 = distCoeffs.cols + distCoeffs.rows - 1 >= 8 ? ((double*)distCoeffs.data)[6] : 0.;
	double k6 = distCoeffs.cols + distCoeffs.rows - 1 >= 8 ? ((double*)distCoeffs.data)[7] : 0.;

	vector<Point2f> corners;
	corners.push_back(Point2f(0.0f,0.0f));
	corners.push_back(Point2f(0.0f, static_cast<float>(imgSize.width - 1)));
	corners.push_back(Point2f(static_cast<float>(imgSize.height - 1), static_cast<float>(imgSize.width - 1)));
	corners.push_back(Point2f(static_cast<float>(imgSize.height - 1), 0.0f));
	auto dis = [](Point2f corner, Point2f dcorner)->float {return static_cast<float>(std::sqrt((corner.x - dcorner.x)*(corner.x - dcorner.x) + (corner.y - dcorner.y) *(corner.y - dcorner.y))); };
	//auto dis_center = [u0, v0](Point2f corner)->float {dis(corner, Point2f(u0,v0)); };
	
	avg_dev = 0.0f;
	for (vector<Point2f>::iterator it = corners.begin(); it != corners.end(); ++it)
	{
		double u = (it->x - u0) / fx, v = (it->y - v0) / fy;
		double u2 = u*u, v2 = v*v, r2 = u2 + v2, r4 = r2*r2, r6 = r4*r2, kr = (1 + k1*r2 + k2*r4 + k3*r6) / (1 + k4*r2 + k5*r4 + k6*r6);
		double ui = u*kr + p1 * 2 * u*v + p2*(r2 + 2 * u2), vi = v*kr + p1*(r2 + 2 * v2) + p2 * 2 * u*v;
		Point2f dcorner = Point2f(static_cast<float>(ui*fx + u0), static_cast<float>(vi*fy + v0));
		avg_dev += dis(*it, dcorner) / dis(*it, Point2f(u0, v0));
	}
	avg_dev /= corners.size();
	return 0;		
}

int writeIni(const std::string& fn, const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, const bool& ifuse, const float avg_dev)
{
	if (cameraMatrix.size() != cv::Size(3, 3) || distCoeffs.size() != cv::Size(1, 5))
	{
		std::cerr << "cameraMatrix size should be:3*3   distCoeffs size should be: 5*1" << std::endl;
		return -1;
	}
	std::ofstream fs(fn, std::ios::app);
	if (!fs)
	{
		std::cerr << "Write INI file " << fn << " error!" << std::endl;
		return -1;
	}

	fs << "[Distortion]\n";
	fs << "u0 = " << cameraMatrix.at<double>(0, 2) << "\n";
	fs << "v0 = " << cameraMatrix.at<double>(1, 2) << "\n";
	fs << "fx = " << cameraMatrix.at<double>(0, 0) << "\n";
	fs << "fy = " << cameraMatrix.at<double>(1, 1) << "\n";
	fs << "k1 = " << distCoeffs.at<double>(0, 0) << "\n";
	fs << "k2 = " << distCoeffs.at<double>(1, 0) << "\n";
	fs << "k3 = " << distCoeffs.at<double>(4, 0) << "\n";
	fs << "p1 = " << distCoeffs.at<double>(2, 0) << "\n";
	fs << "p2 = " << distCoeffs.at<double>(3, 0) << "\n";
	fs << "ifuse = " << ifuse << "\n";
	fs << ";avgDev = " << avg_dev * 1000 << "/1000\n" << std::endl;

	fs.close();
	return 0;
}
