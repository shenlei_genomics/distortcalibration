#include "SelectGoodPoints.h"
#include <iostream>
#include <time.h> 
#include <ostream>
//#include "../GetTrackCross/logLib.h"
//#define _DEBUG

using std::vector;
using cv::Point2f;
using cv::Point2i;
using cv::Point;
using cv::Mat;
using std::string;

float getThreshold(vector<float>& v, const float& thPercent)
{
	size_t thIdx = static_cast<size_t>((1 - thPercent)*v.size());
	nth_element(v.begin(), v.begin() + thIdx, v.end(), [](const float e1, const float e2)->bool {return e1 > e2; });	
	return v[thIdx];
}

void findPeakPtsVals(const cv::Mat& img, vector<PeakInfo>& peak, const PARAM& param)
{
	vector<float> focus;
	vector<float> peakInts;
	vector<PeakInfo> candidate;
	const int Y_UP_LIMIT = img.rows - 1
		, X_UP_LIMIT = img.cols - 1;
	for (int y = 1; y < Y_UP_LIMIT; ++y)
	{
		int x = 1;
		while (x < X_UP_LIMIT)
		{			
			if (judgePeakPoint(img, cv::Point(x, y)))
			{
				float s1 = img.at<float>(y - 1, x - 1) + img.at<float>(y - 1, x) + img.at<float>(y - 1, x + 1);
				float s2 = img.at<float>(y, x - 1) + img.at<float>(y, x) + img.at<float>(y, x + 1);
				float s3 = img.at<float>(y + 1, x - 1) + img.at<float>(y + 1, x) + img.at<float>(y + 1, x + 1);
				float pi = img.at<float>(y, x);				
				float f = pi / (s1 + s2 + s3);					
				focus.push_back((f));
				peakInts.push_back(pi);
				float cen_y = (s3 - s1) / (s1 + s2 + s3);
				if (cen_y > param.centroidUpTh || cen_y < param.centroidLowTh)
				{
					x += 2;
					continue;
				}
				float c1 = img.at<float>(y - 1, x - 1) + img.at<float>(y, x - 1) + img.at<float>(y + 1, x - 1);
				float c3 = img.at<float>(y - 1, x + 1) + img.at<float>(y, x + 1) + img.at<float>(y + 1, x + 1);
				float cen_x = (c3 - c1) / (s1 + s2 + s3);
				if (cen_x > param.centroidUpTh || cen_x < param.centroidLowTh)
				{
					x += 2;
					continue;
				}
				candidate.emplace_back(Point(x, y), f, pi, cen_x, cen_y);
				x += 2;
			}
			else
				x += 1;
					
		}
	}
	float focus_th = getThreshold(focus, param.focusScorePercentTh);
	float int_th = getThreshold(peakInts, param.intensPercentTh);
	peak.clear();	
#ifdef  _DEBUG
	std::sort(candidate.begin(), candidate.end(), [](const PeakInfo& p1, const PeakInfo& p2)->bool {return p1.intens > p2.intens; });
	std::sort(candidate.begin(), candidate.end(), [](const PeakInfo& p1, const PeakInfo& p2)->bool {return p1.focus > p2.focus; });
#endif //  _DEBUG

	for (vector<PeakInfo>::const_iterator it = candidate.cbegin(); it != candidate.cend(); ++it)
	{
		
		if (it->focus > focus_th && it->intens > int_th)
		{
			peak.push_back(*it);
		}
	}
#ifdef _DEBUG
	ofstream fs("./goodpoints.txt");
	if (fs)
	{
		for (vector<PeakInfo>::const_iterator it = peak.cbegin(); it != peak.cend(); ++it)
		{
			fs << it->loc.x <<"\t" << it->loc.y << "\n";			
		}
	}
	fs.close();
#endif
	
}


int findGoodPoints(const Mat& img, Mat& goodPointsMask, Mat(&centroidMat)[2], const PARAM& param)
{
	//clock_t cstart, cends;
	//cstart = clock();
	
	if (img.empty())
	{
		std::cout << "[Error]: image is empty." << std::endl;
		return -1;
	}
	Mat img_tmp;
	img.copyTo(img_tmp);
	img_tmp.convertTo(img_tmp, CV_32FC1);

	vector<PeakInfo> peak;
	findPeakPtsVals(img_tmp, peak, param);

	goodPointsMask = cv::Mat::zeros(img_tmp.size(), CV_8UC1);
	centroidMat[0] = cv::Mat::zeros(img_tmp.size(), CV_32FC1);
	centroidMat[1] = cv::Mat::zeros(img_tmp.size(), CV_32FC1);

	for (vector<PeakInfo>::const_iterator it = peak.cbegin(); it != peak.cend(); ++it)
	{
		goodPointsMask.at<unsigned char>(it->loc.y, it->loc.x) = 255;
		centroidMat[0].at<float>(it->loc.y, it->loc.x) = it->cen_x;
		centroidMat[1].at<float>(it->loc.y, it->loc.x) = it->cen_y;
	}

	//cends = clock();
	//CGI_INFO(CGI::OSIIP::gclog, "[Info]: findGoodPoints running time" << cends - cstart <<" ms.\n");
	//cout << "findGoodPoints running time" << cends - cstart << endl;
	return 0;
}