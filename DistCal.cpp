#pragma once
#include "DistCal.h"
#include "SelectImages.h"
#include "Calibration.h"

bool DistCal::initFlag = true;
cv::Size DistCal::imgSize;


int DistCal::init(const std::string &strDnbnumX, const std::string &strDnbnumY, const float pitchSize)
{
	if (DistCal::initFlag)
	{
		int ret = 0;				
		BlockDC::pitchSize = pitchSize;
		ret = BlockForAddress::initTemplate(strDnbnumX, strDnbnumY, pitchSize);
		getOutsideBlkIdx( BlockForAddress::getBlockrow(), BlockForAddress::getBlockcol());
		if (ret < 0)
		{
			std::cout << "[Error:] init block info error!" << std::endl;
			return ret;
		}		
		initFlag = false;
	}	
	return 0;
}

std::vector<std::size_t> DistCal::outsideBlkIndex;
void DistCal::getOutsideBlkIdx(const std::size_t BLOCKROW, const std::size_t BLOCKCOL)
{
	outsideBlkIndex.clear();
	//top line   
	for (size_t i = 0; i < BLOCKCOL; ++i)
	{
		outsideBlkIndex.push_back(i);
	}
	//left and right vertical lines 
	for (size_t i = 1; i < BLOCKROW - 1; i++)
	{
		outsideBlkIndex.push_back(i * BLOCKCOL);
		outsideBlkIndex.push_back(i * BLOCKCOL + BLOCKCOL - 1);
	}
	//bottom line
	for (size_t i = (BLOCKROW - 1)*BLOCKCOL; i < BLOCKROW *BLOCKCOL; i++)
	{
		outsideBlkIndex.push_back(i);
	}
}

std::vector<INFOLIST> DistCal::imginfolistVec;
int DistCal::getImgInfolist(const cv::Mat &img, const TCOutput &trackcross, const int camera_index)
{		
	IMGINFO info;
	int ret = getImgInfo(img, outsideBlkIndex, trackcross, info);
	if (ret < 0 )
	{
		std::cout << "[Error:] get image info failed!" << std::endl;
		return ret;
	}
	if (imginfolistVec.size() < camera_index + 1)
	{
		imginfolistVec.push_back(INFOLIST());
	}	
	Addlist(imginfolistVec[camera_index], info);
	return 0;
}

int DistCal::calDCMap(const std::string& fn, cv::Mat& cameraMatrix, cv::Mat& distCoeffs, bool& ifuse, float& avg_dev)
{
	try
	{	
		for (auto it = imginfolistVec.cbegin(); it != imginfolistVec.cend(); ++it)
		{	
			if (it->size() < 2)
			{
				continue;
			}
			int ret = runCalibrationOnImgs(*it, imgSize, cameraMatrix, distCoeffs);
			if (ret < 0)
			{
				return ret;
			}
			avg_dev = -1.0f;
			calDistortedDev(cameraMatrix, distCoeffs, imgSize, avg_dev);
			bool ifuse = false;
			calReprojErr(cameraMatrix, distCoeffs, *it, ifuse);		
			writeIni(fn, cameraMatrix, distCoeffs, ifuse, avg_dev);
		}
		return 0;
	}
	catch (...)
	{
		std::cout << "[Error:]" << __FILE__ << "\t"<<__FUNCTION__ << " failed in " << __LINE__ << std::endl;
	}
	return -1;	
}

void DistCal::clear()
{	
	imginfolistVec.clear();
}


