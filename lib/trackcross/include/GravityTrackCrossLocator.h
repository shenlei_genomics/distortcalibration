#ifndef GRAVITY_TRACKCROSS_LOCATOR
#define GRAVITY_TRACKCROSS_LOCATOR

#include "TrackCrossLocator.h"

class GravityTrackCrossLocator : public TrackCrossLocator
{
public:
	GravityTrackCrossLocator(const cv::Mat& img,
		const int cycle,
		const int channel,
		const int fov_row, 
		const int fov_col);
	GravityTrackCrossLocator(const cv::Mat& img,
		const int channel, 
		const int fov_row,
		const int fov_col);
	GravityTrackCrossLocator(const cv::Mat& img);
	GravityTrackCrossLocator();
	virtual ~GravityTrackCrossLocator();

	/*init image track cross offsets*/
	int initTrackOffsets(const std::vector<int>& dnb_numsX,
		const std::vector<int>& dnb_numsY);

	int initTrackOffsets(const std::vector<int>& dnb_numsX,
		const std::vector<int>& dnb_numsY, const float& pixel_per_dnb);

	/*image preprocessing*/
	int preprocess();

	/*locate track line roughly*/
	int locateTrack(const std::vector<float>& features, const std::pair<int, int>& pair_range, const int& flag, std::pair<int, int>& id_pos);

	/*locate first track cross*/
	int locateFirstTrackCross(const std::vector<float>& features_y,
		const std::vector<float>& features_x, TrackCross& track_cross);

	// add dynamic scan param
	int locateFirstTrackCross(const std::vector<float>& features_y,
		const std::vector<float>& features_x,
		const ScanParam& param,
		TrackCross& track_cross);

	/*deduce all track cross points*/
	int deduceAllTrackCross(const TrackCross& first_acc_point, std::vector<cv::Point>& coarse_points, std::pair<int, int>& track_num_pair);

	/*@overload, deduce all track cross points*/
	int deduceAllTrackCross(const TrackCross& first_acc_point, std::vector<TrackCross>& coarse_points, std::pair<int, int>& track_num_pair);

	/*track cross locating*/
	int locateTrackCrossInPixel(std::vector<cv::Point>& pts_pixel);
	int locateTrackCrossInPixel(std::vector<cv::Point>& pts_pixel, const float& pixel_per_dnb);
	//out put pts_pixel and row,col ids
	int locateTrackCrossInPixel(TCOutput& out, const float& pixel_per_dnb);
	int locateTrackCrossInPixelNew(TCOutput& out, const float& pixel_per_dnb);

	// add dynamic scan param
	int locateTrackCrossInPixel(TCOutput& out,
		const float& pixel_per_dnb,
		ScanParam& param);

	inline void locateTrackCrossSubpixel(const std::vector<cv::Point>& pts_pixel)
	{
		const uint SIZE = (int)pts_pixel.size();
		int ret, R;
		cv::Point2f pt_out;
		this->m_tc_pts.resize(SIZE);//clear before loading
		for (int i = 0; i < SIZE; ++i)
		{
			R = 90;
#ifdef USE_INTEGRAL
			ret = this->correctByLinefitInteg(pts_pixel[i], R, pt_out);
#else
			ret = Utils::correctByLinefit(m_img, pts_pixel[i], R, pt_out);
#endif
			if (ret == 0)
				this->m_tc_pts[i] = pt_out;
			else
				this->m_tc_pts[i] = (cv::Point2f)pts_pixel[i];
		}
	}

	inline int correctByLinefitInteg(const cv::Point& pt_in,
		int& R,
		cv::Point2f& pt_out)
	{
		std::pair<std::pair<float, float>, std::pair<float, float>> lines_params;
		int ret = Utils::getLineParamsInteg(this->m_img,
			this->m_integ, pt_in, R, lines_params);
		if (ret < 0) return ret;

		ret = Utils::getHVLineCross(lines_params.first.first, lines_params.second.first,
			lines_params.first.second, lines_params.second.second, pt_out);
		if (ret < 0) return ret;

		if (std::fabs(pt_out.x - pt_in.x) > 1.5f
			|| std::fabs(pt_out.y - pt_in.y) > 1.5f)
		{
			fprintf(stdout, "[Warning]: quit the sub_pixel correction, offset: [%.3f, %.3f].\n",
				float(pt_out.x - pt_in.x), float(pt_out.y - pt_in.y));
			return -1;
		}

		return 0;
	}

	inline int correctByLinefitInteg(const cv::Point& pt_in,
		const float& pixel_per_dnb,
		int& R,
		cv::Point2f& pt_out)
	{
		std::pair<std::pair<float, float>, std::pair<float, float>> lines_params;
		int ret = Utils::getLineParamsInteg(this->m_img,
			this->m_integ, pt_in, R, lines_params);
		if (ret < 0) 
			return ret;

		ret = Utils::getHVLineCross(lines_params.first.first, lines_params.second.first,
			lines_params.first.second, lines_params.second.second, pt_out);
		if (ret < 0) return ret;

		if (pixel_per_dnb < 3.0f)
		{
			if (std::abs(pt_out.x - pt_in.x) > 1.5f
				|| std::abs(pt_out.y - pt_in.y) > 1.5f)
			{
				printf("[Warning]: quit the sub_pixel correction.\n");
				return -1;
			}
		}
		else
		{
			if (std::abs(pt_out.x - pt_in.x) > 1.0f
				|| std::abs(pt_out.y - pt_in.y) > 1.0f)
			{
				printf("[Warning]: quit the sub_pixel correction.\n");
				return -1;
			}
		}
		//if (std::fabs(pt_out.x - pt_in.x) > 0.53f * pixel_per_dnb
		//	|| std::fabs(pt_out.y - pt_in.y) > 0.53f * pixel_per_dnb)
		//{
		//	CGI_DEBUG(CGI::OSIIP::gclog, "[Warning]: quit the sub_pixel correction.\n");
		//	return -1;
		//}
		return 0;
	}

	inline void locateTrackCrossSubpixel(const std::vector<cv::Point>& pts_pixel,
		const float& pixel_per_dnb)
	{
		const uint SIZE = (int)pts_pixel.size();
		int ret, R;
		cv::Point2f pt_out;
		this->m_tc_pts.resize(SIZE);//clear before loading
		for (int i = 0; i < SIZE; ++i)
		{
			R = 90;
			ret = this->correctByLinefitInteg(pts_pixel[i], pixel_per_dnb, R, pt_out);
			if (ret == 0) 
				this->m_tc_pts[i] = pt_out;
			else 
				this->m_tc_pts[i] = (cv::Point2f)pts_pixel[i];
		}
	}

	/*locate track crosses in sub_pixel SIMD*/
	//inline int locateTCsSubpixelSIMD(const std::vector<cv::Point>& pts_pixel)
	//{
	//	const int size = (int)pts_pixel.size();
	//	this->m_tc_pts.resize(size);

	//	float arr_k1[81], arr_k2[81], arr_b1[81], arr_b2[81], arr_x[81], arr_y[81];

	//	/*get fitted line parameters*/
	//	Utils::getLinesParameterArray(m_img, m_integ, pts_pixel,
	//		arr_k1, arr_k2, arr_b1, arr_b2);

	//	/*get track crosses*/
	//	Utils::getHVLineCrossSSE(arr_k1, arr_k2, arr_b1, arr_b2, size, arr_x, arr_y);
	//	//Utils::getHVLineCrossAVX(arr_k1, arr_k2, arr_b1, arr_b2, size, arr_x, arr_y);
	//	for (int i = 0; i < size; ++i)
	//	{
	//		this->m_tc_pts[i] = cv::Point2f(arr_x[i], arr_y[i]);
	//	}

	//	return 0;
	//}

	/*processing 3*3 DNB*/
	int locateTrackCrossInSubpixel_3_3(const std::vector<cv::Point>& pts_pixel);

private:
	/*members beyond TrackCrossLocator*/
	bool m_is_preprocessed;
	float m_track_offsets_y[9];//track line offsets
	float m_track_offsets_x[9];
	std::vector<int> m_row_track_ids;
	std::vector<int> m_col_track_ids;

	friend class Utils;
	friend class UnitTester;
	friend class CoordExtractor;
	friend class Corrector;
};

#endif
