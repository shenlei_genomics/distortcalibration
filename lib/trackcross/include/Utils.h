#ifndef UTILS
#define UTILS

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/calib3d.hpp>
//#include <opencv2/contrib/contrib.hpp>
#include "type.h"

#define GET_ARRAY_LEN(arr, len) {len = (sizeof(arr) / sizeof(arr[0]));}

//V0.1 inside block grid vector
static const uint GRID_VECT_IN_X_V01[8] = { 76, 96, 110, 120, 120, 110, 96, 76 };
static const uint GRID_VECT_IN_Y_V01[8] = { 76, 96, 110, 120, 120, 110, 96, 76 };
static const uint GRID_VECT_ALL_X_V01[10] = { 90, 76, 96, 110, 120, 120, 110, 96, 76, 89 };
static const uint GRID_VECT_ALL_Y_V01[10] = { 90, 76, 96, 110, 120, 120, 110, 96, 76, 89 };

//V1 inside block grid vector
static const uint GRID_VECT_IN_X_V1[8] = { 65, 82, 95, 105, 105, 95, 82, 65 };
static const uint GRID_VECT_IN_Y_V1[8] = { 55, 63, 77, 90, 90, 77, 63, 55 };
static const uint GRID_VECT_ALL_X_V1[10] = { 82, 65, 82, 95, 105, 105, 95, 82, 65, 81 };
static const uint GRID_VECT_ALL_Y_V1[10] = { 77, 55, 63, 77, 90, 90, 77, 63, 55, 76 };

//V2, 800 nm inside block vector
//static const uint GRID_VECT[8] = { 68, 102, 119, 136, 136, 119, 102, 68 };

//inside track cross ids for total 121
static const uint INSIDE_ID[81] = { 12, 13, 14, 15, 16, 17, 18, 19, 20,
                                    23, 24, 25, 26, 27, 28, 29, 30, 31,
									34, 35, 36, 37, 38, 39, 40, 41, 42,
									45, 46, 47, 48, 49, 50, 51, 52, 53,
									56, 57, 58, 59, 60, 61, 62, 63, 64,
									67, 68, 69, 70, 71, 72, 73, 74, 75,
									78, 79, 80, 81, 82, 83, 84, 85, 86,
									89, 90, 91, 92, 93, 94, 95, 96, 97,
									100, 101, 102, 103, 104, 105, 106, 107, 108 };

/*track cross struct*/
struct TrackCross
{
	cv::Point point;
	int row_id, col_id;
};

/*TrackDetector output*/
struct TCOutput
{
	std::vector<cv::Point> m_tracks_pixel;
	std::vector<cv::Point2f> m_tracks_subpixel;
	std::vector<int> m_track_ids_y;
	std::vector<int> m_track_ids_x;
	TrackCross m_first_tc;
	uint m_bad_num;
};

struct ScanParam
{
	int m_y_begin;
	int m_x_begin;
	int m_y_end;
	int m_x_end;
	int m_feat_left;
	int m_feat_right;
	int m_feat_up;
	int m_feat_down;
	//uint m_bad_num;
	TrackCross m_first_tc;
	std::pair<int, int> m_allowance;
	bool m_is_init;
};

struct PtDistComparator
{
	bool operator()(const std::pair<float, cv::Point2f>& p1, const std::pair<float, cv::Point2f>& p2)
	{
		return p2.first < p1.first;//in the order of smallest to largest in size
	}
};

template<typename T>
struct ComparatorL2S
{
	inline const bool operator()(const T& val_1, const T& val_2) const
	{
		return val_2 < val_1;//from large to small
	}
};

template<typename T>
struct ComparatorS2L
{
	inline const bool operator()(const T& val_1, const T& val_2) const
	{
		return val_2 > val_1;//from small to large
	}
};

struct PtIntensityComparator
{
public:
	bool operator()(const cv::Point& pt_1, const cv::Point& pt_2)
	{
		return m_img.at<float>(pt_1.y, pt_1.x) < m_img.at<float>(pt_2.y, pt_2.x);
	}
protected:
	cv::Mat m_img;//presume that m_img.type() == CV_32F

	friend class Utils;
	friend class TrackCrossTester;
	friend class GoodPointSelector;
};

/*stores block_id, DNBs and good points*/
struct Block
{
	uint m_id;
	std::pair<uint, uint> m_grid;
	std::vector<cv::Point2f> m_vertices;
	std::vector<cv::Point2f> m_dnbs;
	std::vector<cv::Point2f> m_good_pts;
	Block() { m_vertices.resize(4); }
};

struct Block2D
{
	uint m_id;
	std::pair<uint, uint> m_grid;
	std::vector<cv::Point2f> m_vertices;
	std::vector<std::vector<cv::Point2f>> m_dnbs2d;
	std::vector<cv::Point2f> m_good_pts;
	Block2D() { m_vertices.resize(4); }
};

struct GoodPtParam 
{
	float m_bg_ratio;
	float m_th_focus;
	float m_th_centrality;

	GoodPtParam(){}
	//GoodPtParam(const float& bg_ratio, const float& th_focus, const float& th_centrality)
	//	: m_bg_ratio(bg_ratio)
	//	, m_th_focus(th_focus)
	//	, m_th_centrality(th_centrality)
	//{}

	/*init*/
	void init(const float& bg_ratio, const float& th_focus, const float& th_centrality)
	{
		m_bg_ratio = bg_ratio;
		m_th_focus = th_focus;
		m_th_centrality = th_centrality;
	}
};

class Utils
{
public:
	/*get img features*/
	static int getFeatures(const cv::Mat& img,
		const cv::Point2f& pt_in,
		std::vector<int>& row_features,
		std::vector<int>& col_features,
		const int R = 50,
		const int L = 100);
	static int getFeaturesInteg(const cv::Mat& integ,
		const cv::Point2f& pt_in,
		std::vector<int>& features_y,
		std::vector<int>& features_x,
		const int width,
		const int height,
		const int R = 50,
		const int L = 100);

	static int getFeatures(const cv::Mat& img, const cv::Range& x_range,
		const cv::Range& y_range, std::vector<int>& y_features, std::vector<int>& x_features);

	/*get features by integral*/
	static inline void getFeaturesInteg(const cv::Mat& integ,
		const cv::Range& x_range,
		const cv::Range& y_range,
		std::vector<float>& features_y,
		std::vector<float>& features_x)
	{
		features_y.resize(y_range.end);
		features_x.resize(x_range.end);
		for (int y = y_range.start, x = x_range.start;
			y < y_range.end && x < x_range.end; ++y, ++x)
		{
			features_y[y] = Utils::getRoiSum(integ,
				cv::Range(Utils::colLeft, Utils::colRight), cv::Range(y, y + 1));
			features_x[x] = Utils::getRoiSum(integ, cv::Range(x, x + 1),
				cv::Range(Utils::rowUp, Utils::rowDown));
		}
	}

	// using dynamic scan parameters
	inline static void getFeaturesInteg(const cv::Mat& integ,
		const cv::Range& x_range,
		const cv::Range& y_range,
		const ScanParam& param,
		std::vector<float>& features_y,
		std::vector<float>& features_x)
	{
		features_y.resize(y_range.end, FLT_MAX);
		features_x.resize(x_range.end, FLT_MAX);
		for (int y = y_range.start, x = x_range.start;
		y < y_range.end && x < x_range.end; ++y, ++x)
		{
			features_y[y] = Utils::getRoiSum(integ,
				std::move(cv::Range(param.m_feat_left, param.m_feat_right)),
				std::move(cv::Range(y, y + 1)));
			features_x[x] = Utils::getRoiSum(integ,
				std::move(cv::Range(x, x + 1)),
				std::move(cv::Range(param.m_feat_up, param.m_feat_down)));
		}
	}

	static int getFeaturesPtr(const float* data, const cv::Point& pt_start, const cv::Point& pt_end, const std::pair<int, int>& width_height, std::vector<int>& row_features, std::vector<int>& col_features);

	/*get origin image filtered features*/
	static int getFilterFeatures(const cv::Mat& img, const cv::Point2f& pt_in, std::vector<int>& row_features, std::vector<int>& col_features, int R = 50, int L = 100);

	static int getColsFeatures(const cv::Mat& img, const cv::Range& col_range, std::vector<int>& col_features);

	static int getRowsFeatures(const cv::Mat& img, const cv::Range& row_range, std::vector<int>& row_features);

	static int getRowFiterFearure(cv::Mat& img, const int row, const std::pair<int, int>& center_radius, int row_feature);
	static int getColFiterFearure(cv::Mat& img, const int col, const std::pair<int, int>& center_radius, int col_feature);

	/*correct track cross in pixel*/
	static int correctTCPixel(const cv::Mat& img, const cv::Point& pt_in, cv::Point& pt_out, const int R = 50, const int L = 100);
	static int correctPointXOrY(const cv::Mat& img, const cv::Point& pt_in, const int flag, const int R, const int L, cv::Point& pt_out);

	/*correct track cross in pixel by integral*/
	static int correctTCPixelInteg(const cv::Mat& integ,
		const cv::Point& pt_in, 
		cv::Point& pt_out,
		const int width,
		const int height,
		const float& pixel_per_dnb,
		const int R = 45,         
		const int L = 75);
	static int correctTCPixelInteg(const cv::Mat& integ,
		const cv::Point& pt_in,
		cv::Point& pt_out,
		const int width,
		const int height,
		const int R = 45,
		const int L = 75);

	/*correct track cross in pixel further by statistics*/
	static int correctPointStatistics(const cv::Mat& img, const cv::Point& pt_in, cv::Point& pt_out, const int offset = 1, int dnb_num = 25);

	/*scan filter and sum*/
	static int filterAndCount(const cv::Mat& img_in, const cv::Point& pt_in, const int dnb_num, const int threshold, const int flag, int& count);

	/*get offset by statistics. When DNB_SIZE_IN_PIXEL is 3*/
	static int getOffsetByStatistics(const std::vector<float>& row_features,
		const std::vector<float>& col_features, cv::Point& pt_offset);

	/*get gravity*/
	static int calculateGravity(const std::vector<int>& row_features, const std::vector<int>& col_features, const int& pos_flag, cv::Point2f& pt_gravity);

	/*correct track cross in sub_pixel*/
	static int correctPointSubpixel(const cv::Mat& img, const cv::Point& pt_in, cv::Point2f& pt_out, const int& R = 14, const int& L = 14);
	static int correctPointSubpixelWithFlag(const cv::Mat& img, const cv::Point& pt_in, cv::Point2f& pt_out, const int flag, const int& R = 14, const int& L = 14);

	/*
	* extract DNBs from track crosses
	* for V1 all blocks
	*/
	inline static int extractDNBs(const std::vector<cv::Point2f>& tracks,
		const std::vector<int>& dnb_vectX,
		const std::vector<int>& dnb_vectY, std::vector<cv::Point2f>& dnbs)
	{
		if (tracks.size() != 121 || dnb_vectX.size() != 10 || dnb_vectY.size() != 10)
		{
			printf("[Error]: input parameters are not correct.\n");
			return -1;
		}

		dnbs.clear();
		std::vector<cv::Point2f> vertices(4);
		for (uint i = 0; i < 100; ++i)
		{
			uint row = i / 10;
			uint col = i % 10;

			vertices[0] = tracks[row * 11 + col];
			vertices[1] = tracks[row * 11 + (col + 1)];
			vertices[2] = tracks[(row + 1) * 11 + (col + 1)];
			vertices[3] = tracks[(row + 1) * 11 + col];

			const uint DNB_NUM_Y = dnb_vectY[row];
			const uint DNB_NUM_X = dnb_vectX[col];
			const uint Y_LIMIT = DNB_NUM_Y - 1;
			const uint X_LIMIT = DNB_NUM_X - 1;
			cv::Point2f left, right;
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnbs.push_back(float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right);
				}
			}
		}
		return 0;
	}

	/*calculate point offset*/
	inline static void getPointOffset(const cv::Point2f pt_in,
		const cv::Point2f& reference, cv::Point2f& offset)
	{
		offset = pt_in - reference;
	}

	/*
	* get template temporary grid vector: 2 color
	* 9*9 track lines
	*/
	inline static void getDNBVector(const std::vector<int>& vect_in,
		std::vector<int>& vect_out)
	{//presume: vect_in.size() == 9
		vect_out.resize(10);
		memcpy(&vect_out[1], &vect_in[0], (sizeof(int) << 3));
		vect_out[0] = (vect_in[8] >> 1) + 2;
		vect_out[9] = (vect_in[8] >> 1) + 1;
	}

	/*
	* get template temporary grid vector: 4 color
	* 9*9 track lines
	*/
	inline static void getDNBVector
		(const std::vector<int>& grids_x,
			const std::vector<int>& grids_y,
			std::vector<int>& dnb_vectX,
			std::vector<int>& dnb_vectY)
	{
		dnb_vectX.resize(10);
		dnb_vectY.resize(10);
		memcpy(&dnb_vectX[1], &grids_x[0], (sizeof(int) << 3));
		memcpy(&dnb_vectY[1], &grids_y[0], (sizeof(int) << 3));
		dnb_vectX[0] = (grids_x[8] >> 1) + 2;
		dnb_vectY[0] = (grids_y[8] >> 1) + 2;
		dnb_vectX[9] = (grids_x[8] >> 1) + 1;
		dnb_vectY[9] = (grids_y[8] >> 1) + 1;
	}

	inline static void getDNBXYNUM(const std::vector<int>& dnb_vect_x,
		const std::vector<int>& dnb_vect_y,
		std::pair<int, int>& xy_num)
	{
		xy_num.first = 0;
		xy_num.second = 0;
		const uint SIZE_Y = (uint)dnb_vect_y.size();
		const uint SIZE_X = (uint)dnb_vect_x.size();
		for (uint y = 0; y < SIZE_Y; ++y)
			xy_num.second += dnb_vect_y[y] - 3;
		for (uint x = 0; x < SIZE_X; ++x)
			xy_num.first += dnb_vect_x[x] - 3;
	}

	inline static bool judgeDNBPt(const cv::Mat& img,
		const cv::Point& pt)
	{
		const auto& x = pt.x;
		const auto& y = pt.y;
		if (x - 1 < 0 || x + 2 > img.cols
			|| y - 1 < 0 || y + 2 > img.rows) return false;
		if (img.at<float>(pt) < img.at<float>(y - 1, x)
			|| img.at<float>(pt) < img.at<float>(y + 1, x)
			|| img.at<float>(pt) < img.at<float>(y, x - 1)
			|| img.at<float>(pt) < img.at<float>(y, x + 1))
		{
			return false;
		}
		return true;
	}

	// get track area background uisng Even's algorithm
	inline static float getTrackAreaBkg(const cv::Mat& img,
		const cv::Point2f& pt,
		const float ratio,
		const uint R)
	{
		// determine boundary
		int X = cvRound(pt.x);
		int Y = cvRound(pt.y);
		X = X >= 0 ? X : 0;
		X = X < img.cols ? X : img.cols - 1;
		Y = Y >= 0 ? Y : 0;
		Y = Y < img.rows ? Y : img.rows - 1;

		int start_x_h = X - R;
		int end_x_h = X + R;
		int start_y_h = Y - 1;
		int end_y_h = Y + 1;
		start_x_h = start_x_h >= 0 ? start_x_h : 0;
		start_y_h = start_y_h >= 0 ? start_y_h : 0;
		end_x_h = end_x_h < img.cols ? end_x_h : img.cols - 1;
		end_y_h = end_y_h < img.rows ? end_y_h : img.rows - 1;

		int start_x_v = X - 1;
		int end_x_v = X + 1;
		int start_y_v = Y - R;
		int end_y_v = Y + R;
		start_x_v = start_x_v >= 0 ? start_x_v : 0;
		start_y_v = start_y_v >= 0 ? start_y_v : 0;
		end_x_v = end_x_v < img.cols ? end_x_v : img.cols - 1;
		end_y_v = end_y_v < img.rows ? end_y_v : img.rows - 1;
		const uint SIZE_H = (end_y_h - start_y_h + 1) * (end_x_h - start_x_h + 1);
		const uint SIZE_V = (end_y_v - start_y_v + 1) * (end_x_v - start_x_v + 1);

		// fill with filtered points
		std::vector<cv::Point> pts_h(SIZE_H), pts_v(SIZE_V);
		uint k = 0;
		for (int y = start_y_h; y <= end_y_h; ++y)
		{
			for (int x = start_x_h; x <= end_x_h; ++x)
				pts_h[k++] = std::move(cv::Point(x, y));
		}
		k = 0;
		for (int x = start_x_v; x <= end_x_v; ++x)
		{
			for (int y = start_y_v; y <= end_y_v; ++y)
				pts_v[k++] = std::move(cv::Point(x, y));
		}

		// get dnb cell pts
		std::vector<cv::Point> pts_dnb;
		for (const auto& pt : pts_h)
		{
			if (Utils::judgeDNBPt(img, pt))
			{
				const int the_x = pt.x;
				const int the_y = pt.y;
				for (uint i = 0; i < 3; ++i)
				{
					for (uint j = 0; j < 3; ++j)
					{
						auto the_pt = cv::Point(the_x - 1 + j, the_y - 1 + i);
						if (the_pt.x >= start_x_h && the_pt.x <= end_x_h
							&& the_pt.y >= start_y_h && the_pt.y <= end_y_h)
						{
							pts_dnb.push_back(the_pt);
						}
					}
				}
			}
		}
		for (const auto& pt : pts_v)
		{
			if (Utils::judgeDNBPt(img, pt))
			{
				const int the_x = pt.x;
				const int the_y = pt.y;
				for (uint i = 0; i < 3; ++i)
				{
					for (uint j = 0; j < 3; ++j)
					{
						auto the_pt = cv::Point(the_x - 1 + j, the_y - 1 + i);
						if (the_pt.x >= start_x_v && the_pt.x <= end_x_v
							&& the_pt.y >= start_y_v && the_pt.y <= end_y_v)
						{
							pts_dnb.push_back(the_pt);
						}
					}
				}
			}
		}

		// concatenate h and v pts
		std::vector<cv::Point> pts(SIZE_H + SIZE_V);
		std::copy(pts_h.begin(), pts_h.end(), pts.begin());
		std::copy(pts_v.begin(), pts_v.end(), pts.begin() + SIZE_H);

		// get difference set 
		std::vector<float> ints;
		for (const auto& pt : pts)
		{
			auto it = std::find(pts_dnb.begin(), pts_dnb.end(), pt);
			if (it != pts_dnb.end())
				continue; // if found
			ints.push_back(img.at<float>(pt));
		}

		// get bkg from sorted bkg ints
		std::sort(ints.begin(), ints.end(), std::less<float>());
		return ints[cvRound(ratio * ints.size())];
	}

	/*
	* get DNB background of given block
	* Note: make sure img.type() == CV_32F
	*/
	inline static void getBlockDNBBkg(const uint BLK_ID,
		const cv::Mat& img,
		const std::vector<cv::Point2f>& TCs,
		const std::vector<int>& dnb_vect_x,
		const std::vector<int>& dnb_vect_y,
		std::vector<std::vector<float>>& blk_bkg)
	{
		// get block's corners, width and height
		const int X = int(BLK_ID % 10);
		const int Y = int(BLK_ID / 10);
		cv::Point2f vertices[4];
		vertices[0] = TCs[Y * 11 + X];
		vertices[1] = TCs[Y * 11 + (X + 1)];
		vertices[2] = TCs[(Y + 1) * 11 + (X + 1)];
		vertices[3] = TCs[(Y + 1) * 11 + X];
		const auto BLK_WID = (vertices[1].x - vertices[0].x + vertices[2].x - vertices[3].x) * 0.5f;
		const auto BLK_HEI = (vertices[3].y - vertices[0].y + vertices[2].y - vertices[1].y) * 0.5f;

		// get 4 backgrounds of 4 track cross area
		float bkgs[4];
		bkgs[0] = Utils::getTrackAreaBkg(img, vertices[0], 0.5f, 90);
		bkgs[1] = Utils::getTrackAreaBkg(img, vertices[1], 0.5f, 90);
		bkgs[2] = Utils::getTrackAreaBkg(img, vertices[2], 0.5f, 90);
		bkgs[3] = Utils::getTrackAreaBkg(img, vertices[3], 0.5f, 90);

		// prepare and allocate
		const uint DNB_NUM_Y = dnb_vect_y[Y];
		const uint DNB_NUM_X = dnb_vect_x[X];
		const uint Y_LIMIT = DNB_NUM_Y - 1;
		const uint X_LIMIT = DNB_NUM_X - 1;
		blk_bkg.resize(DNB_NUM_Y - 3);
		for (uint i = 0; i < blk_bkg.size(); ++i)
			blk_bkg[i].resize(DNB_NUM_X - 3);

		// calculate each DNB's background of gicen block
		cv::Point2f left, right, dnb;
		if (X > 0 && X < 9 && Y > 0 && Y < 9) // inside block
		{
			float u, v;
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					u = (dnb.x - (vertices[0].x + vertices[3].x) * 0.5f) / BLK_WID * 0.5f;
					v = (dnb.y - (vertices[0].y + vertices[1].y) * 0.5f) / BLK_HEI * 0.5f;
					blk_bkg[y - 2][x - 2] = (1 - u) * (1 - v) * bkgs[0] + (1 - u) * v * bkgs[3] \
						+ u * (1 - v) * bkgs[1] + u * v * bkgs[2];
				}
			}
		}
		else if (Y == 0 && X > 0 && X < 9) // up border
		{
			float dist_3, dist_2, ratio;
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					dist_3 = std::sqrt((dnb.x - vertices[3].x) * (dnb.x - vertices[3].x)
						+ (dnb.y - vertices[3].y) * (dnb.y - vertices[3].y));
					dist_2 = std::sqrt((dnb.x - vertices[2].x) * (dnb.x - vertices[2].x)
						+ (dnb.y - vertices[2].y) * (dnb.y - vertices[2].y));
					ratio = dist_3 / (dist_3 + dist_2);
					blk_bkg[y - 2][x - 2] = (1.0f - ratio) * bkgs[3] + ratio * bkgs[2];
				}
			}
		}
		else if (Y == 9 && X > 0 && X < 9) // down border
		{
			float dist_0, dist_1, ratio;
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					dist_0 = std::sqrt((dnb.x - vertices[0].x) * (dnb.x - vertices[0].x)
						+ (dnb.y - vertices[0].y) * (dnb.y - vertices[0].y));
					dist_1 = std::sqrt((dnb.x - vertices[1].x) * (dnb.x - vertices[1].x)
						+ (dnb.y - vertices[1].y) * (dnb.y - vertices[1].y));
					ratio = dist_0 / (dist_0 + dist_1);
					blk_bkg[y - 2][x - 2] = (1.0f - ratio) * bkgs[0] + ratio * bkgs[1];
				}
			}
		}
		else if (X == 0 && Y > 0 && Y < 9) // left border
		{
			float dist_1, dist_2, ratio;
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					dist_1 = std::sqrt((dnb.x - vertices[1].x) * (dnb.x - vertices[1].x)
						+ (dnb.y - vertices[1].y) * (dnb.y - vertices[1].y));
					dist_2 = std::sqrt((dnb.x - vertices[2].x) * (dnb.x - vertices[2].x)
						+ (dnb.y - vertices[2].y) * (dnb.y - vertices[2].y));
					ratio = dist_1 / (dist_1 + dist_2);
					blk_bkg[y - 2][x - 2] = (1.0f - ratio) * bkgs[1] + ratio * bkgs[2];
				}
			}
		}
		else if (X == 9 && Y > 0 && Y < 9) // right border
		{
			float dist_0, dist_3, ratio;
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					dist_0 = std::sqrt((dnb.x - vertices[0].x) * (dnb.x - vertices[0].x)
						+ (dnb.y - vertices[0].y) * (dnb.y - vertices[0].y));
					dist_3 = std::sqrt((dnb.x - vertices[3].x) * (dnb.x - vertices[3].x)
						+ (dnb.y - vertices[3].y) * (dnb.y - vertices[3].y));
					ratio = dist_0 / (dist_0 + dist_3);
					blk_bkg[y - 2][x - 2] = (1.0f - ratio) * bkgs[0] + ratio * bkgs[3];
				}
			}
		}
		else if (BLK_ID == 0) // leftup corner(0)
		{
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					blk_bkg[y - 2][x - 2] = bkgs[2];
				}
			}
		}
		else if (BLK_ID == 9) // rightup corner(1)
		{
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					blk_bkg[y - 2][x - 2] = bkgs[3];
				}
			}
		}
		else if (BLK_ID == 99) // rightdown corner(2)
		{
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					blk_bkg[y - 2][x - 2] = bkgs[0];
				}
			}
		}
		else if (BLK_ID == 90) // leftdown corner(3)
		{
			for (uint y = 2; y < Y_LIMIT; ++y)
			{
				left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
					+ (float)y / DNB_NUM_Y * vertices[3];
				right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
					+ (float)y / DNB_NUM_Y * vertices[2];
				for (uint x = 2; x < X_LIMIT; ++x)
				{
					dnb = float(DNB_NUM_X - x) / DNB_NUM_X * left
						+ (float)x / DNB_NUM_X * right;
					blk_bkg[y - 2][x - 2] = bkgs[1];
				}
			}
		}
	}

	/*get arbitrary template inside DNB vector*/
	inline static void getInsideDNBVect(const std::vector<int>& dnb_num_x,
		const std::vector<int>& dnb_num_y, const std::vector<int>& track_ids_x,
		const std::vector<int>& track_ids_y,
		std::vector<int>& dnb_vect_x, std::vector<int>& dnb_vect_y)
	{
		const uint SIZE_Y = (uint)track_ids_y.size() - 1;
		dnb_vect_y.resize(SIZE_Y);
		const uint SIZE_X = (uint)track_ids_x.size() - 1;
		dnb_vect_x.resize(SIZE_X);
		for (uint y = 0; y < SIZE_Y; ++y)
		{
			if (track_ids_y[y])// != 0
				dnb_vect_y[y] = dnb_num_y[track_ids_y[y] - 1];
			else
				dnb_vect_y[y] = dnb_num_y[dnb_num_y.size() - 1];
		}
		for (uint x = 0; x < SIZE_X; ++x)
		{
			if (track_ids_x[x])// != 0
				dnb_vect_x[x] = dnb_num_x[track_ids_x[x] - 1];
			else
				dnb_vect_x[x] = dnb_num_x[dnb_num_x.size() - 1];
		}
	}

	/*get template all DNB number using DNB vector*/
	inline static size_t getAllDNBNumByDNBVect(const std::vector<int>& dnb_vect)
	{
		if (dnb_vect.size() != 10)
		{
			printf("[Error]: Input dnb vector is not correct.\n");
			return 0;
		}
		size_t dnb_sum = 0;
		for (uint i = 0; i < 10; ++i)
		{
			for (uint j = 0; j < 10; ++j)
			{
				dnb_sum += (dnb_vect[i] - 3) * (dnb_vect[j] - 3);
			}
		}
		return dnb_sum;
	}

	/*get template all DNB number*/
	inline static size_t getAllDNBNum(const std::vector<int>& grid_vect)
	{
		if (grid_vect.size() != 9)
		{
			printf("Input grid vector is not correct.\n");
			return 0;
		}
		std::vector<int> dnb_vect;
		Utils::getDNBVector(grid_vect, dnb_vect);
		size_t dnb_sum = 0;
		for (uint i = 0; i < 10; ++i)
		{
			for (uint j = 0; j < 10; ++j)
			{
				dnb_sum += (dnb_vect[i] - 3) * (dnb_vect[j] - 3);
			}
		}
		return dnb_sum;
	}

	/*get template inside DNB number*/
	inline static size_t getInsideDNBNum(const std::vector<int>& grid_vect)
	{
		if (grid_vect.size() != 9)
		{
			printf("Input grid vector is not correct.\n");
			return 0;
		}
		std::vector<int> dnb_vect;
		Utils::getDNBVector(grid_vect, dnb_vect);
		size_t dnb_sum = 0;
		for (uint i = 1; i < 9; ++i)
		{
			for (uint j = 1; j < 9; ++j)
			{
				dnb_sum += (dnb_vect[i] - 3) * (dnb_vect[j] - 3);
			}
		}
		return dnb_sum;
	}

	/*get template inside DNB number using DNB vector*/
	inline static size_t getInsideDNBNumByDNBVector(const std::vector<int>& dnb_vect)
	{
		if (dnb_vect.size() != 10)
		{
			printf("[Error]: Input dnb vector is not correct.\n");
			return 0;
		}
		size_t dnb_sum = 0;
		for (uint i = 1; i < 9; ++i)
		{
			for (uint j = 1; j < 9; ++j)
			{
				dnb_sum += (dnb_vect[i] - 3) * (dnb_vect[j] - 3);
			}
		}
		return dnb_sum;
	}

	/*correct track cross with integral*/
	static int correctTCSubpixelWithFlagInteg(const cv::Mat& integral,
		const cv::Point& pt_in,
		cv::Point2f& pt_out,
		const int flag,
		const int width,
		const int height,
		const int R = 14,
		const int L = 14);

	/*filter exceptional bright spot*/
	static int filterExpBrightSpot(const cv::Mat& img_in, cv::Mat& img_out);
	static int filterExpBrightSpot(const cv::Mat& img_in,
		const float& th_1,
		const float& th_2,
		cv::Mat& img_out);

	/*judge exceptional bright point*/
	static inline bool judgeBrightPoint(const cv::Mat& img, const cv::Point& point)
	{//presume img.type() == CV_32F
		const int x = point.x;
		const int y = point.y;
		if (img.at<float>(y, x) > 20000)
			return true;
		if (img.at<float>(y, x - 1) > 9000 && img.at<float>(y, x + 1) > 9000)
			return true;
		if (img.at<float>(y, x - 1) > 9000 && img.at<float>(y, x) > 9000)
			return true;
		if (img.at<float>(y, x) > 9000 && img.at<float>(y, x + 1) > 9000)
			return true;
		return false;
	}

	static inline bool judgeBrightPoint(const cv::Mat& img, const float& th_1, const float& th_2, const cv::Point& point)
	{//presume img.type() == CV_32F
		const int x = point.x;
		const int y = point.y;
		if (img.at<float>(y, x) > th_1)
			return true;
		if (img.at<float>(y, x - 1) > th_2 && img.at<float>(y, x + 1) > th_2)
			return true;
		if (img.at<float>(y, x - 1) > th_2 && img.at<float>(y, x) > th_2)
			return true;
		if (img.at<float>(y, x) > th_2 && img.at<float>(y, x + 1) > th_2)
			return true;
		return false;
	}

	/*filter exceptional bright point of points*/
	static inline void filterExpbrightPoints(const cv::Mat& img, std::vector<cv::Point>& pts)
	{//presume img.type() == CV_32F
		auto it = pts.begin();
		while (it != pts.end())
		{
			if (Utils::judgeBrightPoint(img, *it))
				it = pts.erase(it);
			else
				++it;
		}
	}

	static inline void filterExpbrightPoints(const cv::Mat& img, const float& th_1, const float& th_2, std::vector<cv::Point>& pts)
	{//presume img.type() == CV_32F
		auto it = pts.begin();
		while (it != pts.end())
		{
			if (Utils::judgeBrightPoint(img, th_1, th_2, *it))
				it = pts.erase(it);
			else
				++it;
		}
	}

	/*get line points by flag and line ID*/
	static int getLinePoints(const std::vector<cv::Point>& matrix, const std::pair<int, int>& width_height, const int flag, const int line_id, std::vector<cv::Point>& line_points);//cv::Point
	static int getLinePoints(const std::vector<cv::Point2f>& matrix, const std::pair<int, int>& width_height, const int flag, const int line_id, std::vector<cv::Point2f>& line_points);//cv::Point2f

	static inline int getHVLineCross(const float& k1,
		const float& k2,
		const float& b1,
		const float& b2,
		cv::Point2f& pt_out)
	{
		if (k1 != k2)
		{
			pt_out.x = (b2 - b1) / (k1 - k2);
			pt_out.y = (k1 * b2 - k2 * b1) / (k1 - k2);
			return 0;
		}
		else
		{
			printf("[Warning]: two lines are parallel.\n");
			return -1;
		}
	};

	/*put line points back to matrix(array)*/
	static int putLinePointsBack(const std::vector<cv::Point2f>& line_points, const std::pair<int, int>& flag_id, const std::pair<int, int>& width_height, std::vector<cv::Point2f>& matrix);

	/*correct line x(1) or y(0)*/
	static int correctLine(const cv::Mat& img, const cv::Point2f& pt_in, cv::Point2f& pt_out, int flag = 0, int R = 10, int L = 100);

	/*correct line by integral*/
	static int correctLineInteg(const cv::Mat& integral, const cv::Point2f& pt_in, cv::Point2f& pt_out, const int width, const int height, int flag = 0, int R = 10, int L = 100);

	/*get all DNB coordinates*/
	static int extractAllDNBPoints(const int mode, const std::vector<cv::Point>& grid_sizes, const std::vector<std::vector<cv::Point2f>>& blocks, std::vector<cv::Point2f>& dnb_points);
	static int extractAllDNBPoints(const int mode, const std::vector<cv::Point>& grid_sizes, const std::vector<std::vector<cv::Point2f>>& blocks, const cv::Point2f& offset, std::vector<cv::Point2f>& dnb_points);

	/*get 2 nearest points*/
	static int get2NearestPoints(const cv::Point& pt_in, std::vector<cv::Point>& points, std::pair<cv::Point, cv::Point>& two_points);
	//sub-pixel point
	static int get2NearestPoints(const cv::Point2f& pt_in, std::vector<cv::Point2f>& points, std::pair<cv::Point2f, cv::Point2f>& two_points);

	/*get 2 neighbor points*/
	static int get2NeighborPoints(const cv::Point& pt_in, std::vector<cv::Point>& points, const int flag, std::pair<cv::Point, cv::Point>& two_points);
	//sub-pixel point
	static int get2NeighborPoints(const cv::Point2f& pt_in, std::vector<cv::Point2f>& points, const int flag, std::pair<cv::Point2f, cv::Point2f>& two_points);

	/*rearrange 4 points*/
	static int rearrange4Points(const cv::Point(&pts_in)[4], cv::Point(&pts_out)[4]);
	static int rearrange4Points(const cv::Point2f(&pts_in)[4], cv::Point2f(&pts_out)[4]);

	/*remove out_lier*/
	static int removeOutlier(const std::vector<cv::Point2f>& offsets, cv::Point2f& offset);

	static int calculateOffsetToTemplate(const cv::Point(&pts_reference)[4], const cv::Point2f(&pts)[4], cv::Point2f& pt_offset);

	/*calculate warp matrix*/
	static int getAffineMatrix(const cv::Point2f(&pts_reference)[4], const cv::Point2f(&pts)[4], cv::Mat& mat_out);

	/*select 3 points out of 4 points*/
	static int select3Of4Points(const cv::Point2f(&pts_in)[4], cv::Point2f(&pts_out)[3]);
	static int select3Of4Points(const cv::Point(&pts_in)[4], cv::Point(&pts_out)[3]);
	static int select3of4Points(const cv::Point2f(&pts_in)[4], const int remove_id, cv::Point2f(&pts_out)[3]);

	/*detect track line*/
	static int detectTrack(const std::map<int, int>& features, const std::pair<int, int>& start_bias, int& key);

	/*detect all track lines*/
	static int detectAllTrack(const std::map<int, int>& features, std::vector<int>& keys);

	/*extract area features of image*/
	static int getAreaFeatures(const cv::Mat& img, const cv::Point pt_start, const std::pair<int, int>& wid_len, const int flag, std::map<int, int>& features);

	/*get block good points*/
	static void getBlockGoodPts(const cv::Mat& img,
		const std::pair<cv::Point2f, cv::Point2f>& roi, std::vector<cv::Point2f>& good_pts);

	/*get block DNBs*/
	inline static void getBlockDNBs(const std::vector<cv::Point2f>& vertices,
		const std::pair<uint, uint>& grid,
		std::vector<cv::Point2f>& dnbs)
	{
		const uint& DNB_NUM_Y = grid.second;
		const uint& DNB_NUM_X = grid.first;
		dnbs.resize((DNB_NUM_X - 3) * (DNB_NUM_Y - 3));
		const uint Y_LIMIT = DNB_NUM_Y - 1;
		const uint X_LIMIT = DNB_NUM_X - 1;
		cv::Point2f left, right;
		uint i = 0;
		for (uint y = 2; y < Y_LIMIT; ++y)
		{
			left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
				+ (float)y / DNB_NUM_Y * vertices[3];
			right = (float)(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
				+ (float)y / DNB_NUM_Y * vertices[2];
			for (uint x = 2; x < X_LIMIT; ++x)
			{
				dnbs[i++] = float(DNB_NUM_X - x) / DNB_NUM_X * left
					+ (float)x / DNB_NUM_X * right;
			}
		}
	}

	/*get block DNBs*/
	inline static void getBlockDNBs(const cv::Point2f(&vertices)[4],
		const std::pair<uint, uint>& grid,
		std::vector<cv::Point2f>& dnbs)
	{//grid: first: x, second: y
		cv::Point2f left, right;
		const uint& DNB_NUM_X = grid.first;
		const uint& DNB_NUM_Y = grid.second;
		dnbs.resize((DNB_NUM_X - 3) * (DNB_NUM_Y - 3));
		const uint X_LIMIT = DNB_NUM_X - 1;
		const uint Y_LIMIT = DNB_NUM_Y - 1;
		uint i = 0;
		for (int y = 2; y < Y_LIMIT; ++y)
		{
			left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
				+ (float)y / DNB_NUM_Y * vertices[3];
			right = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
				+ (float)y / DNB_NUM_Y * vertices[2];
			for (int x = 2; x < X_LIMIT; ++x)
			{
				dnbs[i++] = float(DNB_NUM_X - x) / DNB_NUM_X * left
					+ (float)x / DNB_NUM_X * right;
			}
		}
	}

	/*
	* get block DNBs
	* arranged to be 2D array
	*/
	inline static void getBlockDNBs2D(const cv::Point2f(&vertices)[4],
		const std::pair<uint, uint>& grid,
		std::vector<std::vector<cv::Point2f>>& dnbs)
	{
		cv::Point2f left, right;
		const uint& DNB_NUM_X = grid.first;
		const uint& DNB_NUM_Y = grid.second;
		dnbs.resize(DNB_NUM_Y - 3);//resize dnbs
		const uint X_LIMIT = DNB_NUM_X - 1;
		const uint Y_LIMIT = DNB_NUM_Y - 1;
		std::vector<cv::Point2f> row_dnbs;
		for (uint y = 2, row = 0; y < Y_LIMIT; ++y, ++row)
		{
			dnbs[row].resize(DNB_NUM_X - 3);
			left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
				+ (float)y / DNB_NUM_Y * vertices[3];
			right = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
				+ (float)y / DNB_NUM_Y * vertices[2];
			for (uint x = 2, col = 0; x < X_LIMIT; ++x, ++col)
			{
				dnbs[row][col] = float(DNB_NUM_X - x) / DNB_NUM_X * left
					+ (float)x / DNB_NUM_X * right;
			}
		}
	}
	//arranged to be 2D array
	//presume: vertices.size() == 4
	inline static void getBlockDNBs2D(const std::vector<cv::Point2f>& vertices,
		const std::pair<uint, uint>& grid,
		std::vector<std::vector<cv::Point2f>>& dnbs)
	{
		cv::Point2f left, right;
		const uint& DNB_NUM_X = grid.first;
		const uint& DNB_NUM_Y = grid.second;
		dnbs.resize(DNB_NUM_Y - 3);//resize dnbs
		const uint X_LIMIT = DNB_NUM_X - 1;
		const uint Y_LIMIT = DNB_NUM_Y - 1;
		std::vector<cv::Point2f> row_dnbs;
		for (uint y = 2, row = 0; y < Y_LIMIT; ++y, ++row)
		{
			dnbs[row].resize(DNB_NUM_X - 3);
			left = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[0]
				+ (float)y / DNB_NUM_Y * vertices[3];
			right = float(DNB_NUM_Y - y) / DNB_NUM_Y * vertices[1]
				+ (float)y / DNB_NUM_Y * vertices[2];
			for (uint x = 2, col = 0; x < X_LIMIT; ++x, ++col)
			{
				dnbs[row][col] = float(DNB_NUM_X - x) / DNB_NUM_X * left
					+ (float)x / DNB_NUM_X * right;
			}
		}
	}

	/*get a quarter of given block DNBs*/
	inline static void getBlockQuarterDNBs(const std::pair<uint, uint>& grid,
		const std::vector<std::vector<cv::Point2f>>& dnbs2D,
		const int quarter_id,
		std::vector<cv::Point2f>& quarter_dnbs,
		int& dnb_id)
	{
		uint x_start, y_start, x_end, y_end;
		if (quarter_id == 0)
		{
			y_start = 0;
			x_start = 0;
			y_end = (uint)dnbs2D.size() >> 1;
			x_end = (uint)dnbs2D[0].size() >> 1;
			dnb_id = (uint)(y_end - y_start) * (x_end - x_start) - 1;
		}
		else if (quarter_id == 1)
		{
			y_start = 0;
			x_start = (uint)dnbs2D[0].size() >> 1;
			y_end = (uint)dnbs2D.size() >> 1;
			x_end = (uint)dnbs2D[0].size();
			dnb_id = -1;
		}
		else if (quarter_id == 2)
		{
			y_start = (uint)dnbs2D.size() >> 1;
			x_start = (uint)dnbs2D[0].size() >> 1;
			y_end = (uint)dnbs2D.size();
			x_end = (uint)dnbs2D[0].size();
			dnb_id = 0;
		}
		else if (quarter_id == 3)
		{
			y_start = (uint)dnbs2D.size() >> 1;
			x_start = 0;
			y_end = (uint)dnbs2D.size();
			x_end = (uint)dnbs2D[0].size() >> 1;
			dnb_id = -1;
		}
		else
		{
			printf("[Error]: the flag is supposed to be 0,1,2 or 3.\n");
			return;
		}
		quarter_dnbs.resize((y_end - y_start) * (x_end - x_start));
		uint i = 0;
		for (uint y = y_start; y < y_end; ++y)
		{
			for (uint x = x_start; x < x_end; ++x)
				quarter_dnbs[i++] = dnbs2D[y][x];
		}
	}

	/*compare 1D block DNBs and 2D DNBs*/
	inline static bool comp1DAnd2DBlockDNBs(const std::vector<cv::Point2f>& dnbs1D,
		const std::vector<std::vector<cv::Point2f>>& dnbs2D)
	{
		const uint Y_LIMIT = (uint)dnbs2D.size();
		const uint X_LIMIT = (uint)dnbs2D[0].size();
		uint i = 0;
		for (uint y = 0; y < Y_LIMIT; ++y)
		{
			for (uint x = 0; x < X_LIMIT; ++x)
			{
				if (dnbs1D[i++] != dnbs2D[y][x])
					return false;
			}
		}
		return true;
	}

	/*get parallelogram distortion*/
	static inline float getParallelogramDistort(const std::vector<cv::Point2f>& vertices)
	{//assume that input vertices's size() == 4
		const float x_diff = vertices[0].x + vertices[2].x - vertices[1].x - vertices[3].x,
			y_diff = vertices[0].y + vertices[2].y - vertices[1].y - vertices[3].y;
		return std::sqrt(x_diff * x_diff + y_diff * y_diff);
	}
	static inline float getParallelogramDistort(const std::vector<cv::Point>& vertices)
	{//assume that input vertices's size() == 4
		const float x_diff = vertices[0].x + vertices[2].x - vertices[1].x - vertices[3].x,
			y_diff = vertices[0].y + vertices[2].y - vertices[1].y - vertices[3].y;
		return std::sqrt(x_diff * x_diff + y_diff * y_diff);
	}

	/*build inside 64 blocks*/
	static inline void build64Blocks(const std::vector<cv::Point>& pts_in,
		std::vector<std::vector<cv::Point>>& blocks)
	{
		blocks.resize(64);
		int x, y;
		for (int i = 0; i < 64; ++i)
		{
			/*get row and col index*/
			y = i >> 3;//i / 8
			x = i & 7;//i % 8

			/*fill block vertices*/
			blocks[i].resize(4);
			blocks[i][0] = pts_in[y * 9 + x];
			blocks[i][1] = pts_in[y * 9 + (x + 1)];
			blocks[i][2] = pts_in[(y + 1) * 9 + (x + 1)];
			blocks[i][3] = pts_in[(y + 1) * 9 + x];
		}
	}
	static inline void build64Blocks(const std::vector<cv::Point2f>& pts_in,
		std::vector<std::vector<cv::Point2f>>& blocks)
	{
		blocks.resize(64);
		uint x, y;
		for (uint i = 0; i < 64; ++i)
		{
			/*get row and col index*/
			y = i >> 3;//i / 8
			x = i & 7;//i % 8

			/*fill block vertices*/
			blocks[i].resize(4);
			blocks[i][0] = pts_in[y * 9 + x];
			blocks[i][1] = pts_in[y * 9 + (x + 1)];
			blocks[i][2] = pts_in[(y + 1) * 9 + (x + 1)];
			blocks[i][3] = pts_in[(y + 1) * 9 + x];
		}
	}

	/*fill 64 inside blocks(Block)*/
	inline static void fill64Blocks(cv::Mat& img, const uint(&grid_vect)[8],
		const cv::Point2f(&tracks)[81], Block(&blocks)[64])
	{
		uint x, y;
		for (uint i = 0; i < 64; ++i)
		{
			x = i & 7;//blockIDX
			y = i >> 3;//blockIDY
			blocks[i].m_vertices[0] = tracks[(y << 3) + y + x];
			blocks[i].m_vertices[1] = tracks[(y << 3) + y + x + 1];
			blocks[i].m_vertices[2] = tracks[(y << 3) + y + x + 10];
			blocks[i].m_vertices[3] = tracks[(y << 3) + y + x + 9];
			blocks[i].m_id = i;
			Utils::getBlockDNBs(blocks[i].m_vertices,
				std::make_pair(grid_vect[x], grid_vect[y]), blocks[i].m_dnbs);
			Utils::getBlockGoodPts(img, std::make_pair(blocks[i].m_dnbs.front(),
				blocks[i].m_dnbs.back()), blocks[i].m_good_pts);
		}
	}

	/*build and fill 64blocks*/
	inline static void fill64Blocks(const cv::Mat& img, const uint(&grid_vect)[8],
		const std::vector<cv::Point2f>tracks, Block(&blocks)[64])
	{
		assert(tracks.size() == 81);
		uint x, y;
		for (uint i = 0; i < 64; ++i)
		{
			x = i & 7;//blockIDX
			y = i >> 3;//blockIDY
			blocks[i].m_vertices[0] = tracks[(y << 3) + y + x];
			blocks[i].m_vertices[1] = tracks[(y << 3) + y + x + 1];
			blocks[i].m_vertices[2] = tracks[(y << 3) + y + x + 10];
			blocks[i].m_vertices[3] = tracks[(y << 3) + y + x + 9];
			blocks[i].m_id = i;
			blocks[i].m_grid.first = grid_vect[x];
			blocks[i].m_grid.second = grid_vect[y];
			Utils::getBlockDNBs(blocks[i].m_vertices, blocks[i].m_grid, blocks[i].m_dnbs);
			Utils::getBlockGoodPts(img, std::make_pair(blocks[i].m_dnbs.front(),
				blocks[i].m_dnbs.back()), blocks[i].m_good_pts);
		}
	}
	//fill 2D Blocks
	inline static void fill64Blocks(const cv::Mat& img, const uint(&grid_vect)[8],
		const std::vector<cv::Point2f>tracks, Block2D(&blocks)[64])
	{
		assert(tracks.size() == 81);
		uint x, y;
		for (uint i = 0; i < 64; ++i)
		{
			x = i & 7;//blockIDX
			y = i >> 3;//blockIDY
			blocks[i].m_vertices[0] = tracks[(y << 3) + y + x];
			blocks[i].m_vertices[1] = tracks[(y << 3) + y + x + 1];
			blocks[i].m_vertices[2] = tracks[(y << 3) + y + x + 10];
			blocks[i].m_vertices[3] = tracks[(y << 3) + y + x + 9];
			blocks[i].m_id = i;
			blocks[i].m_grid.first = grid_vect[x];
			blocks[i].m_grid.second = grid_vect[y];
			//Utils::getBlockDNBs(blocks[i].m_vertices, blocks[i].grid, blocks[i].m_dnbs2d);
			//Utils::getBlockGoodPts(img, std::make_pair(blocks[i].m_dnbs2d.front(),
			//	blocks[i].m_dnbs2d.back()), blocks[i].m_good_pts);
		}
	}
	/*fill 100 blocks(Block2D)*/
	inline static void fillAllBlocks2D(cv::Mat& img, const uint(&grid_vect)[10],
		const cv::Point2f(&tracks)[121], Block2D(&blocks)[100])
	{
		std::pair<cv::Point2f, cv::Point2f> roi;
		for (uint i = 0; i < 100; ++i)
		{
			uint x = i % 10;
			uint y = i / 10;
			blocks[i].m_vertices[0] = tracks[y * 11 + x];
			blocks[i].m_vertices[1] = tracks[y * 11 + (x + 1)];
			blocks[i].m_vertices[2] = tracks[(y + 1) * 11 + x + 1];
			blocks[i].m_vertices[3] = tracks[(y + 1) * 11 + x];
			blocks[i].m_id = i;
			blocks[i].m_grid.first = grid_vect[x];
			blocks[i].m_grid.second = grid_vect[y];
			Utils::getBlockDNBs2D(blocks[i].m_vertices,//need to modify this
				std::make_pair(grid_vect[x], grid_vect[y]), blocks[i].m_dnbs2d);
			Utils::getBlock2DROI(blocks[i].m_dnbs2d, roi);
			Utils::getBlockGoodPts(img, roi, blocks[i].m_good_pts);
		}
	}

	/*get Block2D's top and back point*/
	inline static void getBlock2DROI(const std::vector<std::vector<cv::Point2f>>& dnbs2d,
		std::pair<cv::Point2f, cv::Point2f>& roi)
	{
		const uint ROW_SIZE = uint(dnbs2d.size());
		const uint COL_SIZE = uint(dnbs2d[0].size());
		roi.first = dnbs2d[0][0];
		roi.second = dnbs2d[ROW_SIZE - 1][COL_SIZE - 1];
	}

	inline static void fillAllBlocks2D(cv::Mat& img, const uint(&grid_vect)[10],
		const std::vector<cv::Point2f>& tracks, Block2D(&blocks)[100])
	{
		if (tracks.size() != 121)
		{
			printf("[Error]: input track cross size is wrong.\n");
			return;
		}
		std::pair<cv::Point2f, cv::Point2f> roi;
		for (uint i = 0; i < 100; ++i)
		{
			uint x = i % 10;
			uint y = i / 10;
			blocks[i].m_vertices[0] = tracks[y * 11 + x];
			blocks[i].m_vertices[1] = tracks[y * 11 + x + 1];
			blocks[i].m_vertices[2] = tracks[y * 11 + x + 12];//(y + 1) * 11 + (x + 1)
			blocks[i].m_vertices[3] = tracks[y * 11 + x + 11];//(y + 1) * 11 + x
			blocks[i].m_id = i;
			blocks[i].m_grid.first = grid_vect[x];
			blocks[i].m_grid.second = grid_vect[y];
			Utils::getBlockDNBs2D(blocks[i].m_vertices,//need to modify this
				std::make_pair(grid_vect[x], grid_vect[y]), blocks[i].m_dnbs2d);
			Utils::getBlock2DROI(blocks[i].m_dnbs2d, roi);
			Utils::getBlockGoodPts(img, roi, blocks[i].m_good_pts);
		}
	}

	/*
	* fill blocks from 121 track crosses
	* presume: 121 track crosses and 100 blocks
	* grids_x are the same with grids_y
	* ~10ms
	*/
	inline static void fillBlocks(cv::Mat& img,
		const int(&grids_x)[10],
		const int(&grids_y)[10],
		const std::vector<cv::Point2f>& track_cross, Block(&blocks)[100])
	{
		uint x, y;
		for (uint i = 0; i < 100; ++i)
		{
			x = i % 10;
			y = i / 10;
			blocks[i].m_vertices[0] = track_cross[y * 11 + x];
			blocks[i].m_vertices[1] = track_cross[y * 11 + x + 1];
			blocks[i].m_vertices[2] = track_cross[y * 11 + x + 12];//(y + 1) * 11 + (x + 1)
			blocks[i].m_vertices[3] = track_cross[y * 11 + x + 11];//(y + 1) * 11 + x
			blocks[i].m_id = i;
			blocks[i].m_grid.first = grids_x[x];
			blocks[i].m_grid.second = grids_y[y];
			Utils::getBlockDNBs(blocks[i].m_vertices, blocks[i].m_grid, blocks[i].m_dnbs);
		}
	}
	//presume: grids_x.size() == 10 && grids_y.size() == 10
	inline static void fillBlocks(cv::Mat& img,
		const std::vector<int>& dnb_vect_x,
		const std::vector<int>& dnb_vect_y,
		const std::vector<cv::Point2f>& track_cross,
		Block(&blocks)[100])
	{
		uint x, y;
		for (uint i = 0; i < 100; ++i)
		{
			x = i % 10;
			y = i / 10;
			blocks[i].m_vertices[0] = track_cross[y * 11 + x];
			blocks[i].m_vertices[1] = track_cross[y * 11 + x + 1];
			blocks[i].m_vertices[2] = track_cross[y * 11 + x + 12];//(y + 1) * 11 + (x + 1)
			blocks[i].m_vertices[3] = track_cross[y * 11 + x + 11];//(y + 1) * 11 + x
			blocks[i].m_id = i;
			blocks[i].m_grid.first = dnb_vect_x[x];
			blocks[i].m_grid.second = dnb_vect_y[y];
			Utils::getBlockDNBs(blocks[i].m_vertices, blocks[i].m_grid, blocks[i].m_dnbs);
		}
	}

	/*
	* fill a block with DNBs
	* presume: Blocks are already filled
	* presume: tracks.size() == 121
	* 3~4ms
	*/
	inline static void fillBlockVertexDNB(const std::vector<cv::Point2f>& tracks,
		Block(&blocks)[100])
	{
		cv::Point2f vertices[4];
		for (uint i = 0; i < 100; ++i)
		{
			uint x = i % 10;
			uint y = i / 10;
			blocks[i].m_vertices[0] = tracks[y * 11 + x];
			blocks[i].m_vertices[1] = tracks[y * 11 + x + 1];
			blocks[i].m_vertices[2] = tracks[y * 11 + x + 12];//(y + 1) * 11 + (x + 1)
			blocks[i].m_vertices[3] = tracks[y * 11 + x + 11];//(y + 1) * 11 + x
			Utils::getBlockDNBs(blocks[i].m_vertices, blocks[i].m_grid, blocks[i].m_dnbs);
		}
	}

	/*get ROI background*/
	inline static void getRoiBackground(const cv::Mat& roi, float& background)
	{
		const uint COUNT_LIMIT = 5;
		const uint Y_LIMIT = roi.rows - 1;
		const uint X_LIMIT = roi.cols - 1;
		uint count = 0;
		float val, vals_sum = 0.0f;
		for (uint y = 1; y < Y_LIMIT; ++y)
		{
			uint x = 1;
			while (x < X_LIMIT)
			{
				if (Utils::judgePeakPoint(roi, x, y))
				{
					if (count < COUNT_LIMIT)
					{
						val = Utils::getPeakPtBackground(roi, x, y);
						if (val)//in case of zero
						{
							vals_sum += val;
							++count;
						}
						x += 2;
					}
					else break;
				}
				else x += 1;
			}
			if (x < X_LIMIT) break;
		}
		if (count)
			background = vals_sum / count;
		else background = 0;
	}

	/*
	* get ROI DNBs and good points
	* presume: img.type() == CV_32F
	*/
	inline static void getRoiDNBGoodPts(const cv::Mat& img,
		const Block& blk,
		const uint& x_start,
		const uint& y_start,
		const uint& x_end,
		const uint& y_end,
		const float& pixel_per_dnb,
		std::vector<cv::Point2f>& dnbs,
		std::vector<cv::Point2f>& good_pts)
	{
		//sample the background
		float background;
		const cv::Mat& ROI = img(cv::Rect(cv::Point(x_start, y_start), cv::Point(x_end, y_end)));
		Utils::getRoiBackground(ROI, background);

		//get good point and its corresponding DNB
		uint the_id;
		cv::Point2f good_pt;
		uint y = y_start;
		while (y <= y_end)
		{
			bool is_y_good = false;
			uint x = x_start;
			while (x <= x_end)
			{
				if (Utils::judgePeakPoint(img, x, y, background)
					&& Utils::judgeFocus(img, x, y, Utils::th_focus)
					&& Utils::judgeCentralityX(img, x, y, Utils::th_centrality)
					&& Utils::judgeCentralityY(img, x, y, Utils::th_centrality))
				{
					Utils::getDNBGravity(img, cv::Point(x, y), good_pt);
					good_pts.push_back(good_pt);
					Utils::getNearestDNB(blk, good_pt, pixel_per_dnb, the_id);
					dnbs.push_back(blk.m_dnbs[the_id]);
					is_y_good = true;
					x += 2;
				}
				else x += 1;
			}
			if (is_y_good) y += 2;
			else y += 1;
		}
	}

	/*
	* get good point and its corresponding DNB
	* using common parameters
	*/
	inline static void getRoiDNBGoodPts(const cv::Mat& img,
		const Block& blk,
		const cv::Point& start,
		const cv::Point& end,
		const float& pixel_per_dnb,
		std::vector<cv::Point2f>& dnbs,
		std::vector<cv::Point2f>& good_pts)
	{
		//sample the background
		float background;
		const cv::Mat& ROI = img(cv::Rect(start, end));
		Utils::getRoiBackground(ROI, background);

		//get good point and its corresponding DNB
		uint the_id;
		cv::Point2f good_pt;
		uint y = start.y;
		while (y <= end.y)
		{
			bool is_y_good = false;
			uint x = start.x;
			while (x <= end.x)
			{
				if (Utils::judgePeakPoint(img, x, y, background)
					&& Utils::judgeFocus(img, x, y, Utils::th_focus)
					&& Utils::judgeCentralityX(img, x, y, Utils::th_centrality)
					&& Utils::judgeCentralityY(img, x, y, Utils::th_centrality))
				{
					Utils::getDNBGravity(img, cv::Point(x, y), good_pt);
					good_pts.push_back(good_pt);
					Utils::getNearestDNB(blk, good_pt, pixel_per_dnb, the_id);
					dnbs.push_back(blk.m_dnbs[the_id]);
					is_y_good = true;
					x += 2;
				}
				else x += 1;
			}
			if (is_y_good) y += 2;
			else y += 1;
		}
	}

	//using input parameters
	inline static void getRoiDNBGoodPts(const cv::Mat& img,
		const Block& blk,
		const cv::Point& start,
		const cv::Point& end,
		const GoodPtParam& param,
		const float& pixel_per_dnb,
		std::vector<cv::Point2f>& dnbs,
		std::vector<cv::Point2f>& good_pts)
	{
		//sample the background
		float background;
		const cv::Mat& ROI = img(cv::Rect(start, end));
		Utils::getRoiBackground(ROI, background);
		background *= param.m_bg_ratio;

		//get good point and its corresponding DNB
		uint the_id;
		cv::Point2f good_pt;
		uint y = start.y;
		while (y <= end.y)
		{
			bool is_y_good = false;
			uint x = start.x;
			while (x <= end.x)
			{
				if (Utils::judgePeakPoint(img, x, y, background)
					&& Utils::judgeFocus(img, x, y, param.m_th_focus)
					&& Utils::judgeCentralityX(img, x, y, param.m_th_centrality)
					&& Utils::judgeCentralityY(img, x, y, param.m_th_centrality))
				{
					Utils::getDNBGravity(img, cv::Point(x, y), good_pt);
					good_pts.push_back(good_pt);
					Utils::getNearestDNB(blk, good_pt, pixel_per_dnb, the_id);
					dnbs.push_back(blk.m_dnbs[the_id]);
					is_y_good = true;
					x += 2;
				}
				else x += 1;
			}
			if (is_y_good) y += 2;
			else y += 1;
		}
	}

	/*
	* get block(Block) ROI(a quarter) good point and DNB
	* presume: the block is already prepared
	* presume: the img.type() == CV_32F
	* ~ 1ms
	* how to optimize?
	*/
	inline static void getTCAroundDNBGoodPts(const cv::Mat& img, 
		const uint& tc_id,
		const Block(&blks)[100],
		const float& pixel_per_dnb, 
		std::vector<cv::Point2f>& dnbs,
		std::vector<cv::Point2f>& good_pts)
	{
		good_pts.clear();
		dnbs.clear();

		//get 4 block ids surrounding track cross
		uint ids[4];
		Utils::get4BlkIDsByTCID(tc_id, ids);// test track cross 26

		//0
		uint x_start, y_start, x_end, y_end;
		x_start = uint((blks[ids[0]].m_vertices[0].x + blks[ids[0]].m_vertices[1].x
			+ blks[ids[0]].m_vertices[2].x + blks[ids[0]].m_vertices[3].x) * 0.25f + 0.5f);
		y_start = uint((blks[ids[0]].m_vertices[0].y + blks[ids[0]].m_vertices[1].y
			+ blks[ids[0]].m_vertices[2].y + blks[ids[0]].m_vertices[3].y) * 0.25f + 0.5f);
		x_end = uint((blks[ids[0]].m_vertices[1].x + blks[ids[0]].m_vertices[2].x) 
			* 0.5f - pixel_per_dnb * 2.0f + 0.5f);
		y_end = uint((blks[ids[0]].m_vertices[3].y + blks[ids[0]].m_vertices[2].y) 
			* 0.5f - pixel_per_dnb * 2.0f + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[0]], cv::Point(x_start, y_start),
			cv::Point(x_end, y_end), pixel_per_dnb, dnbs, good_pts);

		//1
		x_start = uint((blks[ids[1]].m_vertices[0].x + blks[ids[1]].m_vertices[3].x)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		y_start = uint((blks[ids[1]].m_vertices[0].y + blks[ids[1]].m_vertices[1].y
			+ blks[ids[1]].m_vertices[2].y + blks[ids[1]].m_vertices[3].y) * 0.25f + 0.5f);
		x_end = uint((blks[ids[1]].m_vertices[0].x + blks[ids[1]].m_vertices[1].x
			+ blks[ids[1]].m_vertices[2].x + blks[ids[1]].m_vertices[3].x) * 0.25f + 0.5f);
		y_end = uint((blks[ids[1]].m_vertices[3].y + blks[ids[1]].m_vertices[2].y)
			* 0.5f - 2.0f * pixel_per_dnb + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[1]], cv::Point(x_start, y_start), 
			cv::Point(x_end, y_end), pixel_per_dnb, dnbs, good_pts);

		//2
		x_start = uint((blks[ids[2]].m_vertices[0].x + blks[ids[2]].m_vertices[3].x)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		y_start = uint((blks[ids[2]].m_vertices[0].y + blks[ids[2]].m_vertices[1].y)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		x_end = uint((blks[ids[2]].m_vertices[0].x + blks[ids[2]].m_vertices[1].x
			+ blks[ids[2]].m_vertices[2].x + blks[ids[2]].m_vertices[3].x) * 0.25f + 0.5f);
		y_end = uint((blks[ids[2]].m_vertices[0].y + blks[ids[2]].m_vertices[1].y
			+ blks[ids[2]].m_vertices[2].y + blks[ids[2]].m_vertices[3].y) * 0.25f + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[2]], cv::Point(x_start, y_start),
			cv::Point(x_end, y_end), pixel_per_dnb, dnbs, good_pts);

		//3
		x_start = uint((blks[ids[3]].m_vertices[0].x + blks[ids[3]].m_vertices[1].x
			+ blks[ids[3]].m_vertices[2].x + blks[ids[3]].m_vertices[3].x) * 0.25f + 0.5f);
		y_start = uint((blks[ids[3]].m_vertices[0].y + blks[ids[3]].m_vertices[1].y)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		x_end = uint((blks[ids[3]].m_vertices[1].x + blks[ids[3]].m_vertices[2].x)
			* 0.5f - 2.0f * pixel_per_dnb + 0.5f);
		y_end = uint((blks[ids[3]].m_vertices[0].y + blks[ids[3]].m_vertices[1].y
			+ blks[ids[3]].m_vertices[2].y + blks[ids[3]].m_vertices[3].y) * 0.25f + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[3]], cv::Point(x_start, y_start),
			cv::Point(x_end, y_end), pixel_per_dnb, dnbs, good_pts);
	}
	//using input parameters
	inline static void getTCAroundDNBGoodPts(const cv::Mat& img,
		const uint& tc_id,
		const Block(&blks)[100],
		const GoodPtParam& param,
		const float& pixel_per_dnb,
		std::vector<cv::Point2f>& dnbs,
		std::vector<cv::Point2f>& good_pts)
	{
		good_pts.clear();
		dnbs.clear();

		//get 4 block ids surrounding track cross
		uint ids[4];
		Utils::get4BlkIDsByTCID(tc_id, ids);// test track cross 26

		//0
		uint x_start, y_start, x_end, y_end;
		x_start = uint((blks[ids[0]].m_vertices[0].x + blks[ids[0]].m_vertices[1].x
			+ blks[ids[0]].m_vertices[2].x + blks[ids[0]].m_vertices[3].x) * 0.25f + 0.5f);
		y_start = uint((blks[ids[0]].m_vertices[0].y + blks[ids[0]].m_vertices[1].y
			+ blks[ids[0]].m_vertices[2].y + blks[ids[0]].m_vertices[3].y) * 0.25f + 0.5f);
		x_end = uint((blks[ids[0]].m_vertices[1].x + blks[ids[0]].m_vertices[2].x)
			* 0.5f - pixel_per_dnb * 2.0f + 0.5f);
		y_end = uint((blks[ids[0]].m_vertices[3].y + blks[ids[0]].m_vertices[2].y)
			* 0.5f - pixel_per_dnb * 2.0f + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[0]], cv::Point(x_start, y_start),
			cv::Point(x_end, y_end), param, pixel_per_dnb, dnbs, good_pts);

		//1
		x_start = uint((blks[ids[1]].m_vertices[0].x + blks[ids[1]].m_vertices[3].x)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		y_start = uint((blks[ids[1]].m_vertices[0].y + blks[ids[1]].m_vertices[1].y
			+ blks[ids[1]].m_vertices[2].y + blks[ids[1]].m_vertices[3].y) * 0.25f + 0.5f);
		x_end = uint((blks[ids[1]].m_vertices[0].x + blks[ids[1]].m_vertices[1].x
			+ blks[ids[1]].m_vertices[2].x + blks[ids[1]].m_vertices[3].x) * 0.25f + 0.5f);
		y_end = uint((blks[ids[1]].m_vertices[3].y + blks[ids[1]].m_vertices[2].y)
			* 0.5f - 2.0f * pixel_per_dnb + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[1]], cv::Point(x_start, y_start),
			cv::Point(x_end, y_end), param, pixel_per_dnb, dnbs, good_pts);

		//2
		x_start = uint((blks[ids[2]].m_vertices[0].x + blks[ids[2]].m_vertices[3].x)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		y_start = uint((blks[ids[2]].m_vertices[0].y + blks[ids[2]].m_vertices[1].y)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		x_end = uint((blks[ids[2]].m_vertices[0].x + blks[ids[2]].m_vertices[1].x
			+ blks[ids[2]].m_vertices[2].x + blks[ids[2]].m_vertices[3].x) * 0.25f + 0.5f);
		y_end = uint((blks[ids[2]].m_vertices[0].y + blks[ids[2]].m_vertices[1].y
			+ blks[ids[2]].m_vertices[2].y + blks[ids[2]].m_vertices[3].y) * 0.25f + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[2]], cv::Point(x_start, y_start),
			cv::Point(x_end, y_end), param, pixel_per_dnb, dnbs, good_pts);

		//3
		x_start = uint((blks[ids[3]].m_vertices[0].x + blks[ids[3]].m_vertices[1].x
			+ blks[ids[3]].m_vertices[2].x + blks[ids[3]].m_vertices[3].x) * 0.25f + 0.5f);
		y_start = uint((blks[ids[3]].m_vertices[0].y + blks[ids[3]].m_vertices[1].y)
			* 0.5f + 2.0f * pixel_per_dnb + 0.5f);
		x_end = uint((blks[ids[3]].m_vertices[1].x + blks[ids[3]].m_vertices[2].x)
			* 0.5f - 2.0f * pixel_per_dnb + 0.5f);
		y_end = uint((blks[ids[3]].m_vertices[0].y + blks[ids[3]].m_vertices[1].y
			+ blks[ids[3]].m_vertices[2].y + blks[ids[3]].m_vertices[3].y) * 0.25f + 0.5f);
		Utils::getRoiDNBGoodPts(img, blks[ids[3]], cv::Point(x_start, y_start),
			cv::Point(x_end, y_end), param, pixel_per_dnb, dnbs, good_pts);
	}

	/*evaluate track cross*/
	inline static float evaluateTrackCross(const cv::Mat& img,
		const uint& tc_id,
		const Block(&blks)[100],
		const float& pixel_per_dnb,
		const GoodPtParam& param)
	{
		std::vector<cv::Point2f> dnbs, good_pts;
		Utils::getTCAroundDNBGoodPts(img, tc_id, blks, param, pixel_per_dnb, dnbs, good_pts);
		const uint SIZE = (uint)dnbs.size();
		if (SIZE == 0)
		{
			printf("[Error]: DNB size is 0.\n");
			return 9999.9f;
		}
		float sum = 0.0f;
		for (uint i = 0; i < SIZE; ++i)
			sum += Utils::getPointDist(dnbs[i], good_pts[i]);
		return sum / SIZE;
	}

	/*
	* fine tune track cross
	* presume: img.type() == CV_32F
	* presume: blks are already prepared
	* ~50ms
	*/
	inline static int fineTune(const cv::Mat& img,
		const Block(&blks)[100],
		const GoodPtParam& param,
		const float& pixel_per_dnb,
		TCOutput& out)
	{
		cv::Mat affine;
		std::vector<cv::Point2f> dnbs, good_pts;
		for (uint i = 0; i < 81; ++i)
		{
			Utils::getTCAroundDNBGoodPts(img, INSIDE_ID[i],
				blks, param, pixel_per_dnb, dnbs, good_pts);
			if (dnbs.size() < 3 || good_pts.size() < 3)
			{
				printf("[Warning]: too few DNB or good points, quit fine tune.\n");
				return -1;
			}
			affine = cv::estimateRigidTransform(dnbs, good_pts, false);
			if (affine.empty())
			{
				printf("[Warning]: affine matrix's empty, quit fine tune.\n");
				return -1;
			}
			std::vector<cv::Point2f> input(1), output(1);
			input[0] = out.m_tracks_subpixel[INSIDE_ID[i]];//get out
			cv::transform(input, output, affine);//process
			out.m_tracks_subpixel[INSIDE_ID[i]] = output[0];//get back
		}
		return 0;
	}

	/*
	* fake track cross
	* for standard template
	*/
	inline static int updateTrackCross(const std::vector<int>& grids_x,
		const std::vector<int>& grids_y, 
		const std::pair<uint, uint>& track_num,
		const std::vector<cv::Point2f>& tracks_in,
		std::vector<cv::Point2f>& tracks_out)
	{
		if (tracks_in.size() != track_num.first * track_num.second)
		{
			printf("[Error]: input parameter's wrong.\n");
			return -1;
		}
		/*put inner track crosses*/
		const int width_ext = track_num.first + 2;
		const int height_ext = track_num.second + 2;
		std::vector<cv::Point2f> tracks_ext(height_ext * width_ext);
		for (int y = 0; y < track_num.second; ++y)
		{
			for (int x = 0; x < track_num.first; ++x)
				tracks_ext[(y + 1) * width_ext + (x + 1)] = tracks_in[y * track_num.first + x];
		}

		/*fake horizontal track cross*/
		float dist, ratio;
		const uint BORDER_DNB_NUM_X = (grids_x[grids_x.size() - 1] >> 1);
		for (int y = 1; y < height_ext - 1; ++y)
		{
			dist = tracks_ext[y * width_ext + 2].x
				- tracks_ext[y * width_ext + 1].x;
			ratio = dist / grids_x[0];
			Utils::extendArm(tracks_ext[y * width_ext + 2],
				tracks_ext[y * width_ext + 1],
				dist,
				(BORDER_DNB_NUM_X + 2) * ratio,
				tracks_ext[y * width_ext /*+ 0*/]);

			dist = tracks_ext[y * width_ext + (width_ext - 2)].x
				- tracks_ext[y * width_ext + (width_ext - 3)].x;
			ratio = dist / grids_x[grids_x.size() - 2];
			Utils::extendArm(tracks_ext[y * width_ext + (width_ext - 3)],
				tracks_ext[y * width_ext + (width_ext - 2)],
				dist,
				(BORDER_DNB_NUM_X + 1) * ratio,
				tracks_ext[y * width_ext + (width_ext - 1)]);
		}

		/*fake vertical track cross*/
		const uint BORDER_DNB_NUM_Y = (grids_y[grids_y.size() - 1] >> 1);
		for (int x = 1; x < width_ext - 1; ++x)
		{
			dist = tracks_ext[2 * width_ext + x].y
				- tracks_ext[1 * width_ext + x].y;
			ratio = dist / grids_y[0];
			Utils::extendArm(tracks_ext[2 * width_ext + x],
				tracks_ext[/*1 **/ width_ext + x],
				dist,
				(BORDER_DNB_NUM_Y + 2) * ratio,
				tracks_ext[/*0 * width_ext +*/ x]);

			dist = tracks_ext[(height_ext - 2) * width_ext + x].y
				- tracks_ext[(height_ext - 3) * width_ext + x].y;
			ratio = dist / grids_y[grids_y.size() - 2];
			Utils::extendArm(tracks_ext[(height_ext - 3) * width_ext + x],
				tracks_ext[(height_ext - 2) * width_ext + x], dist,
				(BORDER_DNB_NUM_Y + 1) * ratio,
				tracks_ext[(height_ext - 1) * width_ext + x]);
		}

		/*fake 4 corners*/
		tracks_ext[/*0 * width_ext +*/ 0] = tracks_ext[/*1 **/ width_ext /*+ 0*/]
			+ tracks_ext[/*0 * width_ext +*/ 1]
			- tracks_ext[/*1 **/ width_ext + 1];
		tracks_ext[/*0 * width_ext +*/ (width_ext - 1)] = tracks_ext[/*1 **/ width_ext + (width_ext - 1)]
			+ tracks_ext[/*0 * width_ext +*/ (width_ext - 2)]
			- tracks_ext[/*1 **/ width_ext + (width_ext - 2)];
		tracks_ext[(height_ext - 1) * width_ext + (width_ext - 1)] = tracks_ext[(height_ext - 1) * width_ext
			+ (width_ext - 2)]
			+ tracks_ext[(height_ext - 2) * width_ext + (width_ext - 1)]
			- tracks_ext[(height_ext - 2) * width_ext + (width_ext - 2)];
		tracks_ext[(height_ext - 1) * width_ext /*+ 0*/] = tracks_ext[(height_ext - 1) * width_ext + 1]
			+ tracks_ext[(height_ext - 2) * width_ext /*+ 0*/]
			- tracks_ext[(height_ext - 2) * width_ext + 1];

		/*update track crosses*/
		tracks_out = tracks_ext;
		return 0;
	}

	/*
	* get DNBs and good points within track cross surrounding area
	* presume: blocks(Block2D) are already filled
	*/
	inline static void getTrackCrossDNBsGoodPts(const cv::Mat& img,
		const uint tc_id,
		const Block2D(&blocks)[100],
		std::vector<cv::Point2f>& dnbs,
		std::vector<cv::Point2f>& good_pts)
	{
		/*get surrounding 4 block ids*/
		uint blk_ids[4];
		Utils::get4BlkIDsByTCID(tc_id, blk_ids);

		/*construct surrounding DNBs for track cross*/
		int dnb_ids[4];
		std::vector<std::vector<cv::Point2f>> quarter_dnbs(4);
		Utils::getBlockQuarterDNBs(blocks[blk_ids[0]].m_grid,
			blocks[blk_ids[0]].m_dnbs2d, 2, quarter_dnbs[0], dnb_ids[0]);
		Utils::getBlockQuarterDNBs(blocks[blk_ids[1]].m_grid,
			blocks[blk_ids[1]].m_dnbs2d, 3, quarter_dnbs[1], dnb_ids[1]);
		Utils::getBlockQuarterDNBs(blocks[blk_ids[2]].m_grid,
			blocks[blk_ids[2]].m_dnbs2d, 0, quarter_dnbs[2], dnb_ids[2]);
		Utils::getBlockQuarterDNBs(blocks[blk_ids[3]].m_grid,
			blocks[blk_ids[3]].m_dnbs2d, 1, quarter_dnbs[3], dnb_ids[3]);

		dnbs.resize(quarter_dnbs[0].size() + quarter_dnbs[1].size()
			+ quarter_dnbs[2].size() + quarter_dnbs[3].size());
		std::copy(quarter_dnbs[0].begin(), quarter_dnbs[0].end(), dnbs.begin());
		std::copy(quarter_dnbs[1].begin(),
			quarter_dnbs[1].end(), dnbs.begin() + quarter_dnbs[0].size());
		std::copy(quarter_dnbs[2].begin(), quarter_dnbs[2].end(),
			dnbs.begin() + quarter_dnbs[0].size() + quarter_dnbs[1].size());
		std::copy(quarter_dnbs[3].begin(), quarter_dnbs[3].end(),
			dnbs.begin() + quarter_dnbs[0].size() + quarter_dnbs[1].size() +
			quarter_dnbs[2].size());

		/*get surrounding Good points for track cross*/
		Utils::getBlockGoodPts(img, 
			std::make_pair(quarter_dnbs[0][dnb_ids[0]], quarter_dnbs[2][dnb_ids[2]]), good_pts);
	}

	/*build and fill 1 block*/
	inline static void fillOneBlock(const cv::Mat& img, const uint(&grid_vect)[8],
		const std::vector<cv::Point2f>& tracks, const uint bk_id, Block& block)
	{
		const uint X = bk_id & 7;
		const uint Y = bk_id >> 3;
		block.m_vertices[0] = tracks[(Y << 3) + Y + X];
		block.m_vertices[1] = tracks[(Y << 3) + Y + X + 1];
		block.m_vertices[2] = tracks[(Y << 3) + Y + X + 10];
		block.m_vertices[3] = tracks[(Y << 3) + Y + X + 9];
		block.m_id = bk_id;
		block.m_grid.first = grid_vect[X];
		block.m_grid.second = grid_vect[Y];
		Utils::getBlockDNBs(block.m_vertices, block.m_grid, block.m_dnbs);
		Utils::getBlockGoodPts(img, std::make_pair(block.m_dnbs.front(),
			block.m_dnbs.back()), block.m_good_pts);
	}

	/*match nearest DNB of a good point*/
	inline static float matchGoodPoint(const uint& dnb_dim_x,
		const cv::Point2f& start, const std::vector<cv::Point2f>& dnbs, const cv::Point2f& good_pt)
	{
		const cv::Point PT = cv::Point(cvRound((good_pt.x - start.x) / PIXEL_PER_DNB),
			cvRound((good_pt.y - start.y) / PIXEL_PER_DNB));
		const uint SIZE = (uint)dnbs.size();
		uint id_0 = Utils::getDNBID(dnb_dim_x, PT);
		id_0 = id_0 > 0 ? (id_0 < SIZE ? id_0 : SIZE - 1) : 0;
		uint id_1 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y));
		id_1 = id_1 > 0 ? (id_1 < SIZE ? id_1 : SIZE - 1) : 0;
		uint id_2 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y));
		id_2 = id_2 > 0 ? (id_2 < SIZE ? id_2 : SIZE - 1) : 0;
		uint id_3 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y - 1));
		id_3 = id_3 > 0 ? (id_3 < SIZE ? id_3 : SIZE - 1) : 0;
		uint id_4 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y + 1));
		id_4 = id_4 > 0 ? (id_4 < SIZE ? id_4 : SIZE - 1) : 0;
		uint id_5 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y - 1));
		id_5 = id_5 > 0 ? (id_5 < SIZE ? id_5 : SIZE - 1) : 0;
		uint id_6 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y - 1));
		id_6 = id_6 > 0 ? (id_6 < SIZE ? id_6 : SIZE - 1) : 0;
		uint id_7 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y + 1));
		id_7 = id_7 > 0 ? (id_7 < SIZE ? id_7 : SIZE - 1) : 0;
		uint id_8 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y + 1));
		id_8 = id_8 > 0 ? (id_8 < SIZE ? id_8 : SIZE - 1) : 0;
		std::vector<float> dists(9);
		dists[0] = Utils::getPointDist(good_pt, dnbs[id_0]);
		dists[1] = Utils::getPointDist(good_pt, dnbs[id_1]);
		dists[2] = Utils::getPointDist(good_pt, dnbs[id_2]);
		dists[3] = Utils::getPointDist(good_pt, dnbs[id_3]);
		dists[4] = Utils::getPointDist(good_pt, dnbs[id_4]);
		dists[5] = Utils::getPointDist(good_pt, dnbs[id_5]);
		dists[6] = Utils::getPointDist(good_pt, dnbs[id_6]);
		dists[7] = Utils::getPointDist(good_pt, dnbs[id_7]);
		dists[8] = Utils::getPointDist(good_pt, dnbs[id_8]);
		return *std::min_element(dists.begin(), dists.end());
	}

	/*match nearest DNB of a good point in a given block*/
	inline static float matchGoodPoints(const Block& block, const uint(&grid_vect)[8])
	{
		const uint GOOD_PT_SIZE = (uint)block.m_good_pts.size();
		const uint BLOCK_DNB_SIZE = (uint)block.m_dnbs.size();
		const uint DNB_DIM_X = grid_vect[block.m_id & 7] - 3;
		float val, sum = 0.0f;
		uint count = 0;
		for (uint i = 0; i < GOOD_PT_SIZE; ++i)
		{
			val = Utils::matchGoodPoint(DNB_DIM_X,
				block.m_dnbs[0], block.m_dnbs, block.m_good_pts[i]);
			if (val < 1.0f)
			{
				sum += val;
				++count;
			}
		}
		return sum / count;
	}

	/*
	* get one block good points
	* presume: block has track cross extracted
	*/
	inline static void fineTuneBlock(const cv::Mat& img, Block& block)
	{
		Utils::getBlockDNBs(block.m_vertices, block.m_grid, block.m_dnbs);
		Utils::getBlockGoodPts(img, std::make_pair(block.m_dnbs.front(), block.m_dnbs.back()), block.m_good_pts);
		Utils::fineTuneByAffine(block, block.m_dnbs, block.m_good_pts);
	}

	/*calculate EUCLID distance of two points*/
	inline static float getPointDist(const cv::Point2f& p1, const cv::Point2f& p2)
	{
		return (float)sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
	}

	/*get DNB index of a block*/
	inline static uint getDNBID(const uint grid_x, const cv::Point& dnb)
	{
		return dnb.y * grid_x + dnb.x;
	}

	/*
	* get DNBs by Good points of a given block.
	* this block is supposed to be built and filled.
	*/
	inline static void getBlockDNBsByGoodPts(const Block& block, const uint(&grid_vect)[8],
		std::vector<cv::Point2f>& dnbs, std::vector<cv::Point2f>& good_pts)
	{
		const uint DNB_DIM_X = grid_vect[block.m_id & 7] - 3;//% 8
		const uint SIZE = (uint)block.m_good_pts.size();
		dnbs.resize(SIZE);
		good_pts.resize(SIZE);
		uint the_id;
		for (uint i = 0; i < SIZE; ++i)
		{ 
			if (Utils::getNearestDNB(DNB_DIM_X, block, i, the_id))
			{//DNBs and good points are one-to-one correspondence in order.
				dnbs[i] = block.m_dnbs[the_id];
				good_pts[i] = block.m_good_pts[i];
			}
		}
	}
	inline static void getBlockDNBsByGoodPts(const Block& block,
		std::vector<cv::Point2f>& dnbs, std::vector<cv::Point2f>& good_pts)
	{
		const uint DNB_DIM_X = block.m_grid.first - 3;//% 8
		const uint SIZE = (uint)block.m_good_pts.size();
		dnbs.resize(SIZE);
		good_pts.resize(SIZE);
		uint the_id;
		for (uint i = 0; i < SIZE; ++i)
		{
			if (Utils::getNearestDNB(DNB_DIM_X, block, i, the_id))
			{//DNBs and good points are one-to-one correspondence in order.
				dnbs[i] = block.m_dnbs[the_id];
				good_pts[i] = block.m_good_pts[i];
			}
		}
	}

	/*get nearest DNB points and IDs*/
	inline static void getBlockDNBsByGoodPts(const Block& block, const uint(&grid_vect)[8],
		std::vector<std::pair<uint, cv::Point2f>>& dnbs, std::vector<cv::Point2f>& good_pts)
	{
		const uint DNB_DIM_X = grid_vect[block.m_id & 7] - 3;
		const uint GOOD_PT_SIZE = (uint)block.m_good_pts.size();
		dnbs.clear();
		good_pts.clear();
		uint the_id;
		for (uint i = 0; i < GOOD_PT_SIZE; ++i)
		{
			if (Utils::getNearestDNB(DNB_DIM_X, block, i, the_id))
			{
				dnbs.push_back(std::make_pair(the_id, block.m_dnbs[the_id]));
				good_pts.push_back(block.m_good_pts[i]);
			}
		}
	}

	/*get block DNB coordinate by DNB id*/
	inline static void getDNBCoordById(const cv::Point2f(&vertices)[4],
		const std::pair<uint, uint>& dnb_num, std::pair<uint, cv::Point2f>& dnb)
	{
		const uint& DNB_BUM_Y = dnb_num.second - 3;//start from block inside DNB
		const uint& DNB_BUM_X = dnb_num.first - 3;
		const uint X = dnb.first % dnb_num.first + 2;
		const uint Y = dnb.first / dnb_num.first + 2;
		const cv::Point2f LEFT = float(DNB_BUM_Y - Y) / DNB_BUM_Y * vertices[0]
			+ (float)Y / DNB_BUM_Y * vertices[3];
		const cv::Point2f RIGHT = float(DNB_BUM_Y - Y) / DNB_BUM_Y * vertices[1]
			+ (float)Y / DNB_BUM_Y * vertices[2];
		dnb.second = float(DNB_BUM_X - X) / DNB_BUM_X * LEFT
			+ (float)X / DNB_BUM_X * RIGHT;
	}

	/*update block DNBs by vertices*/
	inline static void updateBlockDNBs(const cv::Point2f(&vertices)[4],
		const std::pair<uint, uint> dnb_nums, std::vector<std::pair<uint, cv::Point2f>>& dnbs)
	{//presume 
		const uint SIZE = (uint)dnbs.size();
		for (uint i = 0; i < SIZE; ++i)
		{
			Utils::getDNBCoordById(vertices, dnb_nums, dnbs[i]);
		}
	}

	/*match corresponding DNBs and Good points*/
	inline static float matchRelatePts(const std::vector<cv::Point2f>& good_pts,
		const std::vector<std::pair<uint, cv::Point2f>>& dnbs)
	{//presume: good points are one-to-one correspondence
		const uint SIZE = (uint)good_pts.size();
		float /*val, */sum = 0.0f;
		uint count = 0;
		for (uint i = 0; i < SIZE; ++i)
		{
			//val = Utils::getPointDist(good_pts[i], dnbs[i].second);
			//if (val < 1.0f)
			//{
			//	sum += val;
			//	++count;
			//}
			sum += Utils::getPointDist(good_pts[i], dnbs[i].second);
		}
		return sum / SIZE/*count*/;
	}

	/*test good point nearest neighbor DNB*/
	//inline static int searchNearestDNB(const uint& dnb_dim_x, const cv::Point2f& start, const std::vector<cv::Point2f>& dnbs, const cv::Point2f& good_pt, uint& the_id)
	inline static int getNearestDNB(const uint& dnb_dim_x,
		const Block& blk, const uint good_id, uint& dnb_id)
	{
		const cv::Point PT = cv::Point(cvRound((blk.m_good_pts[good_id].x - blk.m_dnbs[0].x)
			/ PIXEL_PER_DNB),
			cvRound((blk.m_good_pts[good_id].y - blk.m_dnbs[0].y) / PIXEL_PER_DNB));
		const uint SIZE = (uint)blk.m_dnbs.size();
		uint id_0 = Utils::getDNBID(dnb_dim_x, PT);
		id_0 = id_0 > 0 ? (id_0 < SIZE ? id_0 : SIZE - 1) : 0;
		uint id_1 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y));
		id_1 = id_1 > 0 ? (id_1 < SIZE ? id_1 : SIZE - 1) : 0;
		uint id_2 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y));
		id_2 = id_2 > 0 ? (id_2 < SIZE ? id_2 : SIZE - 1) : 0;
		uint id_3 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y - 1));
		id_3 = id_3 > 0 ? (id_3 < SIZE ? id_3 : SIZE - 1) : 0;
		uint id_4 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y + 1));
		id_4 = id_4 > 0 ? (id_4 < SIZE ? id_4 : SIZE - 1) : 0;
		uint id_5 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y - 1));
		id_5 = id_5 > 0 ? (id_5 < SIZE ? id_5 : SIZE - 1) : 0;
		uint id_6 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y - 1));
		id_6 = id_6 > 0 ? (id_6 < SIZE ? id_6 : SIZE - 1) : 0;
		uint id_7 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y + 1));
		id_7 = id_7 > 0 ? (id_7 < SIZE ? id_7 : SIZE - 1) : 0;
		uint id_8 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y + 1));
		id_8 = id_8 > 0 ? (id_8 < SIZE ? id_8 : SIZE - 1) : 0;
		uint id_9 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 2, PT.y));
		id_9 = id_9 > 0 ? (id_9 < SIZE ? id_9 : SIZE - 1) : 0;
		uint id_10 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 2, PT.y));
		id_10 = id_10 > 0 ? (id_10 < SIZE ? id_10 : SIZE - 1) : 0;
		uint id_11 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y - 2));
		id_11 = id_11 > 0 ? (id_11 < SIZE ? id_11 : SIZE - 1) : 0;
		uint id_12 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y + 2));
		id_12 = id_12 > 0 ? (id_12 < SIZE ? id_12 : SIZE - 1) : 0;
		std::vector<float> dists(13);
		std::vector<uint> ids(13);
		dists[0] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_0]);
		ids[0] = id_0;
		dists[1] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_1]);
		ids[1] = id_1;
		dists[2] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_2]);
		ids[2] = id_2;
		dists[3] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_3]);
		ids[3] = id_3;
		dists[4] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_4]);
		ids[4] = id_4;
		dists[5] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_5]);
		ids[5] = id_5;
		dists[6] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_6]);
		ids[6] = id_6;
		dists[7] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_7]);
		ids[7] = id_7;
		dists[8] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_8]);
		ids[8] = id_8;
		dists[9] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_9]);
		ids[9] = id_9;
		dists[10] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_10]);
		ids[10] = id_10;
		dists[11] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_11]);
		ids[11] = id_11;
		dists[12] = Utils::getPointDist(blk.m_good_pts[good_id], blk.m_dnbs[id_12]);
		ids[12] = id_12;
		auto iterator = std::min_element(dists.begin(), dists.end());
		dnb_id = ids[std::distance(dists.begin(), iterator)];
		if (std::fabs(blk.m_good_pts[good_id].x - blk.m_dnbs[dnb_id].x) > 0.8f
			|| std::fabs(blk.m_good_pts[good_id].y - blk.m_dnbs[dnb_id].y) > 0.8f)
		{
			//printf("Search nearest DNB point failed.\n");
			return 0;
		}
		return 1;
	}
	inline static int getNearestDNB(const uint& dnb_dim_x,
		const Block& blk,
		const cv::Point2f& good_pt,
		uint& dnb_id)
	{
		const cv::Point PT = cv::Point(cvRound((good_pt.x - blk.m_dnbs[0].x)
			/ PIXEL_PER_DNB),
			cvRound((good_pt.y - blk.m_dnbs[0].y) / PIXEL_PER_DNB));
		const uint SIZE = (uint)blk.m_dnbs.size();
		uint id_0 = Utils::getDNBID(dnb_dim_x, PT);
		id_0 = id_0 > 0 ? (id_0 < SIZE ? id_0 : SIZE - 1) : 0;
		uint id_1 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y));
		id_1 = id_1 > 0 ? (id_1 < SIZE ? id_1 : SIZE - 1) : 0;
		uint id_2 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y));
		id_2 = id_2 > 0 ? (id_2 < SIZE ? id_2 : SIZE - 1) : 0;
		uint id_3 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y - 1));
		id_3 = id_3 > 0 ? (id_3 < SIZE ? id_3 : SIZE - 1) : 0;
		uint id_4 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y + 1));
		id_4 = id_4 > 0 ? (id_4 < SIZE ? id_4 : SIZE - 1) : 0;
		uint id_5 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y - 1));
		id_5 = id_5 > 0 ? (id_5 < SIZE ? id_5 : SIZE - 1) : 0;
		uint id_6 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y - 1));
		id_6 = id_6 > 0 ? (id_6 < SIZE ? id_6 : SIZE - 1) : 0;
		uint id_7 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y + 1));
		id_7 = id_7 > 0 ? (id_7 < SIZE ? id_7 : SIZE - 1) : 0;
		uint id_8 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y + 1));
		id_8 = id_8 > 0 ? (id_8 < SIZE ? id_8 : SIZE - 1) : 0;
		uint id_9 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 2, PT.y));
		id_9 = id_9 > 0 ? (id_9 < SIZE ? id_9 : SIZE - 1) : 0;
		uint id_10 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 2, PT.y));
		id_10 = id_10 > 0 ? (id_10 < SIZE ? id_10 : SIZE - 1) : 0;
		uint id_11 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y - 2));
		id_11 = id_11 > 0 ? (id_11 < SIZE ? id_11 : SIZE - 1) : 0;
		uint id_12 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y + 2));
		id_12 = id_12 > 0 ? (id_12 < SIZE ? id_12 : SIZE - 1) : 0;
		std::vector<float> dists(13);
		std::vector<uint> ids(13);
		dists[0] = Utils::getPointDist(good_pt, blk.m_dnbs[id_0]);
		ids[0] = id_0;
		dists[1] = Utils::getPointDist(good_pt, blk.m_dnbs[id_1]);
		ids[1] = id_1;
		dists[2] = Utils::getPointDist(good_pt, blk.m_dnbs[id_2]);
		ids[2] = id_2;
		dists[3] = Utils::getPointDist(good_pt, blk.m_dnbs[id_3]);
		ids[3] = id_3;
		dists[4] = Utils::getPointDist(good_pt, blk.m_dnbs[id_4]);
		ids[4] = id_4;
		dists[5] = Utils::getPointDist(good_pt, blk.m_dnbs[id_5]);
		ids[5] = id_5;
		dists[6] = Utils::getPointDist(good_pt, blk.m_dnbs[id_6]);
		ids[6] = id_6;
		dists[7] = Utils::getPointDist(good_pt, blk.m_dnbs[id_7]);
		ids[7] = id_7;
		dists[8] = Utils::getPointDist(good_pt, blk.m_dnbs[id_8]);
		ids[8] = id_8;
		dists[9] = Utils::getPointDist(good_pt, blk.m_dnbs[id_9]);
		ids[9] = id_9;
		dists[10] = Utils::getPointDist(good_pt, blk.m_dnbs[id_10]);
		ids[10] = id_10;
		dists[11] = Utils::getPointDist(good_pt, blk.m_dnbs[id_11]);
		ids[11] = id_11;
		dists[12] = Utils::getPointDist(good_pt, blk.m_dnbs[id_12]);
		ids[12] = id_12;
		auto iterator = std::min_element(dists.begin(), dists.end());
		dnb_id = ids[std::distance(dists.begin(), iterator)];
		if (std::fabs(good_pt.x - blk.m_dnbs[dnb_id].x) > 0.8f
			|| std::fabs(good_pt.y - blk.m_dnbs[dnb_id].y) > 0.8f)
		{
			//printf("Search nearest DNB point failed.\n");
			return 0;
		}
		return 1;
	}
	inline static int getNearestDNB(const Block& blk,
		const cv::Point2f& good_pt,
		uint& dnb_id)
	{
		const uint& DNB_DIM_X = blk.m_grid.first - 3;
		const cv::Point PT = cv::Point(cvRound((good_pt.x - blk.m_dnbs[0].x)
			/ PIXEL_PER_DNB),
			cvRound((good_pt.y - blk.m_dnbs[0].y) / PIXEL_PER_DNB));
		const uint SIZE = (uint)blk.m_dnbs.size();
		uint id_0 = Utils::getDNBID(DNB_DIM_X, PT);
		id_0 = id_0 > 0 ? (id_0 < SIZE ? id_0 : SIZE - 1) : 0;
		uint id_1 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 1, PT.y));
		id_1 = id_1 > 0 ? (id_1 < SIZE ? id_1 : SIZE - 1) : 0;
		uint id_2 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 1, PT.y));
		id_2 = id_2 > 0 ? (id_2 < SIZE ? id_2 : SIZE - 1) : 0;
		uint id_3 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y - 1));
		id_3 = id_3 > 0 ? (id_3 < SIZE ? id_3 : SIZE - 1) : 0;
		uint id_4 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y + 1));
		id_4 = id_4 > 0 ? (id_4 < SIZE ? id_4 : SIZE - 1) : 0;
		uint id_5 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 1, PT.y - 1));
		id_5 = id_5 > 0 ? (id_5 < SIZE ? id_5 : SIZE - 1) : 0;
		uint id_6 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 1, PT.y - 1));
		id_6 = id_6 > 0 ? (id_6 < SIZE ? id_6 : SIZE - 1) : 0;
		uint id_7 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 1, PT.y + 1));
		id_7 = id_7 > 0 ? (id_7 < SIZE ? id_7 : SIZE - 1) : 0;
		uint id_8 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 1, PT.y + 1));
		id_8 = id_8 > 0 ? (id_8 < SIZE ? id_8 : SIZE - 1) : 0;
		uint id_9 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 2, PT.y));
		id_9 = id_9 > 0 ? (id_9 < SIZE ? id_9 : SIZE - 1) : 0;
		uint id_10 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 2, PT.y));
		id_10 = id_10 > 0 ? (id_10 < SIZE ? id_10 : SIZE - 1) : 0;
		uint id_11 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y - 2));
		id_11 = id_11 > 0 ? (id_11 < SIZE ? id_11 : SIZE - 1) : 0;
		uint id_12 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y + 2));
		id_12 = id_12 > 0 ? (id_12 < SIZE ? id_12 : SIZE - 1) : 0;
		std::vector<float> dists(13);
		std::vector<uint> ids(13);
		dists[0] = Utils::getPointDist(good_pt, blk.m_dnbs[id_0]);
		ids[0] = id_0;
		dists[1] = Utils::getPointDist(good_pt, blk.m_dnbs[id_1]);
		ids[1] = id_1;
		dists[2] = Utils::getPointDist(good_pt, blk.m_dnbs[id_2]);
		ids[2] = id_2;
		dists[3] = Utils::getPointDist(good_pt, blk.m_dnbs[id_3]);
		ids[3] = id_3;
		dists[4] = Utils::getPointDist(good_pt, blk.m_dnbs[id_4]);
		ids[4] = id_4;
		dists[5] = Utils::getPointDist(good_pt, blk.m_dnbs[id_5]);
		ids[5] = id_5;
		dists[6] = Utils::getPointDist(good_pt, blk.m_dnbs[id_6]);
		ids[6] = id_6;
		dists[7] = Utils::getPointDist(good_pt, blk.m_dnbs[id_7]);
		ids[7] = id_7;
		dists[8] = Utils::getPointDist(good_pt, blk.m_dnbs[id_8]);
		ids[8] = id_8;
		dists[9] = Utils::getPointDist(good_pt, blk.m_dnbs[id_9]);
		ids[9] = id_9;
		dists[10] = Utils::getPointDist(good_pt, blk.m_dnbs[id_10]);
		ids[10] = id_10;
		dists[11] = Utils::getPointDist(good_pt, blk.m_dnbs[id_11]);
		ids[11] = id_11;
		dists[12] = Utils::getPointDist(good_pt, blk.m_dnbs[id_12]);
		ids[12] = id_12;
		auto iterator = std::min_element(dists.begin(), dists.end());
		dnb_id = ids[std::distance(dists.begin(), iterator)];
		if (std::fabs(good_pt.x - blk.m_dnbs[dnb_id].x) > 0.8f
			|| std::fabs(good_pt.y - blk.m_dnbs[dnb_id].y) > 0.8f)
		{
			return 0;
		}
		return 1;
	}
	inline static int getNearestDNB(const Block& blk,
		const cv::Point2f& good_pt,
		const float& pixel_per_dnb,
		uint& dnb_id)
	{
		const uint& DNB_DIM_X = blk.m_grid.first - 3;
		const cv::Point PT = cv::Point(cvRound((good_pt.x - blk.m_dnbs[0].x)
			/ pixel_per_dnb),
			cvRound((good_pt.y - blk.m_dnbs[0].y) / pixel_per_dnb));
		const uint SIZE = (uint)blk.m_dnbs.size();
		uint id_0 = Utils::getDNBID(DNB_DIM_X, PT);
		id_0 = id_0 > 0 ? (id_0 < SIZE ? id_0 : SIZE - 1) : 0;
		uint id_1 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 1, PT.y));
		id_1 = id_1 > 0 ? (id_1 < SIZE ? id_1 : SIZE - 1) : 0;
		uint id_2 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 1, PT.y));
		id_2 = id_2 > 0 ? (id_2 < SIZE ? id_2 : SIZE - 1) : 0;
		uint id_3 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y - 1));
		id_3 = id_3 > 0 ? (id_3 < SIZE ? id_3 : SIZE - 1) : 0;
		uint id_4 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y + 1));
		id_4 = id_4 > 0 ? (id_4 < SIZE ? id_4 : SIZE - 1) : 0;
		uint id_5 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 1, PT.y - 1));
		id_5 = id_5 > 0 ? (id_5 < SIZE ? id_5 : SIZE - 1) : 0;
		uint id_6 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 1, PT.y - 1));
		id_6 = id_6 > 0 ? (id_6 < SIZE ? id_6 : SIZE - 1) : 0;
		uint id_7 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 1, PT.y + 1));
		id_7 = id_7 > 0 ? (id_7 < SIZE ? id_7 : SIZE - 1) : 0;
		uint id_8 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 1, PT.y + 1));
		id_8 = id_8 > 0 ? (id_8 < SIZE ? id_8 : SIZE - 1) : 0;
		uint id_9 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x - 2, PT.y));
		id_9 = id_9 > 0 ? (id_9 < SIZE ? id_9 : SIZE - 1) : 0;
		uint id_10 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x + 2, PT.y));
		id_10 = id_10 > 0 ? (id_10 < SIZE ? id_10 : SIZE - 1) : 0;
		uint id_11 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y - 2));
		id_11 = id_11 > 0 ? (id_11 < SIZE ? id_11 : SIZE - 1) : 0;
		uint id_12 = Utils::getDNBID(DNB_DIM_X, cv::Point(PT.x, PT.y + 2));
		id_12 = id_12 > 0 ? (id_12 < SIZE ? id_12 : SIZE - 1) : 0;
		std::vector<float> dists(13);
		std::vector<uint> ids(13);
		dists[0] = Utils::getPointDist(good_pt, blk.m_dnbs[id_0]);
		ids[0] = id_0;
		dists[1] = Utils::getPointDist(good_pt, blk.m_dnbs[id_1]);
		ids[1] = id_1;
		dists[2] = Utils::getPointDist(good_pt, blk.m_dnbs[id_2]);
		ids[2] = id_2;
		dists[3] = Utils::getPointDist(good_pt, blk.m_dnbs[id_3]);
		ids[3] = id_3;
		dists[4] = Utils::getPointDist(good_pt, blk.m_dnbs[id_4]);
		ids[4] = id_4;
		dists[5] = Utils::getPointDist(good_pt, blk.m_dnbs[id_5]);
		ids[5] = id_5;
		dists[6] = Utils::getPointDist(good_pt, blk.m_dnbs[id_6]);
		ids[6] = id_6;
		dists[7] = Utils::getPointDist(good_pt, blk.m_dnbs[id_7]);
		ids[7] = id_7;
		dists[8] = Utils::getPointDist(good_pt, blk.m_dnbs[id_8]);
		ids[8] = id_8;
		dists[9] = Utils::getPointDist(good_pt, blk.m_dnbs[id_9]);
		ids[9] = id_9;
		dists[10] = Utils::getPointDist(good_pt, blk.m_dnbs[id_10]);
		ids[10] = id_10;
		dists[11] = Utils::getPointDist(good_pt, blk.m_dnbs[id_11]);
		ids[11] = id_11;
		dists[12] = Utils::getPointDist(good_pt, blk.m_dnbs[id_12]);
		ids[12] = id_12;
		auto iterator = std::min_element(dists.begin(), dists.end());
		dnb_id = ids[std::distance(dists.begin(), iterator)];
		if (std::fabs(good_pt.x - blk.m_dnbs[dnb_id].x) > 0.8f
			|| std::fabs(good_pt.y - blk.m_dnbs[dnb_id].y) > 0.8f)
		{
			return 0;
		}
		return 1;
	}

	/*match nearest DNB of a good point*/
	inline static float matchGoodPt(const uint& dnb_dim_x,
		const cv::Point2f& start, const std::vector<cv::Point2f>& dnbs, const cv::Point2f& good_pt)
	{
		const cv::Point PT = cv::Point(cvRound((good_pt.x - start.x) / PIXEL_PER_DNB),
			cvRound((good_pt.y - start.y) / PIXEL_PER_DNB));
		const uint SIZE = (uint)dnbs.size();
		uint id_0 = Utils::getDNBID(dnb_dim_x, PT);
		id_0 = id_0 > 0 ? (id_0 < SIZE ? id_0 : SIZE - 1) : 0;
		uint id_1 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y));
		id_1 = id_1 > 0 ? (id_1 < SIZE ? id_1 : SIZE - 1) : 0;
		uint id_2 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y));
		id_2 = id_2 > 0 ? (id_2 < SIZE ? id_2 : SIZE - 1) : 0;
		uint id_3 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y - 1));
		id_3 = id_3 > 0 ? (id_3 < SIZE ? id_3 : SIZE - 1) : 0;
		uint id_4 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x, PT.y + 1));
		id_4 = id_4 > 0 ? (id_4 < SIZE ? id_4 : SIZE - 1) : 0;
		uint id_5 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y - 1));
		id_5 = id_5 > 0 ? (id_5 < SIZE ? id_5 : SIZE - 1) : 0;
		uint id_6 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y - 1));
		id_6 = id_6 > 0 ? (id_6 < SIZE ? id_6 : SIZE - 1) : 0;
		uint id_7 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x - 1, PT.y + 1));
		id_7 = id_7 > 0 ? (id_7 < SIZE ? id_7 : SIZE - 1) : 0;
		uint id_8 = Utils::getDNBID(dnb_dim_x, cv::Point(PT.x + 1, PT.y + 1));
		id_8 = id_8 > 0 ? (id_8 < SIZE ? id_8 : SIZE - 1) : 0;
		std::vector<float> dists(9);
		dists[0] = Utils::getPointDist(good_pt, dnbs[id_0]);
		dists[1] = Utils::getPointDist(good_pt, dnbs[id_1]);
		dists[2] = Utils::getPointDist(good_pt, dnbs[id_2]);
		dists[3] = Utils::getPointDist(good_pt, dnbs[id_3]);
		dists[4] = Utils::getPointDist(good_pt, dnbs[id_4]);
		dists[5] = Utils::getPointDist(good_pt, dnbs[id_5]);
		dists[6] = Utils::getPointDist(good_pt, dnbs[id_6]);
		dists[7] = Utils::getPointDist(good_pt, dnbs[id_7]);
		dists[8] = Utils::getPointDist(good_pt, dnbs[id_8]);
		return *std::min_element(dists.begin(), dists.end());
	}

	/*test search nearest DNB of good point*/
	inline static void searchNearestDNBs(const Block& block, const uint(&grid_vect)[8])
	{
		const uint BLOCK_DNB_SIZE = (uint)block.m_dnbs.size();
		const uint DNB_DIM_X = grid_vect[block.m_id & 7] - 3;
		const uint GOOD_PT_SIZE = (uint)block.m_good_pts.size();
		float sum = 0.0f;
		uint the_id;
		for (uint i = 0; i < GOOD_PT_SIZE; ++i)
		{
			Utils::getNearestDNB(DNB_DIM_X, block, i, the_id);
		}
	}

	/*match nearest DNB of a good point in a given block*/
	inline static float matchGoodPts(const Block& block, const uint(&grid_vect)[8])
	{
		const uint GOOD_PT_SIZE = (uint)block.m_good_pts.size();
		const uint BLOCK_DNB_SIZE = (uint)block.m_dnbs.size();
		const uint DNB_DIM_X = grid_vect[block.m_id & 7] - 3;
		float val, sum = 0.0f;
		uint count = 0;
		for (uint i = 0; i < GOOD_PT_SIZE; ++i)
		{
			val = Utils::matchGoodPt(DNB_DIM_X,
				block.m_dnbs[0], block.m_dnbs, block.m_good_pts[i]);
			if (val < 1.0f)
			{
				sum += val;
				++count;
			}
		}
		if (count)
			return sum / count;
		else
		{
			printf("[Warning]: too few good points.\n");
			return 1.0f;
		}
	}

	/*inline test optimizing block vertices*/
	inline static float optiBlock_1(Block& block, const float bias, const float step)
	{
		const float X_0_LOW = block.m_vertices[0].x - bias;
		const float X_1_LOW = block.m_vertices[1].x - bias;
		const float X_2_LOW = block.m_vertices[2].x - bias;
		const float X_3_LOW = block.m_vertices[3].x - bias;
		const float Y_0_LOW = block.m_vertices[0].y - bias;
		const float Y_1_LOW = block.m_vertices[1].y - bias;
		const float Y_2_LOW = block.m_vertices[2].y - bias;
		const float Y_3_LOW = block.m_vertices[3].y - bias;
		const float X_0_HIGH = block.m_vertices[0].x + bias;
		const float X_1_HIGH = block.m_vertices[1].x + bias;
		const float X_2_HIGH = block.m_vertices[2].x + bias;
		const float X_3_HIGH = block.m_vertices[3].x + bias;
		const float Y_0_HIGH = block.m_vertices[0].y + bias;
		const float Y_1_HIGH = block.m_vertices[1].y + bias;
		const float Y_2_HIGH = block.m_vertices[2].y + bias;
		const float Y_3_HIGH = block.m_vertices[3].y + bias;
		std::vector<cv::Point2f> vertices(4);

		float val, val_min = Utils::matchGoodPts(block, GRID_VECT_IN_X_V01);
		for (float y0 = Y_0_LOW; y0 <= Y_0_HIGH; y0 += step)
		{
			for (float y1 = Y_1_LOW; y1 <= Y_1_HIGH; y1 += step)
			{
				for (float y2 = Y_2_LOW; y2 <= Y_2_HIGH; y2 += step)
				{
					for (float y3 = Y_3_LOW; y3 <= Y_3_HIGH; y3 += step)
					{
						for (float x0 = X_0_LOW; x0 <= X_0_HIGH; x0 += step)
						{
							for (float x1 = X_1_LOW; x1 <= X_1_HIGH; x1 += step)
							{
								for (float x2 = X_2_LOW; x2 <= X_2_HIGH; x2 += step)
								{
									for (float x3 = X_3_LOW; x3 <= X_3_HIGH; x3 += step)
									{
										vertices[0].x = x0, vertices[0].y = y0;
										vertices[1].x = x1, vertices[1].y = y1;
										vertices[2].x = x2, vertices[2].y = y2;
										vertices[3].x = x3, vertices[3].y = y3;
										Utils::getBlockDNBs(vertices, block.m_grid, block.m_dnbs);
										val = Utils::matchGoodPts(block, GRID_VECT_IN_X_V01);
										if (val < val_min)
										{
											val_min = val;
											printf("optimized val: %.5fpixel\n", val_min);
											block.m_vertices = vertices;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return val_min;
	}

	/*inline test optimizing block vertices*/
	inline static float optiBlock(Block& block, const std::vector<cv::Point2f>& filter_good_pts,
		std::vector<std::pair<uint, cv::Point2f>>& dnbs,
		const float bias, const float step, const float init_val)
	{
		const float X_0_LOW = block.m_vertices[0].x - bias;
		const float X_1_LOW = block.m_vertices[1].x - bias;
		const float X_2_LOW = block.m_vertices[2].x - bias;
		const float X_3_LOW = block.m_vertices[3].x - bias;
		const float Y_0_LOW = block.m_vertices[0].y - bias;
		const float Y_1_LOW = block.m_vertices[1].y - bias;
		const float Y_2_LOW = block.m_vertices[2].y - bias;
		const float Y_3_LOW = block.m_vertices[3].y - bias;
		const float X_0_HIGH = block.m_vertices[0].x + bias;
		const float X_1_HIGH = block.m_vertices[1].x + bias;
		const float X_2_HIGH = block.m_vertices[2].x + bias;
		const float X_3_HIGH = block.m_vertices[3].x + bias;
		const float Y_0_HIGH = block.m_vertices[0].y + bias;
		const float Y_1_HIGH = block.m_vertices[1].y + bias;
		const float Y_2_HIGH = block.m_vertices[2].y + bias;
		const float Y_3_HIGH = block.m_vertices[3].y + bias;
		cv::Point2f vertices[4];

		float val, val_min = init_val;
		for (float y0 = Y_0_LOW; y0 <= Y_0_HIGH; y0 += step)
		{
			for (float y1 = Y_1_LOW; y1 <= Y_1_HIGH; y1 += step)
			{
				for (float y2 = Y_2_LOW; y2 <= Y_2_HIGH; y2 += step)
				{
					for (float y3 = Y_3_LOW; y3 <= Y_3_HIGH; y3 += step)
					{
						for (float x0 = X_0_LOW; x0 <= X_0_HIGH; x0 += step)
						{
							for (float x1 = X_1_LOW; x1 <= X_1_HIGH; x1 += step)
							{
								for (float x2 = X_2_LOW; x2 <= X_2_HIGH; x2 += step)
								{
									for (float x3 = X_3_LOW; x3 <= X_3_HIGH; x3 += step)
									{
										vertices[0].x = x0, vertices[0].y = y0;
										vertices[1].x = x1, vertices[1].y = y1;
										vertices[2].x = x2, vertices[2].y = y2;
										vertices[3].x = x3, vertices[3].y = y3;
										Utils::updateBlockDNBs(vertices, block.m_grid, dnbs);
										val = Utils::matchRelatePts(filter_good_pts, dnbs);
										if (val < val_min)
										{
											val_min = val;
											printf("optimized val: %.5fpixel\n", val_min);
											block.m_vertices[0] = vertices[0];
											block.m_vertices[1] = vertices[1];
											block.m_vertices[2] = vertices[2];
											block.m_vertices[3] = vertices[3];
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return val_min;
	}

	/*do offsets statistics*/
	inline static float processDistOffset(const std::vector<cv::Point2f>& good_pts,
		const std::vector<cv::Point2f>& dnbs, const uint bin_num, std::vector<uint>& bins)
	{//presume good_pts.size() == dnbs.size()
		const auto SIZE = good_pts.size();
		printf("Point number: %d\n", SIZE);

		std::vector<float> dists(SIZE)/*, directions(SIZE)*/;
		for (uint i = 0; i < SIZE; ++i)
			dists[i] = Utils::getPointDist(good_pts[i], dnbs[i]);

		//get bins boundary values
		auto min_max = std::minmax_element(dists.begin(), dists.end());
		const float STEP = (*min_max.second - *min_max.first) / bin_num;
		std::vector<float> bin_vals(bin_num + 1);
		for (uint i = 0; i <= bin_num; ++i)
			bin_vals[i] = *min_max.first + i * STEP;

		//do statistics
		bins.resize(bin_num);
		std::memset(&bins[0], 0, sizeof(uint)* bin_num);//all init to 0.
		for (uint i = 0; i < SIZE; ++i)
		{
			for (uint j = 0; j < bin_num; ++j)
			{
				if (dists[i] >= bin_vals[j] && dists[i] < bin_vals[j + 1])
					++bins[j];
			}
		}
		auto max = std::max_element(std::begin(bins), std::end(bins));
		auto idx = std::distance(std::begin(bins), max);
		return *min_max.first + (float)idx / bin_num;
	}

	inline static void getNormParam(const std::vector<float>& ints,
		const uint& start,
		const uint& end,
		const uint& step,
		float& mean,
		float& sd)
	{
		if (start >= end)
		{
			printf("[Error]: start is bigger than end.\n");
			return;
		}

		/*calculate bin endpoint values*/
		const size_t BIN_NUM = (end - start) / step;
		std::vector<float> bin_vals(BIN_NUM + 1);
		std::vector<size_t> bins(BIN_NUM, 0);
		for (size_t i = 0; i <= BIN_NUM; ++i)
			bin_vals[i] = start + i * step;

		/*do statistics*/
		const size_t INT_NUM = ints.size();
		for (size_t i = 0; i <= INT_NUM; ++i)
		{
			for (size_t j = 0; j < BIN_NUM; ++j)
			{
				if (ints[i] >= bin_vals[j] && ints[i] < bin_vals[j + 1])
					++bins[j];
			}
		}

		/*get mean and sd*/
		float max = FLT_MIN;
		float variance = 0.0f;
		for (size_t j = 0; j < BIN_NUM; ++j)
		{
			if (bins[j] > max)
			{
				mean = (bin_vals[j] + bin_vals[j + 1]) * 0.5f;
				max = bins[j];
			}
		}
		uint count = 0;
		for (size_t i = 0; i <= INT_NUM; ++i)
		{
			if (ints[i] < mean)
			{
				variance += (ints[i] - mean) * (ints[i] - mean);
				++count;
			}
		}
		sd = std::sqrt((2.0 * variance) / ((count << 1) - 1));
	}

	/*do offsets statistics*/
	inline static float processDistOffset(const std::vector<cv::Point2f>& good_pts,
		const std::vector<cv::Point2f>& dnbs, const uint bin_num)
	{//presume good_pts.size() == dnbs.size()
		const auto SIZE = good_pts.size();
		printf("Point number: %d\n", SIZE);

		std::vector<float> dists(SIZE)/*, directions(SIZE)*/;
		for (uint i = 0; i < SIZE; ++i)
		{
			dists[i] = Utils::getPointDist(good_pts[i], dnbs[i]);
		}

		//get bins boundary values
		auto min_max = std::minmax_element(dists.begin(), dists.end());
		const float STEP = (*min_max.second - *min_max.first) / bin_num;
		std::vector<float> bin_vals(bin_num + 1);
		for (uint i = 0; i <= bin_num; ++i)
		{
			bin_vals[i] = *min_max.first + i * STEP;
		}

		//do statistics
		std::vector<uint>bins(bin_num);
		std::memset(&bins[0], 0, sizeof(uint)* bin_num);//all init to 0.
		for (uint i = 0; i < SIZE; ++i)
		{
			for (uint j = 0; j < bin_num; ++j)
			{
				if (dists[i] >= bin_vals[j] && dists[i] < bin_vals[j + 1])
				{
					++bins[j];
				}
			}
		}
		auto max = std::max_element(std::begin(bins), std::end(bins));
		auto idx = std::distance(std::begin(bins), max);
		return *min_max.first + (float)idx / bin_num;
	}

	/*filter by dist THRESHOLD and affine to fine tune block track cross*/
	inline static void filterDNBsGoodPts(const float DIST_TH,
		const std::vector<cv::Point2f>& dnbs, const std::vector<cv::Point2f>& good_pts,
		std::vector<cv::Point2f>& filtered_dnbs, std::vector<cv::Point2f>& filtered_good_pts)
	{//presume: good_pts.size() == dnbs.size()
		const uint SIZE = (uint)good_pts.size();
		for (uint i = 0; i < SIZE; ++i)
		{
			if (Utils::getPointDist(good_pts[i], dnbs[i]) < DIST_TH)
			{
				filtered_dnbs.push_back(dnbs[i]);
				filtered_good_pts.push_back(good_pts[i]);
			}
		}
	}

	/*filter by dist THRESHOLD and affine to fine tune block track cross*/
	inline static void fineTuneByAffine(Block& block, const float DIST_TH,
		const std::vector<cv::Point2f>& dnbs, const std::vector<cv::Point2f>& good_pts)
	{//presume: good_pts.size() == dnbs.size()
		//filter 
		std::vector<cv::Point2f> filtered_dnbs, filtered_good_pts;
		Utils::filterDNBsGoodPts(DIST_TH, dnbs, good_pts, filtered_dnbs, filtered_good_pts);
		printf("filtered dnb number: %d\ngood pts number:%d\n",
			filtered_dnbs.size(), filtered_good_pts.size());

		//do affine: dnbs -> good_pts
		cv::Mat affine;
		//warp_mat = cv::findHomography(filtered_dnbs, filtered_good_pts, true);
		affine = cv::estimateRigidTransform(filtered_dnbs, filtered_good_pts, true);
		std::vector<cv::Point2f> vertices(4), tuned_vertices(4);
		vertices = block.m_vertices;
		cv::transform(vertices, tuned_vertices, affine);
		block.m_vertices = tuned_vertices;
	}

	/*
	* apply affine to fine tune block track cross
	* presume: good_pts.size() == dnbs.size()
	* affine: dnbs -> good points
	*/
	inline static int fineTuneByAffine(Block& block,
		const std::vector<cv::Point2f>& dnbs, const std::vector<cv::Point2f>& good_pts)
	{
		if (dnbs.size() == 0
			|| good_pts.size() == 0
			|| dnbs.size() != good_pts.size())
		{
			printf("[Error]: input parameter is wrong.\n");
			return -1;
		}
		cv::Mat affine;
		affine = cv::estimateRigidTransform(dnbs, good_pts, true);//6 degrees of freedom.
		if (affine.empty())
			return -1;
		std::vector<cv::Point2f> vertices(4), tuned_vertices(4);
		vertices = block.m_vertices;
		cv::transform(vertices, tuned_vertices, affine);
		block.m_vertices = tuned_vertices;
		return 0;
	}

	/*
	* apply homography to fine tune block track cross
	* presume: good_pts.size() == dnbs.size()
	*/
	inline static void fineTuneByHomography(Block& block,
		const std::vector<cv::Point2f>& dnbs, const std::vector<cv::Point2f>& good_pts)
	{
		cv::Mat affine;
		affine = cv::findHomography(dnbs, good_pts);
		std::vector<cv::Point2f> vertices(4), tuned_vertices(4);
		vertices = block.m_vertices;
		cv::perspectiveTransform(vertices, tuned_vertices, affine);
		block.m_vertices = tuned_vertices;
	}

	/*output intensity of image's specific area*/
	inline static void outputAreaIntensity(const cv::Mat& img,
		const std::pair<cv::Point, cv::Point>& roi, const std::string& path = "f:/intensity.txt")
	{
		if (img.empty() || img.type() != CV_32F
			|| roi.first.x >= roi.second.x || roi.first.y >= roi.second.y)
		{
			printf("[Error]: parameter is wrong.\n");
			return;
		}
		std::ofstream out;
		out.open(path, std::ios::out);
		uint i = 0;
		for (uint y = roi.first.y; y < roi.second.y; ++y)
		{
			for (uint x = roi.first.x; x < roi.second.x; ++x)
			{
				out << img.at<float>(y, x) << std::endl;
				++i;
			}
		}
		printf("total %d points.\n", i);
		out.close();
	}

	/*get given track cross distortion based on built blocks*/
	//static float getTrackCrossDistortMin(const int tc_id,
	//	const std::vector<FOVBlock>& blocks);
	static float getTrackCrossDistortMin(const int tc_id,
		const std::vector<std::vector<cv::Point>>& blocks);
	static float getTrackCrossDistortMean(const int tc_id,
		const std::vector<std::vector<cv::Point>>& blocks);
	static float getTrackCrossDistortMin(const int tc_id,
		const std::vector<std::vector<cv::Point2f>>& blocks);

	/*judge DNB points in area points*/
	//static inline int judgeDNBPoint(const cv::Mat& img,
	//	const cv::Point& pt_in,
	//	const int threshold, 
	//	const int tolerance)
	//{//_mm_cmpgt_epi32 optimize here?
	//	if (pt_in.x - 1 < 0 || pt_in.x + 2 > img.cols
	//		|| pt_in.y - 1 < 0 || pt_in.y + 2 > img.rows)
	//		return 0;

	//	auto val = img.at<float>(pt_in.y, pt_in.x);
	//	if (img.at<float>(pt_in.y, pt_in.x) < threshold)
	//		return 0;

	//	//1 < 0
	//	if (img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x)
	//		|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x)
	//		|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x - 1)
	//		|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x + 1))
	//		return 0;

	//	//sqrt(2) < 0
	//	if (img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x - 1)
	//		|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x + 1)
	//		|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x + 1)
	//		|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x - 1))
	//		return 0;

	//	cv::Point pts_1[4], pts_sqrt2[4];
	//	pts_1[0] = cv::Point(pt_in.x, pt_in.y - 1);
	//	pts_1[1] = cv::Point(pt_in.x, pt_in.y + 1);
	//	pts_1[2] = cv::Point(pt_in.x - 1, pt_in.y);
	//	pts_1[3] = cv::Point(pt_in.x + 1, pt_in.y);
	//	pts_sqrt2[0] = cv::Point(pt_in.x - 1, pt_in.y - 1);
	//	pts_sqrt2[1] = cv::Point(pt_in.x + 1, pt_in.y - 1);
	//	pts_sqrt2[2] = cv::Point(pt_in.x - 1, pt_in.y + 1);
	//	pts_sqrt2[3] = cv::Point(pt_in.x + 1, pt_in.y + 1);
	//	int count = 0;
	//	for (int i = 0; i < 4; ++i)
	//	{
	//		for (int j = 0; j < 4; ++j)
	//		{
	//			if (img.at<float>(pts_1[i].y, pts_1[i].x) < img.at<float>(pts_sqrt2[j].y, pts_sqrt2[j].x))
	//				++count;
	//		}
	//	}
	//	if (count > tolerance)
	//		return 0;

	//	return 1;
	//}

	inline static int judgeDNBPoint(const cv::Mat& img,
		const cv::Point& pt_in, 
		const float& TH,
		const uint& TOLE)
	{//_mm_cmpgt_epi32 optimize here?
		if (pt_in.x - 1 < 0 || pt_in.x + 2 > img.cols
			|| pt_in.y - 1 < 0 || pt_in.y + 2 > img.rows)
			return 0;

		auto val = img.at<float>(pt_in.y, pt_in.x);
		if (img.at<float>(pt_in.y, pt_in.x) < TH)
			return 0;

		//1 < 0
		if (img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x + 1))
			return 0;

		//sqrt(2) < 0
		if (img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x + 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x + 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x - 1))
			return 0;

		// sqrt2 < 1
		float ints_1[4], ints_sqrt2[4];
		ints_1[0] = img.at<float>(pt_in.y - 1, pt_in.x);
		ints_1[1] = img.at<float>(pt_in.y + 1, pt_in.x);
		ints_1[2] = img.at<float>(pt_in.y, pt_in.x - 1);
		ints_1[3] = img.at<float>(pt_in.y, pt_in.x + 1);
		ints_sqrt2[0] = img.at<float>(pt_in.y - 1, pt_in.x - 1);
		ints_sqrt2[1] = img.at<float>(pt_in.y - 1, pt_in.x + 1);
		ints_sqrt2[2] = img.at<float>(pt_in.y + 1, pt_in.x - 1);
		ints_sqrt2[3] = img.at<float>(pt_in.y + 1, pt_in.x + 1);
		uint count = 0;
		for (int i = 0; i < 4; ++i)
		{
			for (int j = 0; j < 4; ++j)
			{
				if (ints_sqrt2[j] > ints_1[i])
					++count;
			}
		}
		if (count > TOLE)
			return 0;
		return 1;
	}

	// judge inner bad point
	inline static bool judgeInner(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{// presume: r_c.first * r_c.second == pts.size()
		const auto& c = c_r.first; // 一行有多少列
		const auto& pt_0 = pts[tc_id - c - 1];
		const auto& pt_1 = pts[tc_id - c];
		const auto& pt_2 = pts[tc_id - c + 1];
		const auto& pt_3 = pts[tc_id - 1];
		const auto& pt_4 = pts[tc_id + 1];
		const auto& pt_5 = pts[tc_id + c - 1];
		const auto& pt_6 = pts[tc_id + c];
		const auto& pt_7 = pts[tc_id + c + 1];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_0.x + the_pt.x - pt_1.x - pt_3.x)
			* (pt_0.x + the_pt.x - pt_1.x - pt_3.x)
			+ (pt_0.y + the_pt.y - pt_1.y - pt_3.y) * (pt_0.y + the_pt.y - pt_1.y - pt_3.y));
		const float& distort_1 = std::sqrt((pt_2.x + the_pt.x - pt_1.x - pt_4.x)
			* (pt_2.x + the_pt.x - pt_1.x - pt_4.x)
			+ (pt_2.y + the_pt.y - pt_1.y - pt_4.y) * (pt_2.y + the_pt.y - pt_1.y - pt_4.y));
		const float& distort_2 = std::sqrt((pt_5.x + the_pt.x - pt_3.x - pt_6.x)
			* (pt_5.x + the_pt.x - pt_3.x - pt_6.x)
			+ (pt_5.y + the_pt.y - pt_3.y - pt_6.y) * (pt_5.y + the_pt.y - pt_3.y - pt_6.y));
		const float& distort_3 = std::sqrt((pt_7.x + the_pt.x - pt_4.x - pt_6.x)
			* (pt_7.x + the_pt.x - pt_4.x - pt_6.x)
			+ (pt_7.y + the_pt.y - pt_4.y - pt_6.y) * (pt_7.y + the_pt.y - pt_4.y - pt_6.y));
		const auto& min_val = MIN(MIN(distort_0, distort_1), MIN(distort_2, distort_3));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeCorner0(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{// presume: r_c.first * r_c.second == pts.size()
		const auto& c = c_r.first;
		const auto& pt_0 = pts[1];
		const auto& pt_1 = pts[2];
		const auto& pt_2 = pts[c];
		const auto& pt_3 = pts[c + 1];
		const auto& pt_4 = pts[c + 2];
		const auto& pt_5 = pts[c << 1]; // c * 2
		const auto& pt_6 = pts[(c << 1) + 1];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_3.x + the_pt.x - pt_0.x - pt_2.x)
			* (pt_3.x + the_pt.x - pt_0.x - pt_2.x)
			+ (pt_3.y + the_pt.y - pt_0.y - pt_2.y) * (pt_3.y + the_pt.y - pt_0.y - pt_2.y));
		const float& distort_1 = std::sqrt((pt_4.x + the_pt.x - pt_1.x - pt_2.x)
			* (pt_4.x + the_pt.x - pt_1.x - pt_2.x)
			+ (pt_4.y + the_pt.y - pt_1.y - pt_2.y) * (pt_4.y + the_pt.y - pt_1.y - pt_2.y));
		const float& distort_2 = std::sqrt((pt_6.x + the_pt.x - pt_0.x - pt_5.x)
			* (pt_6.x + the_pt.x - pt_0.x - pt_5.x)
			+ (pt_6.y + the_pt.y - pt_0.y - pt_5.y) * (pt_6.y + the_pt.y - pt_0.y - pt_5.y));
		const auto& min_val = MIN(distort_0, MIN(distort_1, distort_2));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeCorner1(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{
		const auto& c = c_r.first;
		const auto& pt_0 = pts[c - 3];
		const auto& pt_1 = pts[c - 2];
		const auto& pt_2 = pts[(c << 1) - 3];
		const auto& pt_3 = pts[(c << 1) - 2];
		const auto& pt_4 = pts[(c << 1) - 1];
		const auto& pt_5 = pts[(c << 1) + c - 2];
		const auto& pt_6 = pts[(c << 1) + c - 1];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_3.x + the_pt.x - pt_1.x - pt_4.x)
			* (pt_3.x + the_pt.x - pt_1.x - pt_4.x)
			+ (pt_3.y + the_pt.y - pt_1.y - pt_4.y) * (pt_3.y + the_pt.y - pt_1.y - pt_4.y));
		const float& distort_1 = std::sqrt((pt_2.x + the_pt.x - pt_0.x - pt_4.x)
			* (pt_2.x + the_pt.x - pt_0.x - pt_4.x)
			+ (pt_2.y + the_pt.y - pt_0.y - pt_4.y) * (pt_2.y + the_pt.y - pt_0.y - pt_4.y));
		const float& distort_2 = std::sqrt((pt_5.x + the_pt.x - pt_1.x - pt_6.x)
			* (pt_5.x + the_pt.x - pt_1.x - pt_6.x)
			+ (pt_5.y + the_pt.y - pt_1.y - pt_6.y) * (pt_5.y + the_pt.y - pt_1.y - pt_6.y));
		const auto& min_val = MIN(distort_0, MIN(distort_1, distort_2));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeCorner2(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		const auto& pt_0 = pts[(r - 2) * c - 2];
		const auto& pt_1 = pts[(r - 2) * c - 1];
		const auto& pt_2 = pts[(r - 1) * c - 3];
		const auto& pt_3 = pts[(r - 1) * c - 2];
		const auto& pt_4 = pts[(r - 1) * c - 1];
		const auto& pt_5 = pts[r * c - 3];
		const auto& pt_6 = pts[r * c - 2];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_3.x + the_pt.x - pt_4.x - pt_6.x)
			* (pt_3.x + the_pt.x - pt_4.x - pt_6.x)
			+ (pt_3.y + the_pt.y - pt_4.y - pt_6.y) * (pt_3.y + the_pt.y - pt_4.y - pt_6.y));
		const float& distort_1 = std::sqrt((pt_0.x + the_pt.x - pt_1.x - pt_6.x)
			* (pt_0.x + the_pt.x - pt_1.x - pt_6.x)
			+ (pt_0.y + the_pt.y - pt_1.y - pt_6.y) * (pt_0.y + the_pt.y - pt_1.y - pt_6.y));
		const float& distort_2 = std::sqrt((pt_2.x + the_pt.x - pt_4.x - pt_5.x)
			* (pt_2.x + the_pt.x - pt_4.x - pt_5.x)
			+ (pt_2.y + the_pt.y - pt_4.y - pt_5.y) * (pt_2.y + the_pt.y - pt_4.y - pt_5.y));
		const auto& min_val = MIN(distort_0, MIN(distort_1, distort_2));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeCorner3(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		const auto& pt_0 = pts[(r - 3) * c];
		const auto& pt_1 = pts[(r - 3) * c + 1];
		const auto& pt_2 = pts[(r - 2) * c];
		const auto& pt_3 = pts[(r - 2) * c + 1];
		const auto& pt_4 = pts[(r - 2) * c + 2];
		const auto& pt_5 = pts[(r - 1) * c + 1];
		const auto& pt_6 = pts[(r - 1) * c + 2];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_3.x + the_pt.x - pt_2.x - pt_5.x)
			* (pt_3.x + the_pt.x - pt_2.x - pt_5.x)
			+ (pt_3.y + the_pt.y - pt_2.y - pt_5.y) * (pt_3.y + the_pt.y - pt_2.y - pt_5.y));
		const float& distort_1 = std::sqrt((pt_1.x + the_pt.x - pt_0.x - pt_5.x)
			* (pt_1.x + the_pt.x - pt_0.x - pt_5.x)
			+ (pt_1.y + the_pt.y - pt_0.y - pt_5.y) * (pt_1.y + the_pt.y - pt_0.y - pt_5.y));
		const float& distort_2 = std::sqrt((pt_4.x + the_pt.x - pt_2.x - pt_6.x)
			* (pt_4.x + the_pt.x - pt_2.x - pt_6.x)
			+ (pt_4.y + the_pt.y - pt_2.y - pt_6.y) * (pt_4.y + the_pt.y - pt_2.y - pt_6.y));
		const auto& min_val = MIN(distort_0, MIN(distort_1, distort_2));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeUpBorder(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{
		const auto& c = c_r.first;
		if (tc_id == 0) return Utils::judgeCorner0(pts, c_r, tc_id);
		else if (tc_id == c - 1) return Utils::judgeCorner1(pts, c_r, tc_id);
		const auto& pt_0 = pts[tc_id - 1];
		const auto& pt_1 = pts[tc_id + 1];
		const auto& pt_2 = pts[tc_id + c - 1];
		const auto& pt_3 = pts[tc_id + c];
		const auto& pt_4 = pts[tc_id + c + 1];
		const auto& pt_5 = pts[tc_id + (c << 1) - 1];
		const auto& pt_6 = pts[tc_id + (c << 1)];
		const auto& pt_7 = pts[tc_id + (c << 1) + 1];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_2.x + the_pt.x - pt_0.x - pt_3.x)
			* (pt_2.x + the_pt.x - pt_0.x - pt_3.x)
			+ (pt_2.y + the_pt.y - pt_0.y - pt_3.y) * (pt_2.y + the_pt.y - pt_0.y - pt_3.y));
		const float& distort_1 = std::sqrt((pt_4.x + the_pt.x - pt_1.x - pt_3.x)
			* (pt_4.x + the_pt.x - pt_1.x - pt_3.x)
			+ (pt_4.y + the_pt.y - pt_1.y - pt_3.y) * (pt_4.y + the_pt.y - pt_1.y - pt_3.y));
		const float& distort_2 = std::sqrt((pt_5.x + the_pt.x - pt_0.x - pt_6.x)
			* (pt_5.x + the_pt.x - pt_0.x - pt_6.x)
			+ (pt_5.y + the_pt.y - pt_0.y - pt_6.y) * (pt_5.y + the_pt.y - pt_0.y - pt_6.y));
		const float& distort_3 = std::sqrt((pt_7.x + the_pt.x - pt_1.x - pt_6.x)
			* (pt_7.x + the_pt.x - pt_1.x - pt_6.x)
			+ (pt_7.y + the_pt.y - pt_1.y - pt_6.y) * (pt_7.y + the_pt.y - pt_1.y - pt_6.y));
		const auto& min_val = MIN(MIN(distort_0, distort_1), MIN(distort_2, distort_3));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeDownBorder(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		if (tc_id == (r - 1) * c) return Utils::judgeCorner3(pts, c_r, tc_id);
		else if (tc_id == r * c - 1) return Utils::judgeCorner2(pts, c_r, tc_id);
		const auto& pt_0 = pts[tc_id - (c << 1) - 1];
		const auto& pt_1 = pts[tc_id - (c << 1)];
		const auto& pt_2 = pts[tc_id - (c << 1) + 1];
		const auto& pt_3 = pts[tc_id - c - 1];
		const auto& pt_4 = pts[tc_id - c];
		const auto& pt_5 = pts[tc_id - c + 1];
		const auto& pt_6 = pts[tc_id - 1];
		const auto& pt_7 = pts[tc_id + 1];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_3.x + the_pt.x - pt_4.x - pt_6.x)
			* (pt_3.x + the_pt.x - pt_4.x - pt_6.x)
			+ (pt_3.y + the_pt.y - pt_4.y - pt_6.y) * (pt_3.y + the_pt.y - pt_4.y - pt_6.y));
		const float& distort_1 = std::sqrt((pt_5.x + the_pt.x - pt_4.x - pt_7.x)
			* (pt_5.x + the_pt.x - pt_4.x - pt_7.x)
			+ (pt_5.y + the_pt.y - pt_4.y - pt_7.y) * (pt_5.y + the_pt.y - pt_4.y - pt_7.y));
		const float& distort_2 = std::sqrt((pt_0.x + the_pt.x - pt_1.x - pt_6.x)
			* (pt_0.x + the_pt.x - pt_1.x - pt_6.x)
			+ (pt_0.y + the_pt.y - pt_1.y - pt_6.y) * (pt_0.y + the_pt.y - pt_1.y - pt_6.y));
		const float& distort_3 = std::sqrt((pt_2.x + the_pt.x - pt_1.x - pt_7.x)
			* (pt_2.x + the_pt.x - pt_1.x - pt_7.x)
			+ (pt_2.y + the_pt.y - pt_1.y - pt_7.y) * (pt_2.y + the_pt.y - pt_1.y - pt_7.y));
		const auto& min_val = MIN(MIN(distort_0, distort_1), MIN(distort_2, distort_3));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeLeftBorder(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		if (tc_id == 0) return Utils::judgeCorner0(pts, c_r, tc_id);
		else if (tc_id == (r - 1) * c) return Utils::judgeCorner3(pts, c_r, tc_id);
		const auto& pt_0 = pts[tc_id - c];
		const auto& pt_1 = pts[tc_id - c + 1];
		const auto& pt_2 = pts[tc_id - c + 2];
		const auto& pt_3 = pts[tc_id + 1];
		const auto& pt_4 = pts[tc_id + 2];
		const auto& pt_5 = pts[tc_id + c];
		const auto& pt_6 = pts[tc_id + c + 1];
		const auto& pt_7 = pts[tc_id + c + 2];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_1.x + the_pt.x - pt_0.x - pt_3.x)
			* (pt_1.x + the_pt.x - pt_0.x - pt_3.x)
			+ (pt_1.y + the_pt.y - pt_0.y - pt_3.y) * (pt_1.y + the_pt.y - pt_0.y - pt_3.y));
		const float& distort_1 = std::sqrt((pt_6.x + the_pt.x - pt_3.x - pt_5.x)
			* (pt_6.x + the_pt.x - pt_3.x - pt_5.x)
			+ (pt_6.y + the_pt.y - pt_3.y - pt_5.y) * (pt_6.y + the_pt.y - pt_3.y - pt_5.y));
		const float& distort_2 = std::sqrt((pt_2.x + the_pt.x - pt_0.x - pt_4.x)
			* (pt_2.x + the_pt.x - pt_0.x - pt_4.x)
			+ (pt_2.y + the_pt.y - pt_0.y - pt_4.y) * (pt_2.y + the_pt.y - pt_0.y - pt_4.y));
		const float& distort_3 = std::sqrt((pt_7.x + the_pt.x - pt_4.x - pt_5.x)
			* (pt_7.x + the_pt.x - pt_4.x - pt_5.x)
			+ (pt_7.y + the_pt.y - pt_4.y - pt_5.y) * (pt_7.y + the_pt.y - pt_4.y - pt_5.y));
		const auto& min_val = MIN(MIN(distort_0, distort_1), MIN(distort_2, distort_3));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	inline static bool judgeRightBorder(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint& tc_id)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		if (tc_id == c - 1) return Utils::judgeCorner1(pts, c_r, tc_id);
		else if (tc_id == r * c - 1) return Utils::judgeCorner2(pts, c_r, tc_id);
		const auto& pt_0 = pts[tc_id - c - 2];
		const auto& pt_1 = pts[tc_id - c - 1];
		const auto& pt_2 = pts[tc_id - c];
		const auto& pt_3 = pts[tc_id - 2];
		const auto& pt_4 = pts[tc_id - 1];
		const auto& pt_5 = pts[tc_id + c - 2];
		const auto& pt_6 = pts[tc_id + c - 1];
		const auto& pt_7 = pts[tc_id + c];
		const auto& the_pt = pts[tc_id];
		const float& distort_0 = std::sqrt((pt_1.x + the_pt.x - pt_2.x - pt_4.x)
			* (pt_1.x + the_pt.x - pt_2.x - pt_4.x)
			+ (pt_1.y + the_pt.y - pt_2.y - pt_4.y) * (pt_1.y + the_pt.y - pt_2.y - pt_4.y));
		const float& distort_1 = std::sqrt((pt_6.x + the_pt.x - pt_4.x - pt_7.x)
			* (pt_6.x + the_pt.x - pt_4.x - pt_7.x)
			+ (pt_6.y + the_pt.y - pt_4.y - pt_7.y) * (pt_6.y + the_pt.y - pt_4.y - pt_7.y));
		const float& distort_2 = std::sqrt((pt_0.x + the_pt.x - pt_2.x - pt_3.x)
			* (pt_0.x + the_pt.x - pt_2.x - pt_3.x)
			+ (pt_0.y + the_pt.y - pt_2.y - pt_3.y) * (pt_0.y + the_pt.y - pt_2.y - pt_3.y));
		const float& distort_3 = std::sqrt((pt_5.x + the_pt.x - pt_3.x - pt_7.x)
			* (pt_5.x + the_pt.x - pt_3.x - pt_7.x)
			+ (pt_5.y + the_pt.y - pt_3.y - pt_7.y) * (pt_5.y + the_pt.y - pt_3.y - pt_7.y));
		const auto& min_val = MIN(MIN(distort_0, distort_1), MIN(distort_2, distort_3));
		//printf("Min distort: %.3f\n", min_val);
		if (min_val > DISTORT) return true;
		return false;
	}

	// 81： 
	inline static int getLinePtsH(const std::vector<cv::Point>& pts,
		const uint row_id,
		std::vector<cv::Point>& line_pts)
	{
		if (row_id < 0 || row_id > 8
			|| pts.size() != 81)
		{
			printf("[Error]: wrong input parameters @getLinePtsH.\n");
			return -1;
		}
		line_pts.resize(9);
		const uint OFFSET = row_id * 9;
		for (uint i = 0; i < 9; ++i)
			line_pts[i] = pts[OFFSET + i];
		return 0;
	}

	// arbitrary pattern
	inline static int getLinePtsH(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint row_id,
		std::vector<cv::Point>& line_pts)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		if (row_id < 0 || row_id > r
			|| pts.size() != c * r)
		{
			printf("[Error]: wrong input parameters @getLinePtsH.\n");
			return -1;
		}
		line_pts.resize(c);
		const uint OFFSET = row_id * c;
		for (uint i = 0; i < c; ++i)
			line_pts[i] = pts[OFFSET + i];
		return 0;
	}

	// arbitrary pattern
	inline static int getLinePtsV(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const uint col_id,
		std::vector<cv::Point>& line_pts)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		if (col_id < 0 || col_id > c
			|| pts.size() != c * r)
		{
			printf("[Error]: wrong input parameters @getLinePtsV.\n");
			return -1;
		}
		line_pts.resize(r);
		for (uint i = 0; i < r; ++i)
			line_pts[i] = pts[col_id + i * c];
		return 0;
	}

	// 81:
	inline static int getLinePtsV(const std::vector<cv::Point>& pts,
		const uint col_id,
		std::vector<cv::Point>& line_pts)
	{
		if (col_id < 0 || col_id > 8
			|| pts.size() != 81)
		{
			printf("[Error]: wrong input parameters @getLinePtsV.\n");
			return -1;
		}
		line_pts.resize(9);
		for (uint i = 0; i < 9; ++i)
			line_pts[i] = pts[col_id + i * 9];
		return 0;
	}

	/*
	* determine a line by 2 points
	* if line is almost vertical, rotate 90 degrees
	*/
	inline static void getLineParamOf2Pts(const cv::Point& pt_1,
		const cv::Point& pt_2,
		const uint flag,
		std::pair<float, float>& param)
	{
		if (flag == 0) // horizontal 
		{
			param.first = float(pt_2.y - pt_1.y) / float(pt_2.x - pt_1.x);
			param.second = float(pt_1.y - param.first * pt_1.x);
		}
		else if (flag == 1) // vertical: swaith x and y to rotate
		{
			param.first = float(pt_2.x - pt_1.x) / float(pt_2.y - pt_1.y);
			param.second = float(pt_1.x - param.first * pt_1.y);
		}
	}

	inline static float distOfPtToLine(const cv::Point& pt,
		const uint flag,
		const std::pair<float, float>& param)
	{
		if (flag == 0) // horizontal line
			return std::abs(param.first * pt.x - pt.y + param.second)
			/ std::sqrt(param.first * param.first + 1);
		else if (flag == 1) // vertical line: rotate
			return std::abs(param.first * pt.y - pt.x + param.second)
			/ std::sqrt(param.first * param.first + 1);
	}

	// test 2 points to ddetermine a line
	inline static void ransacFitLine(const std::vector<cv::Point>& line_pts,
		const float& TH,
		const uint flag,
		std::pair<float, float>& param)
	{
		const uint SIZE = (uint)line_pts.size();
		std::pair<float, float> line_param;
		uint the_count, count = 0;
		for (uint i = 0; i < SIZE; ++i)
		{
			for (uint j = i + 1; j < SIZE; ++j)
			{
				Utils::getLineParamOf2Pts(line_pts[i], line_pts[j], flag, line_param);
				the_count = 0;
				for (uint k = 0; k < SIZE; ++k)
				{
					if (Utils::distOfPtToLine(line_pts[k], flag, line_param) < TH)
						++the_count;
				}
				if (the_count > count)
				{
					count = the_count; // update max count
					param = line_param; //update parameters
				}
			}
		}
	}

	// judge bad point by ransac
	inline static bool judgeBadPtByRansac(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const float& TH,
		const uint& tc_id)
	{
		std::pair<float, float> param;
		std::vector<cv::Point> line_pts;
		Utils::getLinePtsH(pts, tc_id / c_r.first, line_pts);
		Utils::ransacFitLine(line_pts, TH, 0, param);
		float dist = Utils::distOfPtToLine(pts[tc_id], 0, param);
		if (dist > TH) return true;
		Utils::getLinePtsV(pts, tc_id % c_r.first, line_pts);
		Utils::ransacFitLine(line_pts, TH, 1, param);
		dist = Utils::distOfPtToLine(pts[tc_id], 1, param);
		if (dist > TH) return true;
		return false;
	}

	// judge bad point by ransac: calculate each line's param in advance
	inline static bool judgeBadPtByRansac(
		const std::vector<std::pair<float, float>>& h_params,
		const std::vector<std::pair<float, float>>& v_params,
		const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const float& TH,
		const uint& tc_id)
	{
		const auto& dist_h = Utils::distOfPtToLine(pts[tc_id],
			0, h_params[tc_id / c_r.first]);
		const auto& dist_r = Utils::distOfPtToLine(pts[tc_id],
			1, v_params[tc_id % c_r.first]);
		if (dist_h > TH || dist_r > TH)
			return true;
		return false;
	}

	/*calculate each horizontal and vertical line parameters*/
	inline static void calcHVLineParams(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const float& TH,
		std::vector<std::pair<float, float>>& h_params,
		std::vector<std::pair<float, float>>& v_params)
	{
		const auto& c = c_r.first;
		const auto& r = c_r.second;
		h_params.resize(r);
		v_params.resize(c);
		std::pair<float, float> param;
		std::vector<cv::Point> line_pts;
		for (uint y = 0; y < r; ++y) // horizontal lines
		{
			Utils::getLinePtsH(pts, c_r, y, line_pts); // get line points
			Utils::ransacFitLine(line_pts, TH, 0, param); // calculate line param
			h_params[y] = param; // each row
		}
		for (uint x = 0; x < c; ++x)
		{
			Utils::getLinePtsV(pts, c_r, x, line_pts);
			Utils::ransacFitLine(line_pts, TH, 1, param); // vertical
			v_params[x] = param; // each col
		}
	}

	inline static int classify(
		const std::vector<std::pair<float, float>>& h_params,
		const std::vector<std::pair<float, float>>& v_params,
		const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		const float& TH,
		std::vector<std::vector<cv::Point2f>>& good_pts_row,
		std::vector<std::vector<cv::Point2f>>& good_pts_col,
		std::vector<std::pair<int, cv::Point2f>>& bad_pts,
		std::vector<int>& bad_ids,
		bool& is_classified)
	{
		const int& SIZE = (int)pts.size();
		const auto& c = c_r.first; // number of points per row
		const auto& r = c_r.second;
		if (SIZE != c * r)
		{
			printf("[Error]: input params not corret @Utils::classify.\n");
			return -1;
		}
		uint col, row;
		for (int i = 0; i < SIZE; ++i)
		{
			col = i % c;
			row = i / c;
			if (!Utils::judgeBadPtByRansac(h_params, v_params, pts, c_r, TH, i))
			{
				good_pts_row[row][col] = pts[i];
				good_pts_col[col][row] = pts[i];
			}
			else
			{
				bad_pts.push_back(std::make_pair(i, (cv::Point2f)pts[i]));
				bad_ids.push_back(i);
			}
		}

		is_classified = true; // set calssified flag
		if ((uint)bad_ids.size() > uint(0.3f * SIZE))
		{
			//printf("[Error]: total %d bad points.\n", (uint)bad_ids.size());
			return -1;
		}
		return 1;
	}

	/*classify track cross points*/
	inline static int classify(const std::vector<cv::Point>& pts,
		const std::pair<uint, uint>& c_r,
		std::vector<std::vector<cv::Point2f>>& good_pts_row,
		std::vector<std::vector<cv::Point2f>>& good_pts_col,
		std::vector<std::pair<int, cv::Point2f>>& bad_pts,
		std::vector<int>& bad_ids,
		bool& is_classified)
	{
		const int& SIZE = (int)pts.size();
		const auto& c = c_r.first; // number of points per row
		const auto& r = c_r.second;
		if (SIZE != c * c_r.second)
		{
			printf("[Error]: input params not corret @Utils::classify.\n");
			return -1;
		}
		int col, row;
		for (int i = 0; i < SIZE; ++i)
		{
			//printf("processing %d , ", i);
			col = i % c;
			row = i / c;
			if (col > 0 && col < c - 1 && row > 0 && row < r - 1) // inner
			{
				if (!Utils::judgeInner(pts, c_r, i)) // good
				{
					good_pts_row[row][col] = pts[i];
					good_pts_col[col][row] = pts[i];
				}
				else // bad
				{
					bad_pts.push_back(std::make_pair(i, (cv::Point2f)pts[i]));
					bad_ids.push_back(i);
				}
			}
			else if (row == 0) // up row border
			{
				if (!Utils::judgeUpBorder(pts, c_r, i))
				{
					good_pts_row[row][col] = pts[i];
					good_pts_col[col][row] = pts[i];
				}
				else
				{
					bad_pts.push_back(std::make_pair(i, (cv::Point2f)pts[i]));
					bad_ids.push_back(i);
				}
			}
			else if (row == 8) // down row border
			{
				if (!Utils::judgeDownBorder(pts, c_r, i))
				{
					good_pts_row[row][col] = pts[i];
					good_pts_col[col][row] = pts[i];
				}
				else
				{
					bad_pts.push_back(std::make_pair(i, (cv::Point2f)pts[i]));
					bad_ids.push_back(i);
				}
			}
			else if (col == 0) // left col border
			{
				if (!Utils::judgeLeftBorder(pts, c_r, i))
				{
					good_pts_row[row][col] = pts[i];
					good_pts_col[col][row] = pts[i];
				}
				else
				{
					bad_pts.push_back(std::make_pair(i, (cv::Point2f)pts[i]));
					bad_ids.push_back(i);
				}
			}
			else if (col == 8) // right col border
			{
				if (!Utils::judgeRightBorder(pts, c_r, i))
				{
					good_pts_row[row][col] = pts[i];
					good_pts_col[col][row] = pts[i];
				}
				else
				{
					bad_pts.push_back(std::make_pair(i, (cv::Point2f)pts[i]));
					bad_ids.push_back(i);
				}
			}
		}

		is_classified = true; // set calssified flag
		if ((uint)bad_ids.size() > uint(0.3f * SIZE))
		{
			printf("[Error]: total %d bad points.\n", (uint)bad_ids.size());
			return 0;
		}
		return 1;
	}

	/*3*3 cell peak point judgment*/
	inline static bool judgePeakPoint(const cv::Mat& img, const cv::Point& pt_in)
	{//presume IMG's type is CV_32F and 3*3 cell point is always whithin range.
		if (   img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x + 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x + 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x + 1))
		{
			return false;
		}
		else
			return true;
	}
	inline static bool judgePeakPoint(const cv::Mat& img, const uint& x, const uint& y)
	{//presume IMG's type is CV_32F and 3*3 cell point is always whithin range.
		if (   img.at<float>(y, x) < img.at<float>(y - 1, x - 1)
			|| img.at<float>(y, x) < img.at<float>(y - 1, x)
			|| img.at<float>(y, x) < img.at<float>(y - 1, x + 1)
			|| img.at<float>(y, x) < img.at<float>(y, x - 1)
			|| img.at<float>(y, x) < img.at<float>(y, x + 1)
			|| img.at<float>(y, x) < img.at<float>(y + 1, x - 1)
			|| img.at<float>(y, x) < img.at<float>(y + 1, x)
			|| img.at<float>(y, x) < img.at<float>(y + 1, x + 1))
		{
			return false;
		}
		else
			return true;
	}

	/*3*3 cell peak point judgment: with background as threshold*/
	inline static bool judgePeakPoint(const cv::Mat& img,
		const cv::Point& pt_in, const float& background)
	{//presume IMG's type is CV_32F and 3*3 cell point is always whithin range.
		if (img.at<float>(pt_in.y, pt_in.x) < background)
			return false;
		if (img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y - 1, pt_in.x + 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y, pt_in.x + 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x - 1)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x)
			|| img.at<float>(pt_in.y, pt_in.x) < img.at<float>(pt_in.y + 1, pt_in.x + 1))
		{
			return false;
		}
		else
			return true;
	}
	inline static bool judgePeakPoint(const cv::Mat& img,
		const uint& x, const uint& y, const float& background)
	{//presume IMG's type is CV_32F and 3*3 cell point is always whithin range.
		if (img.at<float>(y, x) < background)
			return false;
		if (   img.at<float>(y, x) < img.at<float>(y - 1, x - 1)
			|| img.at<float>(y, x) < img.at<float>(y - 1, x)
			|| img.at<float>(y, x) < img.at<float>(y - 1, x + 1)
			|| img.at<float>(y, x) < img.at<float>(y, x - 1)
			|| img.at<float>(y, x) < img.at<float>(y, x + 1)
			|| img.at<float>(y, x) < img.at<float>(y + 1, x - 1)
			|| img.at<float>(y, x) < img.at<float>(y + 1, x)
			|| img.at<float>(y, x) < img.at<float>(y + 1, x + 1))
		{
			return false;
		}
		else
			return true;
	}

	/*judge centroid of peak points*/
	inline static bool judgeCentrality(const cv::Mat& img,
		const cv::Point& pt_in, const float& THRESHOLD, const int& flag)
	{//presume input point's 3*3 cell are within image.
		if (flag == 0)//judge x direction
		{
			if (
				std::fabs(img.at<float>(pt_in.y - 1, pt_in.x - 1)
				+ img.at<float>(pt_in.y, pt_in.x - 1)
				+ img.at<float>(pt_in.y + 1, pt_in.x - 1)
				- img.at<float>(pt_in.y - 1, pt_in.x + 1)
				- img.at<float>(pt_in.y, pt_in.x + 1)
				- img.at<float>(pt_in.y + 1, pt_in.x + 1)) /
				(
				img.at<float>(pt_in.y - 1, pt_in.x - 1)
				+ img.at<float>(pt_in.y - 1, pt_in.x)
				+ img.at<float>(pt_in.y - 1, pt_in.x + 1)
				+ img.at<float>(pt_in.y, pt_in.x - 1)
				+ img.at<float>(pt_in.y, pt_in.x)
				+ img.at<float>(pt_in.y, pt_in.x + 1)
				+ img.at<float>(pt_in.y + 1, pt_in.x - 1)
				+ img.at<float>(pt_in.y + 1, pt_in.x)
				+ img.at<float>(pt_in.y + 1, pt_in.x + 1)
				) > THRESHOLD

				)
			{
				return false;
			}
		}
		else if (flag == 1)//judge y direction
		{
			if (
				std::fabs(img.at<float>(pt_in.y - 1, pt_in.x - 1)
				+ img.at<float>(pt_in.y - 1, pt_in.x)
				+ img.at<float>(pt_in.y - 1, pt_in.x + 1)
				- img.at<float>(pt_in.y + 1, pt_in.x - 1)
				- img.at<float>(pt_in.y + 1, pt_in.x)
				- img.at<float>(pt_in.y + 1, pt_in.x + 1)) /
				(
				img.at<float>(pt_in.y - 1, pt_in.x - 1)
				+ img.at<float>(pt_in.y - 1, pt_in.x)
				+ img.at<float>(pt_in.y - 1, pt_in.x + 1)
				+ img.at<float>(pt_in.y, pt_in.x - 1)
				+ img.at<float>(pt_in.y, pt_in.x)
				+ img.at<float>(pt_in.y, pt_in.x + 1)
				+ img.at<float>(pt_in.y + 1, pt_in.x - 1)
				+ img.at<float>(pt_in.y + 1, pt_in.x)
				+ img.at<float>(pt_in.y + 1, pt_in.x + 1)
				) > THRESHOLD

				)
			{
				return false;
			}
		}
		return true;
	}

	/*judge centroid of peak points by X direction*/
	inline static bool judgeCentralityX(const cv::Mat& img,
		const cv::Point& pt_in, const float& THRESHOLD)
	{//presume input point's 3*3 cell are within image.
		if (
			std::fabs(img.at<float>(pt_in.y - 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y, pt_in.x - 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x - 1)
			- img.at<float>(pt_in.y - 1, pt_in.x + 1)
			- img.at<float>(pt_in.y, pt_in.x + 1)
			- img.at<float>(pt_in.y + 1, pt_in.x + 1)) /
			(
			img.at<float>(pt_in.y - 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y - 1, pt_in.x)
			+ img.at<float>(pt_in.y - 1, pt_in.x + 1)
			+ img.at<float>(pt_in.y, pt_in.x - 1)
			+ img.at<float>(pt_in.y, pt_in.x)
			+ img.at<float>(pt_in.y, pt_in.x + 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x)
			+ img.at<float>(pt_in.y + 1, pt_in.x + 1)
			) > THRESHOLD
			)
		{
			return false;
		}
		return true;
	}
	inline static bool judgeCentralityX(const cv::Mat& img,
		const uint& x, const uint& y, const float& THRESHOLD)
	{//presume input point's 3*3 cell are within image.
		if (
			std::fabs(img.at<float>(y - 1, x - 1)
			+ img.at<float>(y, x - 1)
			+ img.at<float>(y + 1, x - 1)
			- img.at<float>(y - 1, x + 1)
			- img.at<float>(y, x + 1)
			- img.at<float>(y + 1, x + 1)) /
			(
			img.at<float>(y - 1, x - 1)
			+ img.at<float>(y - 1, x)
			+ img.at<float>(y - 1, x + 1)
			+ img.at<float>(y, x - 1)
			+ img.at<float>(y, x)
			+ img.at<float>(y, x + 1)
			+ img.at<float>(y + 1, x - 1)
			+ img.at<float>(y + 1, x)
			+ img.at<float>(y + 1, x + 1)
			) > THRESHOLD
			)
		{
			return false;
		}
		return true;
	}

	/*judge centroid of peak points by Y direction*/
	inline static bool judgeCentralityY(const cv::Mat& img,
		const cv::Point& pt_in, const float& THRESHOLD)
	{//presume input point's 3*3 cell are within image.
		if (
			std::fabs(img.at<float>(pt_in.y - 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y - 1, pt_in.x)
			+ img.at<float>(pt_in.y - 1, pt_in.x + 1)
			- img.at<float>(pt_in.y + 1, pt_in.x - 1)
			- img.at<float>(pt_in.y + 1, pt_in.x)
			- img.at<float>(pt_in.y + 1, pt_in.x + 1)) /
			(
			img.at<float>(pt_in.y - 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y - 1, pt_in.x)
			+ img.at<float>(pt_in.y - 1, pt_in.x + 1)
			+ img.at<float>(pt_in.y, pt_in.x - 1)
			+ img.at<float>(pt_in.y, pt_in.x)
			+ img.at<float>(pt_in.y, pt_in.x + 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x)
			+ img.at<float>(pt_in.y + 1, pt_in.x + 1)
			) > THRESHOLD
			)
		{
			return false;
		}
		return true;
	}
	inline static bool judgeCentralityY(const cv::Mat& img,
		const uint& x, const uint& y, const float& THRESHOLD)
	{//presume input point's 3*3 cell are within image.
		if (
			std::fabs(img.at<float>(y - 1, x - 1)
			+ img.at<float>(y - 1, x)
			+ img.at<float>(y - 1, x + 1)
			- img.at<float>(y + 1, x - 1)
			- img.at<float>(y + 1, x)
			- img.at<float>(y + 1, x + 1)) /
			(
			  img.at<float>(y - 1, x - 1)
			+ img.at<float>(y - 1, x)
			+ img.at<float>(y - 1, x + 1)
			+ img.at<float>(y, x - 1)
			+ img.at<float>(y, x)
			+ img.at<float>(y, x + 1)
			+ img.at<float>(y + 1, x - 1)
			+ img.at<float>(y + 1, x)
			+ img.at<float>(y + 1, x + 1)
			) > THRESHOLD
			)
		{
			return false;
		}
		return true;
	}

	/*judge peak point focus*/
	inline static bool judgeFocus(const cv::Mat& img,
		const cv::Point& pt_in, const float& ratio)
	{
		float pt_bg = Utils::getPeakPtBackground(img, pt_in);
		if (
			(img.at<float>(pt_in.y, pt_in.x) - pt_bg) /
			(
			img.at<float>(pt_in.y - 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y - 1, pt_in.x)
			+ img.at<float>(pt_in.y - 1, pt_in.x + 1)
			+ img.at<float>(pt_in.y, pt_in.x - 1)
			+ img.at<float>(pt_in.y, pt_in.x)
			+ img.at<float>(pt_in.y, pt_in.x + 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x)
			+ img.at<float>(pt_in.y + 1, pt_in.x + 1)
			- 9 * pt_bg
			)
			< ratio
			)
		{
			return false;
		}
		return true;
	}
	inline static bool judgeFocus(const cv::Mat& img,
		const uint& x, const uint& y, const float& TH)
	{
		float pt_bg = Utils::getPeakPtBackground(img, x, y);
		if (
			(img.at<float>(y, x) - pt_bg) /
			(
			  img.at<float>(y - 1, x - 1)
			+ img.at<float>(y - 1, x)
			+ img.at<float>(y - 1, x + 1)
			+ img.at<float>(y, x - 1)
			+ img.at<float>(y, x)
			+ img.at<float>(y, x + 1)
			+ img.at<float>(y + 1, x - 1)
			+ img.at<float>(y + 1, x)
			+ img.at<float>(y + 1, x + 1)
			- 9 * pt_bg
			)
			< TH
			)
		{
			return false;
		}
		return true;
	}

	/*get track cross surrounding area points*/
	static int getDNBPointsByTC(const cv::Mat& img, const cv::Point& pt_in, int& R, std::vector<cv::Point>& dnb_pts);

	/*get track area points surrounding track cross*/
	//get track area points by horizontal and vertical direction
	static int getTrackAreaPoints(const cv::Mat& img,
		const cv::Point& pt_in,
		int& R,
		std::vector<cv::Point>& pts_h,
		std::vector<cv::Point>& pts_v);

	/*get track area points by integral*/
	static int getTrackAreaPtsInteg(const cv::Mat& integral,
		const cv::Point& pt_in,
		const int width,
		const int height,
		int& R,
		std::vector<cv::Point>& pts_h,
		std::vector<cv::Point>& pts_v);

	static int getTrackAreaPoints(const cv::Mat& img, const cv::Point& pt_in, int& R, std::vector<cv::Point>& pts);

	//put into horizontal and vertical points
	static int getDNBPointsByTC(const cv::Mat& img, const cv::Point& pt_in, int& R, std::vector<cv::Point>& dnb_pts_h, std::vector<cv::Point>& dnb_pts_v);
	static int getDNBPointsByTC(const cv::Mat& img, const cv::Point& pt_in, int& R, std::vector<cv::Point2f>& dnb_pts_h, std::vector<cv::Point2f>& dnb_pts_v);
	static int getDNBPointsByTrackCross(const cv::Mat& img,
		const cv::Point& pt_in,
		int& R,
		std::vector<cv::Point2f>& dnb_pts_h,
		std::vector<cv::Point2f>& dnb_pts_v);

	/*static get DNB points with integral*/
	static int getDNBPtsByTCInteg(const cv::Mat& img,
		const cv::Mat& integral,
		const cv::Point& pt_in,
		int& R,
		std::vector<cv::Point2f>& dnb_pts_h,
		std::vector<cv::Point2f>& dnb_pts_v);

	/*calculate gravity of a DNB cell*/
	static int calculateGravityOfDNB(cv::Mat& img,
		const cv::Point& pt_in, const float& background, cv::Point2f& pt_out);
	//static int getDNBGravity(cv::Mat& img, const cv::Point& pt_in, cv::Point2f& pt_out);

	inline static void getDNBGravity(const cv::Mat& img,
		const cv::Point& pt_in, 
		cv::Point2f& pt_out)
	{//background processing need to be more specific
		float background, val_1, val_2, val_3;
		Utils::getDNBBG(img, pt_in, background);
		//Utils::getDNBBackground(img, pt_in, background);
		val_1 = img.at<float>(pt_in.y, pt_in.x - 1) - background;
		val_2 = img.at<float>(pt_in.y, pt_in.x) - background;
		val_3 = img.at<float>(pt_in.y, pt_in.x + 1) - background;
		if (val_1 + val_2 + val_3 == 0)
			pt_out.x = float(pt_in.x) + (val_3 - val_1) / (val_1 + val_2 + val_3 + 1);
		else
			pt_out.x = float(pt_in.x) + (val_3 - val_1) / (val_1 + val_2 + val_3);
		val_1 = img.at<float>(pt_in.y - 1, pt_in.x) - background;
		val_2 = img.at<float>(pt_in.y, pt_in.x) - background;
		val_3 = img.at<float>(pt_in.y + 1, pt_in.x) - background;
		if (val_1 + val_2 + val_3 == 0)
			pt_out.y = float(pt_in.y) + (val_3 - val_1) / (val_1 + val_2 + val_3 + 1);
		else
			pt_out.y = float(pt_in.y) + (val_3 - val_1) / (val_1 + val_2 + val_3);
	}

	/*get good point centroid*/
	inline static void getGoodPtCentroid(const cv::Mat& img,
		const cv::Point& pt_in, cv::Point2f& pt_out)
	{
		float background, val_1, val_2, val_3;
		//Utils::getDNBBG(img, pt_in, background);
		Utils::getDNBBackground(img, pt_in, background);
		val_1 = img.at<float>(pt_in.y - 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y, pt_in.x - 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x - 1) - 3 * background;
		val_2 = img.at<float>(pt_in.y - 1, pt_in.x)
			+ img.at<float>(pt_in.y, pt_in.x)
			+ img.at<float>(pt_in.y + 1, pt_in.x) - 3 * background;
		val_3 = img.at<float>(pt_in.y - 1, pt_in.x + 1)
			+ img.at<float>(pt_in.y, pt_in.x + 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x + 1) - 3 * background;
		if (val_1 + val_2 + val_3 == 0)
			pt_out.x = float(pt_in.x) + (val_3 - val_1) / (val_1 + val_2 + val_3 + 1);
		else
			pt_out.x = float(pt_in.x) + (val_3 - val_1) / (val_1 + val_2 + val_3);
		val_1 = img.at<float>(pt_in.y - 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y - 1, pt_in.x)
			+ img.at<float>(pt_in.y - 1, pt_in.x + 1) - 3 * background;
		val_2 = img.at<float>(pt_in.y, pt_in.x - 1)
			+ img.at<float>(pt_in.y, pt_in.x)
			+ img.at<float>(pt_in.y, pt_in.x + 1) - 3 * background;
		val_3 = img.at<float>(pt_in.y + 1, pt_in.x - 1)
			+ img.at<float>(pt_in.y + 1, pt_in.x)
			+ img.at<float>(pt_in.y + 1, pt_in.x + 1) - 3 * background;
		if (val_1 + val_2 + val_3 == 0)
			pt_out.y = float(pt_in.y) + (val_3 - val_1) / (val_1 + val_2 + val_3 + 1);
		else
			pt_out.y = float(pt_in.y) + (val_3 - val_1) / (val_1 + val_2 + val_3);
	}

	/*get line fitting params*/
	static int getLineParams(cv::Mat& img, const cv::Point& pt_in,
		int& R, std::pair<std::pair<float, float>, std::pair<float, float>>& lines_params);

	/*parameters with integral*/
	static int getLineParamsInteg(cv::Mat& img,
		const cv::Mat& integral,
		const cv::Point& pt_in,
		int& R, std::pair < std::pair<float, float>,
		std::pair < float, float >> &lines_params);

	/*get lines parameter array with integral*/
	static int getLinesParameterArray(cv::Mat& img, const cv::Mat& integral,
		const std::vector<cv::Point>& pts_in,
		float* arr_k1,
		float* arr_k2,
		float* arr_b1,
		float* arr_b2);

	/*allocate continuous memory for 2D array*/
	//C++如何使用标准的方式生成连续的动态数组？
	//template <typename T>
	//inline static T** malloc2DArray(uint row, const uint col)
	//{
	//	const uint T_SIZE = sizeof(T);
	//	const uint PT_SIZE = sizeof(T*);
	//	T** arr2d = (T**)malloc(PT_SIZE * row + T_SIZE * row * col);
	//	if (arr2d)
	//	{
	//		T* head = (T*)(reinterpret_cast<int>(arr2d)+PT_SIZE * row);
	//		while (row--)
	//			arr2d[row] = (T*)(reinterpret_cast<int>(head)+row * col * T_SIZE);
	//	}
	//	return (T**)arr2d;
	//}
	////maybe lead to memory leakage here
	//inline static void free2Darray(void** arr)
	//{
	//	if (arr)
	//	{
	//		free(arr);
	//		arr = NULL;
	//	}
	//}

	//template<typename T>
	//inline static T** new2DArray(uint row, uint col)
	//{
	//	const uint T_SIZE = sizeof(T);
	//	const uint PT_SIZE = sizeof(T*);
	//	T** arr2d = (T**)malloc(PT_SIZE * row + T_SIZE * row * col);
	//	if (arr2d)
	//	{
	//		T* head = (T*)(reinterpret_cast<int>(arr2d)+PT_SIZE * row);
	//		for (uint i = 0; i < row; ++i)
	//		{
	//			arr2d[i] = (T*)(reinterpret_cast<int>(head)+i * col * T_SIZE);
	//			for (uint j = 0; j < col; ++j)
	//				new (&arr2d[i][j]) T;
	//		}
	//	}
	//	return (T**)arr2d;
	//}
	////release 2D array
	//template<typename T>
	//inline static void delete2DArray(T** arr2d, uint row, uint col)
	//{
	//	for (uint i = 0; i < row; ++i)
	//	{
	//		for (uint j = 0; j < col; ++j)
	//			arr2d[i][j].~T();
	//	}
	//	if (arr2d)
	//		free((void**)arr2d);
	//}

	/*
	* linspace and return an array
	*/
	inline static std::vector<float>& linspace(const float& min, 
		const float& max,
		const float& step,
		std::vector<float>& arr)
	{
		const size_t SIZE = (max - min) / step + 1;
		arr.resize(SIZE);
		float cur = min;
		for (uint i = 0; i < SIZE; ++i)
		{
			arr[i] = min + i * step;
		}
		return arr;
	}

	/*
	* binary search
	* presume: arr is sorted in ascending order
	*/
	inline static uint binarySearch(const std::vector<float>& arr, 
		const float& obj)
	{
		const size_t SIZE = arr.size();
		uint low = 0, high = SIZE - 1;
		uint mid;
		while (low < high)
		{
			mid = uint((low + high) * 0.5f);
			if (arr[mid + 1] <= obj)
				low = mid + 1;
			else if (arr[mid - 1] >= obj)
				high = mid - 1;
			else
				return mid;
		}
		return -1;
	}

	/*show 2D array*/
	//template<typename T>
	inline static void show2DArray(uint** arr2d,
		const uint& y_num, const uint& x_num)
	{
		for (uint y = 0; y < y_num; ++y)
		{
			for (uint x = 0; x < x_num; ++x)
			{
				if (arr2d[y][x])
				{
					printf("x:%d\ty:%d\n", x, y);
					std::cout << arr2d[y][x] << "\t";
				}
			}
			std::cout << "\n\n";
		}
	}

	/*
	*get track crosses by fitted line cross
	*presume: all the arrays have been correctly allocated.
	*/
	static inline int getHVLineCross(const float* arr_k1,
		const float* arr_k2,
		const float* arr_b1,
		const float* arr_b2,
		const int size,
		float* arr_x,
		float* arr_y)
	{
		for (int i = 0; i < size; ++i)
		{
			if (arr_k1[i] != arr_k2[i])
			{
				arr_x[i] = (arr_b2[i] - arr_b1[i]) / (arr_k1[i] - arr_k2[i]);
				arr_y[i] = (arr_k1[i] * arr_b2[i] - arr_k2[i] * arr_b1[i])
					/ (arr_k1[i] - arr_k2[i]);
				return 0;
			}
			else
			{
				printf("Error: two lines are parallel.\n");
				return -1;
			}
		}
	}

	/*get track crosses by fitted line cross using SSE*/
	//static inline void getHVLineCrossSSE(const float* arr_k1,
	//	const float* arr_k2,
	//	const float* arr_b1,
	//	const float* arr_b2,
	//	const int size,
	//	float* arr_x,
	//	float* arr_y)
	//{
	//	/*set variables and constants*/
	//	const int block_width = 4;
	//	const int block_num = size >> 2;//: / 4
	//	const int remain_num = size % block_width;
	//	int i = 0;
	//	__m128 m_k1, m_k2, m_b1, m_b2, m_x, m_y;

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		/*load data to register*/
	//		m_k1 = _mm_load_ps(arr_k1);
	//		m_k2 = _mm_load_ps(arr_k2);
	//		m_b1 = _mm_load_ps(arr_b1);
	//		m_b2 = _mm_load_ps(arr_b2);

	//		/*calculate x and y*/
	//		m_x = _mm_div_ps(_mm_sub_ps(m_b2, m_b1), _mm_sub_ps(m_k1, m_k2));
	//		m_y = _mm_div_ps(_mm_sub_ps(_mm_mul_ps(m_k1, m_b2), _mm_mul_ps(m_k2, m_b1)),
	//			_mm_sub_ps(m_k1, m_k2));

	//		/*move data to memory*/
	//		_mm_store_ps(arr_x, m_x);
	//		_mm_store_ps(arr_y, m_y);

	//		/*move input and output ptr*/
	//		arr_k1 += block_width;
	//		arr_k2 += block_width;
	//		arr_b1 += block_width;
	//		arr_b2 += block_width;
	//		arr_x += block_width;
	//		arr_y += block_width;
	//	}

	//	/*process remain*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		arr_x[i] = (arr_b2[i] - arr_b1[i]) / (arr_k1[i] - arr_k2[i]);
	//		arr_y[i] = (arr_k1[i] * arr_b2[i] - arr_k2[i] * arr_b1[i])
	//			/ (arr_k1[i] - arr_k2[i]);
	//	}
	//}

	//static inline void getHVLineCrossAVX(const float* arr_k1,
	//	const float* arr_k2,
	//	const float* arr_b1,
	//	const float* arr_b2,
	//	const int size,
	//	float* arr_x,
	//	float* arr_y)
	//{
	//	/*set variable and constants*/
	//	const int block_width = 8;
	//	const int block_num = size >> 3;//: / 8
	//	const int remain_num = size % block_width;
	//	int i = 0;
	//	__m256 m_k1, m_k2, m_b1, m_b2, m_x, m_y;

	//	/*AVX batch processing*/
	//	for (i = 0; i < block_num; ++i)
	//	{
	//		/*load data to register*/
	//		m_k1 = _mm256_load_ps(arr_k1);
	//		m_k2 = _mm256_load_ps(arr_k2);
	//		m_b1 = _mm256_load_ps(arr_b1);
	//		m_b2 = _mm256_load_ps(arr_b2);

	//		/*calculate x and y*/
	//		m_x = _mm256_div_ps(_mm256_sub_ps(m_b2, m_b1), _mm256_sub_ps(m_k1, m_k2));
	//		m_y = _mm256_div_ps(_mm256_sub_ps(_mm256_mul_ps(m_k1, m_b2), _mm256_mul_ps(m_k2, m_b1)),
	//			_mm256_sub_ps(m_k1, m_k2));

	//		/*move data to memory*/
	//		_mm256_store_ps(arr_x, m_x);
	//		_mm256_store_ps(arr_y, m_y);

	//		/*move input and output ptr*/
	//		arr_k1 += block_width;
	//		arr_k2 += block_width;
	//		arr_b1 += block_width;
	//		arr_b2 += block_width;
	//		arr_x += block_width;
	//		arr_y += block_width;
	//	}

	//	/*process remain*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		arr_x[i] = (arr_b2[i] - arr_b1[i]) / (arr_k1[i] - arr_k2[i]);
	//		arr_y[i] = (arr_k1[i] * arr_b2[i] - arr_k2[i] * arr_b1[i])
	//			/ (arr_k1[i] - arr_k2[i]);
	//	}
	//}

	static int getLineParamsAdjustR(cv::Mat& img, const cv::Point& pt_in, int& R, std::pair<std::pair<float, float>, std::pair<float, float>>& lines_params);

	/*correct track cross points by line fitting of DNB points(gravity corrected)*/
	static int correctByLinefit(cv::Mat& img, const cv::Point& pt_in, int& R, cv::Point2f& pt_out);

	/*correct track cross points by line fitting of DNB points(use integral)*/
	static int correctByLinefitInteg(cv::Mat& img,
		const cv::Mat& integ,
		const cv::Point& pt_in,
		int& R,
		cv::Point2f& pt_out);

	/*process local area*/
	static int filterLocalAndCorrect(const cv::Mat& img_in,
		const cv::Point& pt_in, 
		int& R, 
		const int flag,
		cv::Point& pt_out);

	/*score track crosses*/
	//static double scoreTrackCrosses(const std::vector<cv::Point>& coarse_points, const GravityTrackCrossLocator* locator);

	/*get block ids by track cross id*/
	inline static int getBlockIDsByTCID(const int tc_id, std::vector<int>& block_ids)
	{
		const int row = tc_id / 9;
		const int col = tc_id % 9;
		int x_1 = -1, x_2 = -1, y_1 = -1, y_2 = -1;//initial value
		if (row - 1 < 0)
			y_1 = row;
		else if (row + 1 > 8)
			y_1 = row - 1;
		else if (row - 1 >= 0 && row + 1 <= 8)
		{
			y_1 = row - 1;
			y_2 = row;
		}
		if (col - 1 < 0)
			x_1 = col;
		else if (col + 1 > 8)
			x_1 = col - 1;
		else if (col - 1 >= 0 && col + 1 <= 8)
		{
			x_1 = col - 1;
			x_2 = col;
		}

		block_ids.clear();//clear before loading
		if (y_1 != -1)
		{
			if (x_1 != -1)
				block_ids.push_back((y_1 << 3) + x_1);//y_1 * 8
			if (x_2 != -1)
				block_ids.push_back((y_1 << 3) + x_2);//y_1 * 8
		}
		if (y_2 != -1)
		{
			if (x_1 != -1)
				block_ids.push_back((y_2 << 3) + x_1);//y_2 * 8
			if (x_2 != -1)
				block_ids.push_back((y_2 << 3) + x_2);//y_2 * 8
		}
		return 0;
	}
	inline static int getBlockIDsByTCID(const int tc_id, uint(&block_ids)[4])
	{
		const int row = tc_id / 9;
		const int col = tc_id % 9;
		int x_1 = -1, x_2 = -1, y_1 = -1, y_2 = -1;//initial value
		if (row - 1 < 0)
			y_1 = row;
		else if (row + 1 > 8)
			y_1 = row - 1;
		else if (row - 1 >= 0 && row + 1 <= 8)
		{
			y_1 = row - 1;
			y_2 = row;
		}
		if (col - 1 < 0)
			x_1 = col;
		else if (col + 1 > 8)
			x_1 = col - 1;
		else if (col - 1 >= 0 && col + 1 <= 8)
		{
			x_1 = col - 1;
			x_2 = col;
		}

		if (y_1 != -1)
		{
			if (x_1 != -1)
				block_ids[0] = (y_1 << 3) + x_1;//y_1 * 8
			if (x_2 != -1)
				block_ids[1] = (y_1 << 3) + x_2;//y_1 * 8
		}
		if (y_2 != -1)
		{
			if (x_1 != -1)
				block_ids[2] = (y_2 << 3) + x_1;//y_2 * 8
			if (x_2 != -1)
				block_ids[3] = (y_2 << 3) + x_2;//y_2 * 8
		}
		return 0;
	}

	/*get inside 49 track crosses block IDs*/
	inline static int get4InBlkIDsByTCID(const int tc_id, std::vector<int>& block_ids)
	{
		const int row = tc_id / 9;
		const int col = tc_id % 9;
		//presume: row - 1 >= 0 && row + 1 <= 8 && col - 1 >= 0 && col + 1 <= 8
		int y_1 = row - 1
			, y_2 = row
			, x_1 = col - 1
			, x_2 = col;
		block_ids.clear();//clear before loading
		if (y_1 != -1)
		{
			if (x_1 != -1)
				block_ids.push_back((y_1 << 3) + x_1);//y_1 * 8
			if (x_2 != -1)
				block_ids.push_back((y_1 << 3) + x_2);//y_1 * 8
		}
		if (y_2 != -1)
		{
			if (x_1 != -1)
				block_ids.push_back((y_2 << 3) + x_1);//y_2 * 8
			if (x_2 != -1)
				block_ids.push_back((y_2 << 3) + x_2);//y_2 * 8
		}
		return 0;
	}
	inline static int get4InBlkIDsByTCID(const int tc_id, uint(&block_ids)[4])
	{
		const int row = tc_id / 9;
		const int col = tc_id % 9;
		//presume: row - 1 >= 0 && row + 1 <= 8 && col - 1 >= 0 && col + 1 <= 8
		int y_1 = row - 1
			, y_2 = row
			, x_1 = col - 1
			, x_2 = col;
		if (y_1 != -1)
		{
			if (x_1 != -1)
				block_ids[0] = (y_1 << 3) + x_1;//y_1 * 8
			if (x_2 != -1)
				block_ids[1] = (y_1 << 3) + x_2;//y_1 * 8
		}
		if (y_2 != -1)
		{
			if (x_1 != -1)
				block_ids[2] = (y_2 << 3) + x_1;//y_2 * 8
			if (x_2 != -1)
				block_ids[3] = (y_2 << 3) + x_2;//y_2 * 8
		}
		return 0;
	}
	//tc_id is within 81 of total 121
	inline static void get4BlkIDsByTCID(const int tc_id, uint(&block_ids)[4])
	{
		const int row = tc_id / 11;
		const int col = tc_id % 11;
		int y_1 = row - 1
		  , y_2 = row
		  , x_1 = col - 1
		  , x_2 = col;
		block_ids[0] = y_1 * 10 + x_1;
		block_ids[1] = y_1 * 10 + x_2;
		block_ids[2] = y_2 * 10 + x_2;
		block_ids[3] = y_2 * 10 + x_1;
	}

	template<typename T>
	inline static void outputVectWithNum(const std::vector<T>& vect,
		const std::string& path = "f:/vect_num.txt")
	{
		const size_t size = vect.size();
		if (size == 0)
			return;
		std::ofstream out;
		out.open(path, std::ios::out);
		for (size_t i = 0; i < size; ++i)
			out << i << "\t" << vect[i] << "\n";
		out.close();
	}

	template<typename T>
	inline static void outputVectWithNum(const T* vect, const size_t size,
		const std::string& path = "f:/vect_num.txt")
	{
		if (size == 0)
			return;
		std::ofstream out;
		out.open(path, std::ios::out);
		for (size_t i = 0; i < size; ++i)
		{
			out << i << "\t" << vect[i] << "\n";
		}
		out.close();
	}


	template<typename T>
	inline static void outputVect(const T* vect, const size_t size,
		const std::string& path = "f:/vect.txt")
	{
		if (size == 0)
			return;
		std::ofstream out;
		out.open(path, std::ios::out);
		for (size_t i = 0; i < size; ++i)
		{
			out << vect[i] << "\n";
		}
		out.close();
	}

	template<typename T>
	inline static void outputVect(const std::vector<T>& vect,
		const std::string& path = "f:/vect.txt")
	{
		const size_t SIZE = vect.size();
		if (SIZE == 0)
			return;
		std::ofstream out;
		out.open(path, std::ios::out);
		for (size_t i = 0; i < SIZE; ++i)
		{
			out << vect[i] << "\n";
		}
		out.close();
	}

	inline static void outputIntensity(const cv::Mat& img, const std::string path = "f:/intensity.txt")
	{
		const size_t SIZE = size_t(img.cols * img.rows);
		std::ofstream out;
		out.open(path, std::ios::out);
		for (size_t i = 0; i < SIZE; ++i)
		{
			out << (int)*(img.data + i) << "\n";
		}
		out.close();
	}

	inline static void outputIntensityFloat(const cv::Mat& img,
		const std::string& path = "f:/intensity.txt")
	{
		std::ofstream out;
		out.open(path, std::ios::out);
		for (uint i = 0; i < img.rows; ++i)
		{
			auto ptr = img.ptr<float>(i);
			for (uint j = 0; j < img.cols; ++j)
			{
				out << ptr[j] << "\n";
			}
		}
		out.close();
	}

	/*output track crosses to f for testing:.tc.txt*/
	inline static void outputPts(const std::vector<cv::Point>&
		pts_pixel, const std::string& path = "f:/pts.txt")
	{
		std::ofstream out_tc;
		out_tc.open(path, std::ios::out);
		const uint size = (uint)pts_pixel.size();
		for (uint i = 0; i < size; ++i)
			out_tc << pts_pixel[i].x << "\t" << pts_pixel[i].y << "\n";
		out_tc.close();
	}
	inline static void outputPts(const std::vector<cv::Point2f>& pts,
		const std::string& path = "f:/pts.txt")
	{
		std::ofstream out_tc;
		out_tc.open(path, std::ios::out);
		const int pts_size = (int)pts.size();
		for (int i = 0; i < pts_size; ++i)
			out_tc << pts[i].x << "\t" << pts[i].y << "\n";
		out_tc.close();
	}
	//output 2D block dnb pts
	inline static void output2DBlockPts(const std::vector<std::vector<cv::Point2f>>& dnbs2d,
		const std::string& path = "f:/pts.txt")
	{
		const uint Y_LIMIT = (uint)dnbs2d.size();
		const uint X_LIMIT = (uint)dnbs2d[0].size();
		if (Y_LIMIT == 0 || X_LIMIT == 0)
		{
			printf("[Error]: input points are empty.\n");
			return;
		}
		std::ofstream out;
		out.open(path, std::ios::out);
		for (uint y = 0; y < Y_LIMIT; ++y)
		{
			for (uint x = 0; x < X_LIMIT; ++x)
			{
				out << dnbs2d[y][x].x << "\t" << dnbs2d[y][x].y << "\n";
			}
		}
		out.close();
	}

	/*output points intensities*/
	static inline void outputIntensity(const cv::Mat& img,
		const std::vector<cv::Point>& pts_in, const std::string& file_path)
	{//presume input img.type() == CV_32F
		std::ofstream out;
		out.open(file_path, std::ios::out);
		const int pts_size = (int)pts_in.size();
		for (int i = 0; i < pts_size; ++i)
			out << i + 1 << "\t" << img.at<float>(pts_in[i].y, pts_in[i].x) << "\n";
		out.close();
	}

	static inline void outputDNBPoints(const std::vector<cv::Point2f>& dnb_pts_h, const std::vector<cv::Point2f>& dnb_pts_v, const std::string& path)
	{
		std::ofstream out;
		out.open(path, std::ios::out);
		const int h_size = (int)dnb_pts_h.size(),
			v_size = (int)dnb_pts_v.size();
		for (int i = 0; i < h_size; ++i)
			out << dnb_pts_h[i].x << "\t" << dnb_pts_h[i].y << "\n";
		out << "\n";
		for (int i = 0; i < v_size; ++i)
			out << dnb_pts_v[i].x << "\t" << dnb_pts_v[i].y << "\n";
		out.close();
	}

	static inline void outputIntensity(const cv::Mat& img, const std::vector<cv::Point>& pts_in, const char* file_path)
	{//presume input img.type() == CV_32F
		std::ofstream out;
		out.open(file_path, std::ios::out);
		const int pts_size = (int)pts_in.size();
		for (int i = 0; i < pts_size; ++i)
			out << i + 1 << "\t" << img.at<float>(pts_in[i].y, pts_in[i].x) << "\n";
		out.close();
	}

	static inline int getSortedIntensities(const cv::Mat& img, std::vector<ushort>& intensities)
	{//get intensities and sort
		int height = img.rows;
		int width = img.cols;
		if (img.isContinuous())
		{
			width *= height;
			height = 1;
		}
		intensities.resize(height * width, 0);
		int count = 0;
		for (int row = 0; row < height; ++row)
		{
			const ushort* row_data = img.ptr<ushort>(row);
			for (int col = 0; col < width; ++col)
				intensities[count++] = row_data[col];
		}
		std::sort(intensities.begin(), intensities.end());
		return 0;
	}

	static inline void outputIntensity(const cv::Mat& img, const char* file_path)
	{//presume img.type() == CV_16UC1, do sort and output
		std::vector<ushort> intensities;
		Utils::getSortedIntensities(img, intensities);
		const int pts_size = (int)intensities.size();
		std::ofstream out;
		out.open(file_path, std::ios::out);
		for (int i = 0; i < pts_size; ++i)
			out << i + 1 << "\t" << intensities[i] << "\n";
		out.close();
	}

	/*get parallelogram distortion*/
	static inline float getParallelogramDistortion(const std::vector<cv::Point2f>& vertices)
	{//assume that input vertices's size() == 4
		//if (vertices.size() != 4)
		//{
		//	printf("input vertices wrong.\n");
		//	return -1;
		//}
		const float x_diff = vertices[0].x + vertices[2].x - vertices[1].x - vertices[3].x,
			y_diff = vertices[0].y + vertices[2].y - vertices[1].y - vertices[3].y;
		return std::sqrt(x_diff * x_diff + y_diff * y_diff);
	}
	static inline float getParallelogramDistortion(const std::vector<cv::Point>& vertices)
	{//assume that input vertices's size() == 4
		//if (vertices.size() != 4)
		//{
		//	printf("input vertices wrong.\n");
		//	return -1;
		//}
		const float x_diff = vertices[0].x + vertices[2].x - vertices[1].x - vertices[3].x,
			y_diff = vertices[0].y + vertices[2].y - vertices[1].y - vertices[3].y;
		return std::sqrt(x_diff * x_diff + y_diff * y_diff);
	}

	/*get given track cross distortion based on built blocks*/
	//static double getTrackCrossDistortion(const int tc_id, const std::vector<FOVBlock>& blocks);
	static double getTrackCrossDistortion(const int tc_id, const std::vector<std::vector<cv::Point>>& blocks);
	static double getTrackCrossDistortion(const int tc_id, const std::vector<std::vector<cv::Point2f>>& blocks);

	/*get DNB background threshold*/
	static int getDNBBackgroundThreshold(const cv::Mat& img, const cv::Point& pt_in, const int flag, int& R, float& dnb_bg_th);
	static int getDNBBackgroundThreshold(const cv::Mat& img, const cv::Point2f& pt_in, const int flag, int& R, float& dnb_bg_th);
	static int getDNBBackgroundThreshold(const cv::Mat& img, std::vector<cv::Point>& points, float& dnb_bg_th);

	/*get peak point background*/
	inline static float getPeakPtBackground(const cv::Mat& img, 
		const cv::Point& pt_in)
	{
		return MIN(MIN(img.at<float>(pt_in.y - 1, pt_in.x - 1),
			img.at<float>(pt_in.y - 1, pt_in.x + 1)),
			MIN(img.at<float>(pt_in.y + 1, pt_in.x - 1),
			img.at<float>(pt_in.y + 1, pt_in.x + 1)));
	}
	inline static float getPeakPtBackground(const cv::Mat& img,
		const uint& x,
		const uint& y)
	{
		return MIN(MIN(img.at<float>(y - 1, x - 1),
			img.at<float>(y - 1, x + 1)),
			MIN(img.at<float>(y + 1, x - 1),
			img.at<float>(y + 1, x + 1)));
	}

	/*calculate DNB background*/
	static inline int getDNBBackground(cv::Mat& img, 
		const std::pair<cv::Point, cv::Point>& diag_pts, float& background)
	{//presume that input image's not empty and pt_in and surrounding are within the image and image's type == CV_32F
		std::vector<float> pts_vals;
		for (int y = diag_pts.first.y; y <= diag_pts.second.y; ++y)
		{
			for (int x = diag_pts.first.x; x <= diag_pts.second.x; ++x)
				pts_vals.push_back(img.at<float>(y, x));
		}
		background = *std::min_element(pts_vals.begin(), pts_vals.end());
		return 0;
	}

	inline static void getDNBBackground(const cv::Mat& img, const cv::Point& pt_in, float& background)
	{//presume that input image's not empty and pt_in and surrounding are within the image and image's type == CV_32F
		background = MIN(MIN(img.at<float>(pt_in.y - 1, pt_in.x - 1), img.at<float>(pt_in.y - 1, pt_in.x + 1)),
			MIN(img.at<float>(pt_in.y + 1, pt_in.x - 1), img.at<float>(pt_in.y + 1, pt_in.x + 1)));
	}

	inline static void getDNBBG(const cv::Mat& img, const cv::Point& pt_in, float& background)
	{//presume that input image's not empty and pt_in and surrounding are within the image and image's type == CV_32F
		float background_sqrt2 = MIN(MIN(img.at<float>(pt_in.y - 1, pt_in.x - 1), 
			img.at<float>(pt_in.y - 1, pt_in.x + 1)),
			MIN(img.at<float>(pt_in.y + 1, pt_in.x - 1), img.at<float>(pt_in.y + 1, pt_in.x + 1))),
			background_1 = MIN(MIN(img.at<float>(pt_in.y - 1, pt_in.x), img.at<float>(pt_in.y + 1, pt_in.x)),
			MIN(img.at<float>(pt_in.y, pt_in.x - 1), img.at<float>(pt_in.y, pt_in.x + 1)));
		background = MIN(background_1, background_sqrt2);
	}

	inline static void extendArm(const cv::Point2f& in,
		const cv::Point2f& border, float in_dist, float out_dist, cv::Point2f& out)
	{
		float innerWeight = -(float)out_dist / in_dist;
		float borderWeight = (1 + (float)out_dist / in_dist);
		out = innerWeight * in + borderWeight * border;
	}

	/*get 4 points of 2*2 DNB*/
	static inline int getDNB4Points(const cv::Mat& img,
		const cv::Point& pt_in,
		std::vector<cv::Point>& pts_out,
		const int L = 100)
	{
		const int width = img.cols;
		const int height = img.rows;
		if (pt_in.x < 0 || pt_in.x > width
			|| pt_in.y < 0 || pt_in.y > height
			|| pt_in.x - 1 < 0 || pt_in.x + 2 > width
			|| pt_in.y - 1 < 0 || pt_in.y + 2 > height)
		{
			printf("[Error]: point out of boundary @<getDNB4Points>.\n");
			return -1;
		}

		cv::Point pt_leftup, pt_rightup, pt_rightdown, pt_leftdown;
		cv::Mat line_up, line_down, line_left, line_right;
		int up_val, down_val, left_val, right_val;

		line_up = img(cv::Range(pt_in.y - 1, pt_in.y), cv::Range(MAX(pt_in.x - L, 0), MIN(pt_in.x + L, width)));
		line_down = img(cv::Range(pt_in.y + 1, pt_in.y + 2), cv::Range(MAX(pt_in.x - L, 0), MIN(pt_in.x + L, width)));
		line_left = img(cv::Range(MAX(pt_in.y - L, 0), MIN(pt_in.y + L, height)), cv::Range(pt_in.x - 1, pt_in.x));
		line_right = img(cv::Range(MAX(pt_in.y - L, 0), MIN(pt_in.y + L, height)), cv::Range(pt_in.x + 1, pt_in.x + 2));
		up_val = int(cv::sum(line_up)[0]);
		down_val = int(cv::sum(line_down)[0]);
		left_val = int(cv::sum(line_left)[0]);
		right_val = int(cv::sum(line_right)[0]);

		if (up_val > down_val)
		{
			pt_leftup.y = pt_in.y - 1;
			pt_rightup.y = pt_in.y - 1;
			pt_rightdown.y = pt_in.y;
			pt_leftdown.y = pt_in.y;
		}
		else
		{
			pt_leftup.y = pt_in.y;
			pt_rightup.y = pt_in.y;
			pt_rightdown.y = pt_in.y + 1;
			pt_leftdown.y = pt_in.y + 1;
		}
		if (left_val > right_val)
		{
			pt_leftup.x = pt_in.x - 1;
			pt_rightup.x = pt_in.x;
			pt_rightdown.x = pt_in.x;
			pt_leftdown.x = pt_in.x - 1;
		}
		else
		{
			pt_leftup.x = pt_in.x;
			pt_rightup.x = pt_in.x + 1;
			pt_rightdown.x = pt_in.x + 1;
			pt_leftdown.x = pt_in.x;
		}

		pts_out.resize(4);
		pts_out[0] = pt_leftup;    //0
		pts_out[1] = pt_rightup;   //1
		pts_out[2] = pt_rightdown; //2
		pts_out[3] = pt_leftdown;  //3

		return 0;
	}

	/*get DNB 4 points by integral*/
	static inline int getDNB4PointsInteg(const cv::Mat& integral,
		const cv::Point& pt_in,
		std::vector<cv::Point>& pts_out,
		const int width,
		const int height,
		const int L = 100)
	{
		if (pt_in.x < 0 || pt_in.x > width || pt_in.y < 0 || pt_in.y > height
			|| pt_in.x - 1 < 0 || pt_in.x + 2 > width
			|| pt_in.y - 1 < 0 || pt_in.y + 2 > height)
		{
			printf("[Warning]: out of boundary @<getDNB4PointsInteg>.\n");			
			return -1;
		}

		cv::Point pt_leftup, pt_rightup, pt_rightdown, pt_leftdown;
		cv::Mat line_up, line_down, line_left, line_right;
		int up_val, down_val, left_val, right_val;

		up_val = Utils::getRoiSum(integral, cv::Range(MAX(pt_in.x - L, 0),
			MIN(pt_in.x + L, width)),
			cv::Range(pt_in.y - 1, pt_in.y));
		down_val = Utils::getRoiSum(integral,
			cv::Range(MAX(pt_in.x - L, 0), MIN(pt_in.x + L, width)),
			cv::Range(pt_in.y + 1, pt_in.y + 2));
		left_val = Utils::getRoiSum(integral,
			cv::Range(pt_in.x - 1, pt_in.x),
			cv::Range(MAX(pt_in.y - L, 0), MIN(pt_in.y + L, height)));
		right_val = Utils::getRoiSum(integral,
			cv::Range(pt_in.x + 1, pt_in.x + 2),
			cv::Range(MAX(pt_in.y - L, 0), MIN(pt_in.y + L, height)));

		if (up_val >= down_val)
		{
			pt_leftup.y = pt_in.y - 1;
			pt_rightup.y = pt_in.y - 1;
			pt_rightdown.y = pt_in.y;
			pt_leftdown.y = pt_in.y;
		}
		else
		{
			pt_leftup.y = pt_in.y;
			pt_rightup.y = pt_in.y;
			pt_rightdown.y = pt_in.y + 1;
			pt_leftdown.y = pt_in.y + 1;
		}
		if (left_val >= right_val)
		{
			pt_leftup.x = pt_in.x - 1;
			pt_rightup.x = pt_in.x;
			pt_rightdown.x = pt_in.x;
			pt_leftdown.x = pt_in.x - 1;
		}
		else
		{
			pt_leftup.x = pt_in.x;
			pt_rightup.x = pt_in.x + 1;
			pt_rightdown.x = pt_in.x + 1;
			pt_leftdown.x = pt_in.x;
		}

		pts_out.resize(4);
		pts_out[0] = pt_leftup;    //0
		pts_out[1] = pt_rightup;   //1
		pts_out[2] = pt_rightdown; //2
		pts_out[3] = pt_leftdown;  //3

		return 0;
	}

	/*statistics on point and the fitting beeline distance*/
	static inline int getMeanDistToLine(const std::vector<cv::Point>& pts_in,
		std::pair<float, float>& line_params,
		const int flag,
		float& dist)
	{
		const int pts_size = (int)pts_in.size();
		if (pts_size == 0 || line_params.first == 0
			|| line_params.second == 0 || (flag != 0 && flag != 1))
		{
			printf("Error: input params are wrong.\n");
			return -1;
		}

		float dist_sum = 0.0f;
		if (flag == 0)//horizontal
		{
			for (int i = 0; i < pts_size; ++i)
				dist_sum += std::abs(pts_in[i].y - line_params.first * pts_in[i].x - line_params.second);

			dist = dist_sum / (float)pts_size;
		}
		else//if(flag == 1)//vertical
		{
			for (int i = 0; i < pts_size; ++i)
				dist_sum += std::abs(pts_in[i].x - line_params.first * pts_in[i].y - line_params.second);

			dist = dist_sum / pts_size;
		}

		return 0;
	}

	/*get two un_paralle line equation by two points*/
	static inline int getLineCross(const std::pair<float, float>& line_params_1, 
		const std::pair<float, float>& line_params_2,
		cv::Point2f& pt_cross)
	{
		if (line_params_1.first == line_params_2.first)//first:k, second:b
		{
			printf("[Error]: the two parallel lines.\n");
			return -1;
		}
		if (line_params_1.first == 9999.0f)
		{
			pt_cross.x = line_params_1.second;
			pt_cross.y = line_params_2.first * pt_cross.x + line_params_2.second;
		}
		else if (line_params_2.first == 9999.0f)
		{
			pt_cross.x = line_params_2.second;
			pt_cross.y = line_params_1.first * pt_cross.x + line_params_1.second;
		}
		else
		{
			//x = (b2-b1)/(k1-k2), y = (k1b2-k2b1)/(k1-k2)) 
			pt_cross.x = (float)(line_params_2.second - line_params_1.second) 
				/ (float)(line_params_1.first - line_params_2.first);
			pt_cross.y = (float)(line_params_1.first * line_params_2.second
				- line_params_2.first * line_params_1.second) 
				/ (float)(line_params_1.first - line_params_2.first);
		}
		return 0;
	}

	//line_param.first = 9999.0f;
	//line_param.second = two_pts.first.x;
	//printf("[Warning]: vertical line");
	//return -1;
	static inline int getLineParamBy2Points(const std::pair<cv::Point, cv::Point>& two_pts, std::pair<float, float>& line_param)
	{
		if (two_pts.first.x == two_pts.second.x)//how to handle vertical line
			line_param.first = float(two_pts.second.y - two_pts.first.y) / 0.1f;
		else
			line_param.first = float(two_pts.second.y - two_pts.first.y)
				/ float(two_pts.second.x - two_pts.first.x);
		line_param.second = two_pts.first.y - line_param.first * two_pts.first.x;
		return 0;
	}

	static inline int getCrossBy4Points(const std::pair<cv::Point, cv::Point>& two_pts_1,
		const std::pair<cv::Point, cv::Point>& two_pts_2,
		cv::Point2f& pt_out)
	{
		std::pair<float, float> line_param_1, line_param_2;
		int ret = Utils::getLineParamBy2Points(two_pts_1, line_param_1);
		if (ret < 0) return ret;
		ret = Utils::getLineParamBy2Points(two_pts_2, line_param_2);
		if (ret < 0) return ret;
		Utils::getLineCross(line_param_1, line_param_2, pt_out);
		return 0;
	}

	static inline void roundPoint2f(const cv::Point2f& pt_in, cv::Point& pt_out)
	{
		pt_out.x = pt_in.x > 0 ? int(pt_in.x + 0.5f) : (pt_in.x < 0 ? int(pt_in.x - 0.5f) : 0);
		pt_out.y = pt_in.y > 0 ? int(pt_in.y + 0.5f) : (pt_in.y < 0 ? int(pt_in.y - 0.5f) : 0);
	}

#if defined(_WIN32)
	inline static void getFiles(const std::string& path,
		const std::string& ext, std::vector<std::string>& files)
	{
		long file_handle = 0;
		struct _finddata_t file_info;
		std::string path_name, ext_name;
		if (strcmp(ext.c_str(), "") != 0)//if not empty
			ext_name = "\\*" + ext;
		else
			ext_name = "\\*";

		if ((file_handle = _findfirst(path_name.assign(path).append(ext_name).c_str(), &file_info)) != -1)
		{
			do
			{
				if (file_info.attrib & _A_SUBDIR)
				{
					if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
					{
						Utils::getFiles(path_name.assign(path).append("\\").append(file_info.name), ext, files);
					}
				}
				else
				{
					if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
					{
						files.push_back(path_name.assign(path).append("\\").append(file_info.name));
						//files.push_back(file_info.name);
					}
				}
			} while (_findnext(file_handle, &file_info) == 0);
			_findclose(file_handle);
		}
	}
#endif

	//inline static void getFilesCV(const std::string& path,
	//	const std::string& ext, std::vector<std::string>& files)
	//{
	//	cv::Directory dir;
	//	files = dir.GetListFiles(path, ext, true);
	//}

	static void testCVIntegral(const cv::Mat& img_in)
	{
		cv::Mat img;
		if (img_in.type() != CV_8U)
			img_in.convertTo(img, CV_8U);

		clock_t time_1, time_0 = clock();
		cv::Mat integral;
		for (int i = 0; i < 100; ++i)
		{
			cv::integral(img, integral, CV_32S);
		}
		time_1 = clock();
		printf("cv integrate time: %.3fms\n", (float)(time_1 - time_0) / CLOCKS_PER_SEC * 1000);
		printf("cv integral last data: %d\n", integral.at<int>(1959, 1959));

		//cv::Mat integral_norm;
		//cv::normalize(integral, integral_norm, 0, 65000, cv::NORM_MINMAX);//can be customized?
		////cv::convertScaleAbs(integral, integral_norm);//convert to 8 bit unsigned int
		//cv::Mat img_save;
		//integral_norm.convertTo(img_save, CV_16UC1);
		//printf("cv integral norm last data: %d\n", integral_norm.at<int>(1959, 1959));
		//cv::imwrite("d:/cv_integral_norm.tif", /*integral_norm*/img_save);
	}

	/*test my integral*/
	static inline void testMyIntegral_1(const cv::Mat& img_in)
	{
		cv::Mat img;
		if (img_in.type() != CV_8U)
			img_in.convertTo(img, CV_8U);
		const int width = img.cols
			, height = img.rows;
		auto integral = new int[width*height + 1];

		clock_t time_1, time_0 = clock();
		for (int i = 0; i < 100; ++i)
		{
			Utils::getImgIntegral_1(img_in.data, width, height, integral);
		}
		time_1 = clock();
		printf("my integrate_1 time: %.3fms\n", (float)(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

		/*
		printf("my integral_1 last data: %d\n", integral[height*width - 1]);

		cv::Mat integral_img(cv::Size(1960, 1960), CV_32SC1, integral);
		cv::Mat integral_norm;
		cv::normalize(integral_img, integral_norm, 0, 255, cv::NORM_MINMAX);
		//integral_norm.convertTo(integral_norm, CV_8UC1);
		cv::imwrite("d:/my_integral_norm_1.tif", integral_norm);

		cv::waitKey(0);*/

		delete[] integral;
		integral = NULL;
	}

	static inline void testMyIntegral_2(const cv::Mat& img_in)
	{
		cv::Mat img;
		if (img_in.type() != CV_8U)
			img_in.convertTo(img, CV_8U);
		const int width = img.cols
			, height = img.rows;
		auto integral = new int[width*height + 1];

		clock_t time_1, time_0 = clock();
		for (int i = 0; i < 100; ++i)
		{
			Utils::getImgIntegral_2(img_in.data, width, height, integral);
		}
		time_1 = clock();
		printf("my integrate_2 time: %.3fms\n", (float)(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

		/*
		printf("my integral_2 last data: %d\n", integral[height*width - 1]);

		cv::Mat integral_img(cv::Size(1960, 1960), CV_32SC1, integral);
		cv::Mat integral_norm;
		cv::normalize(integral_img, integral_norm, 0, 255, cv::NORM_MINMAX);
		//integral_norm.convertTo(integral_norm, CV_8UC1);
		cv::imwrite("d:/my_integral_norm_2.tif", integral_norm);

		cv::waitKey(0);*/

		delete[] integral;
		integral = NULL;
	}

	/*test other's optimized method 1*/
	static inline void testOtherIntegral_1(const cv::Mat& img_in)
	{
		cv::Mat img;
		if (img_in.type() != CV_8U)
			img_in.convertTo(img, CV_8U);
		const int width = img.cols
			, height = img.rows;
		auto integral = new int[(width + 1)*(height + 1)];

		clock_t time_1, time_0 = clock();
		for (int i = 0; i < 100; ++i)
		{
			Utils::getIntegralImg_1(img_in.data,
				img_in.cols, img_in.rows, sizeof(uchar)* width, integral);
		}
		time_1 = clock();
		printf("other integrate time_1: %.3fms\n",
			(float)(time_1 - time_0) / CLOCKS_PER_SEC * 1000);
		printf("other integral last data: %d\n",
			integral[(height + 1)*(width + 1) - 1]);

		//cv::Mat integral_img(cv::Size(1961, 1961), CV_32SC1, integral);
		//cv::Mat integral_norm;
		//cv::normalize(integral_img, integral_norm, 0, 255, cv::NORM_MINMAX);
		////integral_norm.convertTo(integral_norm, CV_8UC1);
		//cv::imwrite("d:/other_integral_norm_1.tif", integral_norm);

		delete[] integral;
		integral = NULL;
	}

	/*test other's optimized method 2*/
	static inline void testOtherIntegral_2(const cv::Mat& img_in)
	{
		cv::Mat img;
		if (img_in.type() != CV_8U)
			img_in.convertTo(img, CV_8U);
		const int width = img.cols
			, height = img.rows;
		auto integral = new int[(width + 1)*(height + 1)];

		clock_t time_1, time_0 = clock();
		for (int i = 0; i < 100; ++i)
		{
			Utils::getIntegralImg_2(img_in.data,
				img_in.cols, img_in.rows, sizeof(uchar)* width, integral);
		}
		time_1 = clock();
		printf("other integrate time_2: %.3fms\n",
			(float)(time_1 - time_0) / CLOCKS_PER_SEC * 1000);
		printf("other integral last data: %d\n",
			integral[(height + 1)*(width + 1) - 1]);

		//cv::Mat integral_img(cv::Size(1961, 1961), CV_32SC1, integral);
		//cv::Mat integral_norm;
		//cv::normalize(integral_img, integral_norm, 0, 255, cv::NORM_MINMAX);
		////integral_norm.convertTo(integral_norm, CV_8UC1);
		//cv::imwrite("d:/other_integral_norm_2.tif", integral_norm);

		delete[] integral;
		integral = NULL;
	}

	/*my calculate integral image function
	* 是否将原图转换成8bit的图像然后求积分图
	* SAT(x, y) = SAT(x - 1, y) + SAT(x, y - 1) - SAT(x - 1, y - 1) + Image(x, y)*/
	static inline void getImgIntegral_1(const uchar* input,
		const int width,
		const int height,
		int* integral)
	{
		/*calculate integral first row*/
		integral[0] = input[0];
		for (int i = 1; i < width; ++i)
		{
			integral[i] = integral[i - 1] + input[i];
		}
		//for (int i = 0; i < width; ++i)
		//{
		//	integral[i] = (int)input[i];
		//	if (i)
		//		integral[i] += integral[i - 1];
		//}

		for (int i = 1; i < height; ++i)
		{
			int offset = i * width;

			//first column of each row
			integral[offset] = integral[offset - width] + input[offset];

			//other columns
			for (int j = 1; j < width; ++j)
			{
				integral[offset + j] = integral[offset + j - 1] + integral[offset + j - width]
					- integral[offset + j - width - 1] + input[offset + j];
			}
		}
	}//对于1960*1960 int表示的uchar像素的积分图表示范围已经够了

	/*my calculate integral image function: this can be optimized*/
	static inline void getImgIntegral_2(const uchar* input,
		const int width,
		const int height,
		int* integral)
	{
		/*calculate integral first row*/
		int* col_sums = (int*)calloc(width, sizeof(int));
		integral[0] = input[0];
		col_sums[0] = input[0];
		for (int i = 1; i < width; ++i)
		{
			integral[i] = integral[i - 1] + input[i];
			col_sums[i] = input[i];
		}
		//for (int i = 0; i < width; ++i)
		//{
		//	integral[i] = (int)input[i];
		//	col_sums[i] = (int)input[i];
		//	if (i)
		//		integral[i] += integral[i - 1];
		//}

		for (int y = 1; y < height; ++y)
		{
			int offset = y * width;

			//first column of each row
			col_sums[0] += input[offset];
			integral[offset] = col_sums[0];

			//other columns
			for (int x = 1; x < width; ++x)
			{
				col_sums[x] += input[offset + x];
				integral[offset + x] = integral[offset + x - 1] + col_sums[x];
			}
		}
		free(col_sums);
		col_sums = NULL;
	}

	/*other's method 2*/
	static inline void getIntegralImg_2(uchar* input,
		const int width,
		const int height,
		const int stride,
		int* integral)
	{
		int* col_sum = (int*)calloc(width, sizeof(int));//auto set to zero
		for (int y = 0; y < height; ++y)
		{
			uchar* y_src = input + y * stride;//stride: num of bytes per row of input
			int* y_integ = integral + (y + 1) * (width + 1) + 1;
			y_integ[-1] = 0;
			for (int x = 0; x < width; ++x)//loop the input
			{
				col_sum[x] += y_src[x];
				y_integ[x] = y_integ[x - 1] + col_sum[x];
			}
		}
		free(col_sum);
		col_sum = NULL;
	}

	/*other's method 1: need to digest?*/
	static inline void getIntegralImg_1(uchar* input,
		const int width,
		const int height,
		const int stride,//SRC stride
		int* integral)
	{
		memset(integral, 0, (width + 1) * sizeof(int));
		for (int y = 0; y < height; ++y)
		{
			uchar* y_src = input + y * stride;
			int* y_pre = integral + y * (width + 1) + 1;//previous row
			int* y_cur = integral + (y + 1) * (width + 1) + 1;//current row
			y_cur[-1] = 0;//first column set to 0
			for (int x = 0, sum = 0; x < width; ++x)//SSE accelerate?
			{
				sum += y_src[x];
				y_cur[x] = y_pre[x] + sum;//update integral
			}
		}
	}

	/*judge even and odd*/
	static inline bool isOdd(int num)
	{
		return num & 0x1;
	}

	/*find min max of a vector*/
	template<typename T>
	static void findMinMax(const T* data,
		const size_t size,
		T& min,
		T& max)
	{
		if ((data) && size > 0)
		{
			if (size == 1)
			{
				min = max = data[0];
			}
			else if (size == 2)
			{
				if (data[0] >= data[1])
				{
					min = data[1];
					max = data[0];
				}
				else
				{
					min = data[0];
					max = data[1];
				}
			}
			else
			{
				/*init min max*/
				if (data[0] >= data[1])
				{
					min = data[1];
					max = data[0];
				}
				else
				{
					min = data[0];
					max = data[1];
				}

				/*compare*/
				if (!Utils::isOdd(size))//even
				{
					for (int i = 2; i < size; i += 2)
					{
						if (data[i] <= data[i + 1])
						{
							if (max < data[i + 1])
								max = data[i + 1];
							if (min > data[i])
								min = data[i];
						}
						else
						{
							if (max < data[i])
								max = data[i];
							if (min > data[i + 1])
								min = data[i + 1];
						}
					}
				}
				else//odd
				{
					const size_t id_end = size - 1;
					for (int i = 2; i < id_end; i += 2)
					{
						if (data[i] <= data[i + 1])
						{
							if (max < data[i + 1])
								max = data[i + 1];
							if (min > data[i])
								min = data[i];
						}
						else
						{
							if (max < data[i])
								max = data[i];
							if (min > data[i + 1])
								min = data[i + 1];
						}
					}
					if (max < data[id_end])
						max = data[id_end];
					else if (min > data[id_end])
						min = data[id_end];
				}
			}
		}
		else
			return;
	}

	/*vector normalization without SIMD acceleration*/
	template<typename T>
	static inline void normVect(const T* in,
		const size_t size,
		uchar* out,
		const T alpha = 0,
		const T beta = 255)
	{//presume: beta > alpha
		/*get min and max element of in*/
		T min, max;
		Utils::findMinMax(in, size, min, max);//can use SSE to accelerate?
		const T span_in = max - min,
			span_out = beta - alpha;//span_in > 0 and span_out > 0
		if (span_in <= 0)
		{
			printf("no need to normalize\n");
			return;
		}

		/*do normalization*/
		for (size_t i = 0; i < size; ++i)
		{
			out[i] = (in[i] - min) / span_in * span_out + alpha;
			//printf("%d\n", (int)out[i]);
		}
	}

	///*square*/
	//static __m128 _mm_square_ps(__m128 x)
	//{
	//	__m128 s, r;
	//	s = _mm_mul_ps(x, x);
	//	r = _mm_add_ss(s, _mm_movehl_ps(s, s));
	//	r = _mm_add_ss(r, _mm_shuffle_ps(r, r, 1));
	//	return r;
	//}

	///*
	//*r = 1 / sqrt(a)
	//*0.5 * rsqrtss * (3 - x * rsqrtss(x) * rsqrtss(x))
	//*/
	//static __m128 _mm_rsqrt(__m128 a)
	//{
	//	static const __m128 _05 = _mm_set1_ps(0.5f);
	//	static const __m128 _3 = _mm_set1_ps(3.f);
	//	__m128 rsqrt = _mm_rsqrt_ss(a);
	//	rsqrt = _mm_mul_ss(_mm_mul_ss(_05, rsqrt),
	//		_mm_sub_ss(_3, _mm_mul_ss(a, _mm_mul_ss(rsqrt, rsqrt))));
	//}

	///*normalize a float vector using SSE*/
	//static inline void normVectSSE(const float* in,
	//	const size_t size,
	//	float* out,
	//	const float alpha = 0,
	//	const float beta = 255)
	//{
	//	/*get min and max element of in*/
	//	float min, max;
	//	Utils::findMinMax(in, size, min, max);
	//	const float span_in = max - min,
	//		span_out = beta - alpha;//span_in > 0 and span_out > 0

	//	/*set constants*/
	//	const int block_width = 4;
	//	const size_t block_num = size >> 2;//: / 4
	//	const size_t remain_num = size % 4;
	//	size_t i = 0;
	//	__m128 m_load, m_data,
	//		m_span_in = _mm_set1_ps(span_in),
	//		m_span_out = _mm_set1_ps(span_out),
	//		m_min = _mm_set1_ps(min),
	//		m_alpha = _mm_set1_ps(alpha);

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load = _mm_load_ps(in);
	//		m_data = _mm_add_ps(m_alpha,
	//			_mm_mul_ps(m_span_out,
	//			_mm_div_ps(_mm_sub_ps(m_load, m_min), m_span_in)));
	//		_mm_store_ps(out, m_data);//from register to memory
	//		in += block_width;
	//		out += block_width;
	//	}

	//	/*process remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		out[i] = (in[i] - min) / span_in * span_out + alpha;
	//	}
	//}

	///*normalize a float vector using SSE unfold 4 times*/
	//static inline void normVectSSEUnfold(const float* in,
	//	const size_t size,
	//	float* out,
	//	const float alpha = 0.0f,
	//	const float beta = 255.0f)
	//{
	//	/*get min and max element of in*/
	//	float min, max;
	//	Utils::findMinMax(in, size, min, max);
	//	const float span_in = max - min,
	//		span_out = beta - alpha;//span_in > 0 and span_out > 0

	//	const size_t block_width = 16;
	//	const size_t block_num = size >> 4;
	//	const size_t remain_num = size % block_width;
	//	__m128 m_load_1, m_load_2, m_load_3, m_load_4,
	//		m_data_1, m_data_2, m_data_3, m_data_4,
	//		m_span_in = _mm_set1_ps(span_in),
	//		m_span_out = _mm_set1_ps(span_out),
	//		m_min = _mm_set1_ps(min),
	//		m_alpha = _mm_set1_ps(alpha);
	//	size_t i = 0;

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		/*load data to register*/
	//		m_load_1 = _mm_load_ps(in);
	//		m_load_2 = _mm_load_ps(in + 4);
	//		m_load_3 = _mm_load_ps(in + 8);
	//		m_load_4 = _mm_load_ps(in + 12);

	//		/*normalize*/
	//		m_data_1 = _mm_add_ps(m_alpha,
	//			_mm_mul_ps(m_span_out,
	//			_mm_div_ps(_mm_sub_ps(m_load_1, m_min), m_span_in)));
	//		m_data_2 = _mm_add_ps(m_alpha,
	//			_mm_mul_ps(m_span_out,
	//			_mm_div_ps(_mm_sub_ps(m_load_2, m_min), m_span_in)));
	//		m_data_3 = _mm_add_ps(m_alpha,
	//			_mm_mul_ps(m_span_out,
	//			_mm_div_ps(_mm_sub_ps(m_load_3, m_min), m_span_in)));
	//		m_data_4 = _mm_add_ps(m_alpha,
	//			_mm_mul_ps(m_span_out,
	//			_mm_div_ps(_mm_sub_ps(m_load_4, m_min), m_span_in)));

	//		/*store data to memory*/
	//		_mm_store_ps(out, m_data_1);
	//		_mm_store_ps(out + 4, m_data_2);
	//		_mm_store_ps(out + 8, m_data_3);
	//		_mm_store_ps(out + 12, m_data_4);

	//		/*move ptr*/
	//		in += block_width;
	//		out += block_width;
	//	}

	//	/*process remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		out[i] = (in[i] - min) / span_in * span_out + alpha;
	//	}
	//}

	///*normalize a float vector using AVX*/
	//static inline void normVectAVX(const float* in,
	//	const size_t size,
	//	float* out,
	//	const float alpha = 0.0f,
	//	const float beta = 255.0f)
	//{
	//	/*get min and max element of in*/
	//	float min, max;
	//	Utils::findMinMax(in, size, min, max);
	//	const float span_in = max - min,
	//		span_out = beta - alpha;//span_in > 0 and span_out > 0

	//	/*set constants*/
	//	const size_t block_width = 8;
	//	const size_t block_num = size >> 3;//size / block_width
	//	const size_t remain_num = size % block_width;
	//	size_t i = 0;
	//	__m256 m_load, m_data,
	//		m_span_in = _mm256_set1_ps(span_in),
	//		m_span_out = _mm256_set1_ps(span_out),
	//		m_min = _mm256_set1_ps(min),
	//		m_alpha = _mm256_set1_ps(alpha);

	//	/*AVX batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load = _mm256_load_ps(in);
	//		m_data = _mm256_add_ps(m_alpha,
	//			_mm256_mul_ps(m_span_out,
	//			_mm256_div_ps(_mm256_sub_ps(m_load, m_min), m_span_in)));
	//		_mm256_store_ps(out, m_data);//from register to memory
	//		in += block_width;
	//		out += block_width;
	//	}

	//	/*process remain*/
	//	for (; i < remain_num; ++i)
	//	{
	//		out[i] = (in[i] - min) / span_in * span_out + alpha;
	//	}
	//}

	///*normalize a float vector using AVX unfold 4 times*/
	//static inline void normVectAVXUnfold(const float* in,
	//	const size_t size,
	//	float* out,
	//	const float alpha = 0.0f,
	//	const float beta = 255.0f)
	//{
	//	/*get min and max element of in*/
	//	float min, max;
	//	Utils::findMinMax(in, size, min, max);
	//	const float span_in = max - min,
	//		span_out = beta - alpha;//span_in > 0 and span_out > 0

	//	/*set constants*/
	//	const size_t block_width = 32;//handle 32 floats a time
	//	const size_t block_num = size >> 5;
	//	const size_t remain_num = size % block_width;
	//	size_t i = 0;
	//	__m256 m_load_1, m_load_2, m_load_3, m_load_4,
	//		m_data_1, m_data_2, m_data_3, m_data_4,
	//		m_span_in = _mm256_set1_ps(span_in),
	//		m_span_out = _mm256_set1_ps(span_out),
	//		m_min = _mm256_set1_ps(min),
	//		m_alpha = _mm256_set1_ps(alpha);

	//	/*AVX batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		/*load data to register*/
	//		m_load_1 = _mm256_load_ps(in);
	//		m_load_2 = _mm256_load_ps(in + 8);
	//		m_load_3 = _mm256_load_ps(in + 16);
	//		m_load_4 = _mm256_load_ps(in + 24);

	//		/*normalize*/
	//		m_data_1 = _mm256_add_ps(m_alpha,
	//			_mm256_mul_ps(m_span_out,
	//			_mm256_div_ps(_mm256_sub_ps(m_load_1, m_min), m_span_in)));
	//		m_data_2 = _mm256_add_ps(m_alpha,
	//			_mm256_mul_ps(m_span_out,
	//			_mm256_div_ps(_mm256_sub_ps(m_load_2, m_min), m_span_in)));
	//		m_data_3 = _mm256_add_ps(m_alpha,
	//			_mm256_mul_ps(m_span_out,
	//			_mm256_div_ps(_mm256_sub_ps(m_load_3, m_min), m_span_in)));
	//		m_data_4 = _mm256_add_ps(m_alpha,
	//			_mm256_mul_ps(m_span_out,
	//			_mm256_div_ps(_mm256_sub_ps(m_load_4, m_min), m_span_in)));

	//		/*store data to memory*/
	//		_mm256_store_ps(out, m_data_1);//from register to memory
	//		_mm256_store_ps(out + 8, m_data_2);
	//		_mm256_store_ps(out + 16, m_data_3);
	//		_mm256_store_ps(out + 24, m_data_4);

	//		/*move ptr*/
	//		in += block_width;
	//		out += block_width;
	//	}

	//	/*process remain*/
	//	for (; i < remain_num; ++i)
	//	{
	//		out[i] = (in[i] - min) / span_in * span_out + alpha;
	//	}
	//}

	///*test normalization*/
	//static inline void testMyNorm(cv::Mat& img_in)
	//{
	//	if (img_in.type() != CV_32F)//why?
	//		img_in.convertTo(img_in, CV_32F);
	//	const size_t size = img_in.cols * img_in.rows;
	//	float* data = (float*)img_in.data;//why?
	//	printf("data[0]: %.2f\n", data[0]);

	//	uchar* out = new uchar[size];
	//	clock_t time_1, time_0 = clock();
	//	Utils::normVect(data, size, out);
	//	time_1 = clock();
	//	printf("norm time: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

	//	cv::Mat img_out(cv::Size(1960, 1960), CV_8UC1, out);
	//	cv::imwrite("d:/test_norm.tif", img_out);

	//	delete[] out;
	//	out = NULL;
	//}

	///*how to accelerate this function?*/
	//static void float2UcharArr(const float* in,
	//	uchar* out,
	//	const size_t size)
	//{
	//	for (size_t i = 0; i < size; ++i)
	//	{
	//		//printf("before: %.2f ", in[i]);
	//		out[i] = (uchar)in[i];
	//		//printf("after: %d\n", out[i]);
	//	}
	//}

	//static inline void testMyNormSSE(cv::Mat& img_in)
	//{
	//	if (img_in.type() != CV_32F)//why?
	//		img_in.convertTo(img_in, CV_32F);
	//	const size_t size = img_in.cols * img_in.rows;
	//	const float* data = (float*)img_in.data;//why?

	//	float* out_sse_float = new float[size];
	//	float* out_avx_float = new float[size];

	//	uchar* out = new uchar[size];
	//	clock_t time_1, time_0 = clock();
	//	Utils::normVect(data, size, out);
	//	time_1 = clock();
	//	printf("norm time: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

	//	time_0 = clock();
	//	Utils::normVectSSE(data, size, out_sse_float);
	//	//Utils::normVectSSEUnfold(data, size, out_sse_float);
	//	time_1 = clock();
	//	printf("norm time SSE: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);
	//	//printf("norm time SSE unfold: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

	//	time_0 = clock();
	//	Utils::normVectAVX(data, size, out_avx_float);
	//	//Utils::normVectAVXUnfold(data, size, out_avx_float);
	//	time_1 = clock();
	//	printf("norm time AVX: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);
	//	//printf("norm time AVX unfold: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

	//	uchar* out_sse_uchar = new uchar[size];
	//	uchar* out_avx_ucahr = new uchar[size];
	//	time_0 = clock();
	//	Utils::float2UcharArr(out_sse_float, out_sse_uchar, size);
	//	time_1 = clock();
	//	printf("convert float arr to uchar arr time: %.3fms\n",
	//		float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);
	//	Utils::float2UcharArr(out_avx_float, out_avx_ucahr, size);

	//	Utils::compArr(out, out_sse_uchar, size);
	//	Utils::compArr(out, out_avx_ucahr, size);

	//	cv::Mat img_out(cv::Size(1960, 1960), CV_8U, out);
	//	cv::Mat img_out_sse(cv::Size(1960, 1960), CV_8U, (uchar*)out_sse_uchar);
	//	cv::imwrite("d:/test_norm.tif", img_out);
	//	cv::imwrite("d:/test_norm_sse.tif", img_out_sse);

	//	delete[] out_sse_float;
	//	out_sse_float = NULL;
	//	delete[] out_avx_float;
	//	out_avx_float = NULL;
	//	delete[] out_avx_ucahr;
	//	out_avx_ucahr = NULL;
	//	delete[] out_sse_uchar;
	//	out_sse_uchar = NULL;
	//	delete[] out;
	//	out = NULL;
	//}

	//static inline void testMyNormAndConvert(cv::Mat& img_in)
	//{
	//	if (img_in.type() != CV_32F)//why?
	//		img_in.convertTo(img_in, CV_32F);
	//	const size_t size = img_in.cols * img_in.rows;
	//	const float* data = (float*)img_in.data;//why?

	//	float* out_avx_float = new float[size];
	//	uchar* out_avx_ucahr = new uchar[size];

	//	clock_t time_0 = clock();
	//	for (int i = 0; i < 100; ++i)
	//	{
	//		//Utils::normVectAVX(data, size, out_avx_float);
	//		Utils::normVectAVXUnfold(data, size, out_avx_float);
	//		Utils::float2UcharArr(out_avx_float, out_avx_ucahr, size);
	//	}
	//	clock_t time_1 = clock();
	//	printf("my norm and convert time: %.3fms\n",
	//		float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

	//	delete[] out_avx_float;
	//	out_avx_float = NULL;
	//	delete[] out_avx_ucahr;
	//	out_avx_ucahr = NULL;
	//}

	static inline void testCVNormAndConvert(cv::Mat& img_in)
	{
		clock_t time_0 = clock();
		for (int i = 0; i < 100; ++i)
		{
			cv::Mat img;
			cv::normalize(img_in, img, 0, 255, cv::NORM_MINMAX);
			img.convertTo(img, CV_8U);
		}
		clock_t time_1 = clock();
		printf("CV norm and convert time: %.3fms\n",
			float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);
	}

	/*compare two array*/
	template<typename T>
	static bool compArr(const T* arr_1,
		const T* arr_2,
		const size_t size)
	{
		for (size_t i = 0; i < size; ++i)
		{
			if (arr_1[i] != arr_2[i])
			{
				printf("*****not equal: i = %d*****\n", i);
				return false;
			}
		}
		printf("*****equal*****\n");
		return true;
	}

	/*compare two mat*/
	inline static bool comp2Mat(const cv::Mat& img_1, const cv::Mat& img_2)
	{//presume: img_1.type() == CV_32F && img_2.type() == CV_32F
		if (img_1.cols != img_2.cols || img_1.rows != img_2.rows)
			return false;
		for (uint y = 0; y < img_1.rows; ++y)
		{
			auto ptr_1 = img_1.ptr<float>(y);
			auto ptr_2 = img_2.ptr<float>(y);
			for (uint x = 0; x < img_1.cols; ++x)
			{
				if (ptr_1[x] != ptr_2[x])
				{
					return false;
				}
			}
		}
		return true;
	}

	/*get sum of image ROI by integral*/
	static inline int getRoiSum(const int* integral,
		const int width,
		const int height,
		const cv::Range& x_range,
		const cv::Range& y_range)
	{
		const int id_0 = y_range.start * width + x_range.start,
			id_1 = y_range.start * width + x_range.end,
			id_2 = y_range.end * width + x_range.end,
			id_3 = y_range.end * width + x_range.start;
		return integral[id_0] + integral[id_2] - integral[id_1] - integral[id_3];
	}

	/*SAT(x, y) = SAT(x - 1, y) + SAT(x, y - 1) - SAT(x - 1, y - 1) + Image(x, y)*/
	static inline float getRoiSum(const cv::Mat& integ,
		const cv::Range& x_range, const cv::Range& y_range)
	{
		return integ.at<float>(y_range.start, x_range.start)
			+ integ.at<float>(y_range.end, x_range.end)
			- integ.at<float>(y_range.start, x_range.end)
			- integ.at<float>(y_range.end, x_range.start);
	}

	/*test get ROI sum by integral*/
	static inline int testRoiSumByIntegral(cv::Mat& img,
		const cv::Range& x_range,
		const cv::Range& y_range)
	{
		if (img.type() != CV_8U)
			img.convertTo(img, CV_8U);

		clock_t time_1, time_0 = clock();
		for (int i = 0; i < 10000; ++i)
		{
			/*const int fisrt = */cv::sum(img(y_range, x_range))[0];
		}
		time_1 = clock();
		printf("normal way run time: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

		cv::Mat integral;
		time_0 = clock();
		cv::integral(img, integral, CV_32S);
		time_1 = clock();
		printf("integral image run time: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

		time_0 = clock();
		for (int i = 0; i < 10000; ++i)
		{
			/*const int second = */Utils::getRoiSum(integral, x_range, y_range);
		}
		time_1 = clock();
		printf("new way run time: %.3fms\n", float(time_1 - time_0) / CLOCKS_PER_SEC * 1000);

		//if (fisrt == second)
		//{
		//	printf("test succeeded.\n");
		//	return 0;
		//}
		//else
		//{
		//	printf("test failed.\n");
		//	return -1;
		//}
		return 0;
	}

	/*
	*math optimization of sqrt: y = x^2 - a
	*iteration: x(i) = 0.5*( x(i-1) + a/x(i-1) )
	*/
	static inline float sqrtByNewton_1(const float x, const float eps)
	{
		float val = x;
		float last;
		do
		{
			last = val;
			val = (val + x / val) * 0.5f;
		} while (std::fabs(val - last) > eps);
		return val;
	}

	/*sqrt by newton with a proper init value to reduce iteration number*/
	//static inline float sqrtByNewton_2(const float x, const float eps)
	//{
	//	int temp = (((*(int*)&x) & 0xff7fffff) >> 1 + (64 << 23));
	//	float val = *(float*)&temp;
	//	float last;
	//	do
	//	{
	//		last = val;
	//		val = (val + x / val) * 0.5f;
	//	} while (std::fabs(last - val) > eps);
	//	return val;
	//}

	/*Carmack method is no faster than std::sqrt*/
	static inline float sqrtByCarmack(const float x)
	{
		int i;
		float x2 = x * 0.5f, y = x;
		const float threehalfs = 1.5f;
		i = *(int*)&y;
		i = 0x5f375a86 - (i >> 1);
		y = *(float*)&i;
		y = y * (threehalfs - (x2 * y * y));
		y = y * (threehalfs - (x2 * y * y));
		y = y * (threehalfs - (x2 * y * y));
		return x * y;
	}

	//static float sqrtBySSE(const float x)
	//{
	//	float b = x;
	//	__m128 in = _mm_load_ss(&b);
	//	__m128 out = _mm_rsqrt_ss(in);
	//	_mm_store_ss(&b, out);
	//	return x*b;
	//}

	/*SSE optimized vector dot product*/
	//static float vectMultiplyTest(const float* arr_1,
	//	const float* arr_2,
	//	const int num)
	//{
	//	__m128* src_1 = (__m128*)arr_1;
	//	__m128* src_2 = (__m128*)arr_2;
	//	__m128 m1 = _mm_setzero_ps();

	//	const uint num_p = num >> 2;
	//	for (uint i = 0; i < num_p; ++i, ++src_1, ++src_2)
	//	{
	//		m1 = _mm_add_ps(m1, _mm_mul_ps(*src_1, *src_2));
	//	}

	//	if (num % 4 == 1)
	//	{
	//		__m128 t1 = _mm_load_ss((float*)src_1);//load scalar
	//		__m128 t2 = _mm_load_ss((float*)src_2);
	//		m1 = _mm_add_ps(m1, _mm_mul_ps(t1, t2));
	//	}
	//	else if (num % 4 == 2)
	//	{
	//		__m128 z = _mm_setzero_ps();
	//		__m128 t1 = _mm_shuffle_ps(*src_1, z, _MM_SHUFFLE(0, 0, 1, 0));//set: 0, 1, 2, 3 -> 0, 1, 0, 0
	//		__m128 t2 = _mm_shuffle_ps(*src_2, z, _MM_SHUFFLE(0, 0, 1, 0));
	//		m1 = _mm_add_ps(m1, _mm_mul_ps(t1, t2));
	//	}
	//	else if (num % 4 == 3)
	//	{
	//		__m128 t1 = *src_1;
	//		t1.m128_f32[3] = 0;//set highest order float to 0
	//		__m128 t2 = *src_2;
	//		t2.m128_f32[3] = 0;
	//		m1 = _mm_add_ps(m1, _mm_mul_ps(t1, t2));
	//	}

	//	return m1.m128_f32[0] + m1.m128_f32[1] + m1.m128_f32[2] + m1.m128_f32[3];
	//}

	/*vector dot product using SSE*/
	//static inline float vectMultiplySSE(const float* arr_1,
	//	const float* arr_2,
	//	const int num)
	//{
	//	float ret = 0.0f;
	//	size_t i = 0;
	//	const size_t block_width = 4;
	//	const size_t block_num = num / block_width;
	//	const size_t remain_num = num % block_width;
	//	__m128 m_sum = _mm_setzero_ps();
	//	__m128 m_load_1, m_load_2;

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load_1 = _mm_load_ps(arr_1);
	//		m_load_2 = _mm_load_ps(arr_2);
	//		m_sum = _mm_add_ps(m_sum, _mm_mul_ps(m_load_1, m_load_2));
	//		arr_1 += block_width;
	//		arr_2 += block_width;
	//	}

	//	/*merge packed result*/
	//	const float* ptr_tmp = (const float*)&m_sum;
	//	ret = ptr_tmp[0] + ptr_tmp[1] + ptr_tmp[2] + ptr_tmp[3];

	//	/*processing remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		ret += arr_1[i] * arr_2[i];
	//	}

	//	return ret;
	//}

	/*array multiply using SSE unpacked*/
	//static inline void arrEleMulSSEUnpack(const float* arr_1,
	//	const float* arr_2,
	//	const int num,
	//	float* arr_3)
	//{//'u':16 bytes alignment is not requirement
	//	for (uint i = 0; i < num; i += 4)
	//	{
	//		__m128 m1 = _mm_loadu_ps(arr_1 + i);
	//		__m128 m2 = _mm_loadu_ps(arr_2 + i);
	//		_mm_storeu_ps(arr_3 + i, _mm_mul_ps(m1, m2));
	//	}
	//}

	/*array element multiply using SSE packed*/
	//static inline void vectDotProductSSE(const float* arr_1,
	//	const float* arr_2,
	//	const int num,
	//	float* arr_3)
	//{
	//	size_t i = 0;
	//	const size_t block_width = 4;
	//	const size_t block_num = num / block_width;
	//	const size_t remain_num = num % block_width;
	//	__m128 m_sum = _mm_setzero_ps();
	//	__m128 m_load_1, m_load_2;

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load_1 = _mm_load_ps(arr_1);
	//		m_load_2 = _mm_load_ps(arr_2);
	//		_mm_store_ps(arr_3, _mm_mul_ps(m_load_1, m_load_2));
	//		arr_1 += block_width;
	//		arr_2 += block_width;
	//		arr_3 += block_width;
	//	}

	//	/*process remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		arr_3[i] = arr_1[i] * arr_2[i];
	//	}
	//}

	/*scale a vector SSE*/
	//static inline void scaleVector(const float* in,
	//	const size_t num,
	//	const float scale,
	//	float* out)
	//{
	//	size_t i = 0;
	//	const size_t block_width = 4;
	//	const size_t block_num = num / block_width;
	//	const size_t remain_num = num % block_width;
	//	const __m128 m_scale = _mm_set1_ps(scale);//set scale
	//	__m128 m_load;

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load = _mm_load_ps(in);
	//		_mm_store_ps(out, _mm_mul_ps(m_load, m_scale));
	//		in += block_width;
	//		out += block_width;
	//	}

	//	/*process remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		out[i] = in[i] * scale;
	//	}
	//}

	/*sum float array using SSE*/
	//static inline float sumFloatArrSSE(const float* buff, const size_t num)
	//{
	//	float sum = 0.0f;
	//	size_t i = 0;//idx
	//	const size_t block_width = 4;//process 4 float a time
	//	const size_t block_num = num / block_width;
	//	const size_t remain_num = num % block_width;
	//	__m128 m_sum = _mm_setzero_ps();//SSE sum init
	//	__m128 m_load;//data loaded into register

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load = _mm_load_ps(buff);
	//		m_sum = _mm_add_ps(m_sum, m_load);
	//		buff += block_width;
	//	}

	//	/*merge packed result*/
	//	const float* ptr_tmp = (const float*)&m_sum;
	//	sum = ptr_tmp[0] + ptr_tmp[1] + ptr_tmp[2] + ptr_tmp[3];

	//	/*processing remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		sum += buff[i];
	//	}

	//	return sum;
	//}

	/*sum int array using SSE: this can unfold 4 times or use AVX to handle 8 int a time*/
	//static inline int sumIntArrSSE(const int* buff, const size_t num)
	//{
	//	int sum = 0;
	//	size_t i = 0;
	//	const size_t block_width = 4;//process 4 int a time
	//	const size_t block_num = num / block_width;
	//	const size_t remain_num = num % block_width;
	//	__m128i m_sum = _mm_setzero_si128();
	//	__m128i m_load;

	//	/*SSE batch process*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load = _mm_load_si128((__m128i*)buff);
	//		m_sum = _mm_add_epi32(m_sum, m_load);
	//		buff += block_width;
	//	}

	//	/*merge packed result*/
	//	const int* ptr_tmp = (const int*)&m_sum;
	//	sum = ptr_tmp[0] + ptr_tmp[1] + ptr_tmp[2] + ptr_tmp[3];

	//	/*processing remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		sum += buff[i];
	//	}

	//	return sum;
	//}

	/*compare 4 INTs and count*/
	//inline static uint comp4IntAndCount(const int* arr_1, const int* arr_2)
	//{//_mm_cmpgt_epi32
	//	__m128i m_load_1 = _mm_load_si128((__m128i*)arr_1);
	//	__m128i m_load_2 = _mm_load_si128((__m128i*)arr_2);
	//	__m128i mask = _mm_cmpgt_epi32(m_load_1, m_load_2);
	//	uint* ptr_val = (uint*)&mask;
	//	return ptr_val[0] + ptr_val[1] + ptr_val[2] + ptr_val[3];
	//}//?

	/*sum array using SSE unfold*/
	//static inline float sumArrSSEUnfold(const float* buff, size_t num)
	//{
	//	float sum = 0.0;
	//	size_t i = 0;//idx
	//	const size_t block_width = 16;//process 4*4 float a time: num must > 16?
	//	const size_t block_num = num / block_width;
	//	const size_t remain_num = num % block_width;
	//	__m128 m_sum_1 = _mm_setzero_ps();
	//	__m128 m_sum_2 = _mm_setzero_ps();
	//	__m128 m_sum_3 = _mm_setzero_ps();
	//	__m128 m_sum_4 = _mm_setzero_ps();
	//	__m128 m_load_1, m_load_2, m_load_3, m_load_4;

	//	/*SSE batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		/*load into register*/
	//		m_load_1 = _mm_load_ps(buff);
	//		m_load_2 = _mm_load_ps(buff + 4);
	//		m_load_3 = _mm_load_ps(buff + 8);
	//		m_load_4 = _mm_load_ps(buff + 12);

	//		/*get sum*/
	//		m_sum_1 = _mm_add_ps(m_sum_1, m_load_1);
	//		m_sum_2 = _mm_add_ps(m_sum_2, m_load_2);
	//		m_sum_3 = _mm_add_ps(m_sum_3, m_load_3);
	//		m_sum_4 = _mm_add_ps(m_sum_4, m_load_4);

	//		buff += block_width;
	//	}

	//	/*merge packed result*/
	//	m_sum_1 = _mm_add_ps(m_sum_1, m_sum_2);
	//	m_sum_3 = _mm_add_ps(m_sum_3, m_sum_4);
	//	m_sum_1 = _mm_add_ps(m_sum_1, m_sum_3);
	//	const float* ptr_tmp = (const float*)&m_sum_1;
	//	sum = ptr_tmp[0] + ptr_tmp[1] + ptr_tmp[2] + ptr_tmp[3];

	//	/*processing remain data*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		sum += buff[i];
	//	}

	//	return sum;
	//}

	/*get array sum using AVX*/
	//static inline float sumArrAVX(const float* buff, size_t num)
	//{
	//	float sum = 0.0f;
	//	size_t i = 0;
	//	const size_t block_width = 8;
	//	const size_t block_num = num / block_width;
	//	const size_t remain_num = num % block_width;
	//	__m256 m_sum = _mm256_setzero_ps();
	//	__m256 m_load;

	//	/*AVX batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		m_load = _mm256_load_ps(buff);
	//		m_sum = _mm256_add_ps(m_sum, m_load);
	//		buff += block_width;
	//	}

	//	/*merge packed result*/
	//	const float* ptr_tmp = (const float*)&m_sum;
	//	sum = ptr_tmp[0] + ptr_tmp[1] + ptr_tmp[2] + ptr_tmp[3]
	//		+ ptr_tmp[4] + ptr_tmp[5] + ptr_tmp[6] + ptr_tmp[7];

	//	/*processing remain*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		sum += buff[i];
	//	}

	//	return sum;
	//}

	/*get array sum using AVX unfold to reduce for loop judgment*/
	//static inline float sumArrAVXUnfold(const float* buff, size_t num)
	//{
	//	float sum = 0.0f;
	//	size_t i = 0;
	//	const size_t block_width = 32;//: 8*4, handle 8 float each time 4 times unfold
	//	const size_t block_num = num >> 5;// / 32
	//	const size_t remain_num = num % block_width;

	//	__m256 m_sum_1 = _mm256_setzero_ps();
	//	__m256 m_sum_2 = _mm256_setzero_ps();
	//	__m256 m_sum_3 = _mm256_setzero_ps();
	//	__m256 m_sum_4 = _mm256_setzero_ps();
	//	__m256 m_load_1, m_load_2, m_load_3, m_load_4;

	//	/*AVX batch processing*/
	//	for (; i < block_num; ++i)
	//	{
	//		/*load into register*/
	//		m_load_1 = _mm256_load_ps(buff);
	//		m_load_2 = _mm256_load_ps(buff + 8);
	//		m_load_3 = _mm256_load_ps(buff + 16);
	//		m_load_4 = _mm256_load_ps(buff + 24);

	//		/*get sum*/
	//		m_sum_1 = _mm256_add_ps(m_sum_1, m_load_1);
	//		m_sum_2 = _mm256_add_ps(m_sum_2, m_load_2);
	//		m_sum_3 = _mm256_add_ps(m_sum_3, m_load_3);
	//		m_sum_4 = _mm256_add_ps(m_sum_4, m_load_4);

	//		/*move ptr*/
	//		buff += block_width;
	//	}

	//	/*merge packed result*/
	//	m_sum_1 = _mm256_add_ps(m_sum_1, m_sum_2);
	//	m_sum_3 = _mm256_add_ps(m_sum_3, m_sum_4);
	//	m_sum_1 = _mm256_add_ps(m_sum_1, m_sum_3);
	//	const float* ptr_tmp = (const float*)&m_sum_1;
	//	sum = ptr_tmp[0] + ptr_tmp[1] + ptr_tmp[2] + ptr_tmp[3]
	//		+ ptr_tmp[4] + ptr_tmp[5] + ptr_tmp[6] + ptr_tmp[7];

	//	/*processing remain*/
	//	for (i = 0; i < remain_num; ++i)
	//	{
	//		sum += buff[i];
	//	}

	//	return sum;
	//}

public:
	static const int rBegin;
	static const int rEnd;
	static const int cBegin;
	static const int cEnd;
	static const int rowUp;
	static const int rowDown;
	static const int colLeft;
	static const int colRight;
	static const int m_subPixel_gravity_range;
	static const float secoond_first_ratio;

	static const std::vector<int> grids_x;
	static const std::vector<int> grids_y;

	static int kernel[9];

	/*parameters of Good Point Selector*/
	static const float bg_ratio;
	static const float th_intensity;
	static const float th_focus;
	static const float th_centrality;
};

#endif
