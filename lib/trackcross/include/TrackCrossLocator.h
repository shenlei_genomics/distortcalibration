#ifndef TRACK_CROSS_LOCATOR
#define TRACK_CROSS_LOCATOR

#include "Utils.h"
/*abstract base class*/
class TrackCrossLocator
{
public:
	TrackCrossLocator(const cv::Mat& img,
		const int cycle, 
		const int channel, 
		const int fov_row, 
		const int fov_col);
	TrackCrossLocator(const cv::Mat& img,
		const int channel,
		const int fov_row, 
		const int fov_col);
	TrackCrossLocator(const cv::Mat& img);
	TrackCrossLocator();
	virtual ~TrackCrossLocator();

	int init(const cv::Mat& img);

	/*init row and col offset*/
	virtual int initTrackOffsets(const std::vector<int>& dnb_numsX,
		const std::vector<int>& dnb_numsY) = 0;

	/*input image preprocessing*/
	virtual int preprocess() = 0;

	/*locate track crosses*/
	virtual int locateTrackCrossInPixel(std::vector<cv::Point>& pts_pixel) = 0;
	virtual void locateTrackCrossSubpixel(const std::vector<cv::Point>& pts_pixel) = 0;

	/*get margins*/
	int getMargins();
	int getMargins(const float& pixel_per_dnb);

	/*get grid size*/
	int getGridSize(const std::vector<int>& grids_x, const std::vector<int>& grids_y,
		const std::vector<cv::Point2f>& vertices, cv::Point& grid);
	//according to input pixel_per_dnb
	int getGridSize(const float& pixel_per_dnb, 
		const std::vector<int>& grids_x, const std::vector<int>& grids_y,
		const std::vector<cv::Point2f>& vertices, cv::Point& grid);
	int getGridSize(const std::vector<int>& grids_x, const std::vector<int>& grids_y,
		const std::vector<cv::Point2f>& vertices, int& grid_x, int& grid_y);

	/*get margin grid size*/
	int getMarginBlockGridSize(const std::vector<int>& grids_x,
		const std::vector<int>& grids_y, cv::Point& leftup, cv::Point& rightdown);
	//according to input pixel_per_dnb
	int getMarginBlockGridSize(const float& pixel_per_dnb, const std::vector<int>& grids_x,
		const std::vector<int>& grids_y, cv::Point& leftup, cv::Point& rightdown);

	/*extend tracks crosses*/
	int extendTracks(const std::vector<int>& grid_size_width,
		const std::vector<int>& grid_size_height);

	/*extend track crosses for V0.1(build standard template per cycle)*/
	int extendTrackCross(const std::vector<int>& grids_x,
		const std::vector<int>& grids_y);
	//extend arbitrary template tracks according to margins
	int extendTrackCross(const float& pixel_per_dnb,
		const std::vector<int>& grids_x, const std::vector<int>& grids_y);

	/*calculate the integral image*/
	inline void initIntegral()
	{
		//if (m_img.type() != CV_32F)
		m_img.convertTo(m_img, CV_32F);
		cv::integral(m_img, m_integ, CV_32F);
	}

	/*set processing image*/
	inline void setImage(const cv::Mat& img)
	{
		this->m_img = img;
	}

	/*get img*/
	inline cv::Mat& getImage()
	{
		return this->m_img;
	}

	/*get integral image*/
	inline cv::Mat& getInteg()
	{
		return this->m_integ;
	}

protected:
	int m_cycle;
	int m_channel;
	int m_fov_row;
	int m_fov_col;

	cv::Mat m_img;//input image allows preprocessing

	/*integral image*/
	cv::Mat m_integ;

	//cv::Mat m_img_origin;//input image not processed
	int m_track_num_y;//row track line number
	int m_track_num_x;//col track line number
	std::vector<int> m_margins;//margin block DNB numbers
	std::vector<cv::Point2f> m_tc_pts;//track crosses' coordinates
	std::vector<cv::Point2f> m_inner_Track_cross_pts;//inner 81 track crosses' coordinates
	std::vector<cv::Point> m_grid_sizes;//height and width block DNB number

	friend class FOV;
	friend class Utils;
	friend class CoordExtractor;
	
};

#endif
