#ifndef GOOD_POINT_SELECTOR
#define GOOD_POINT_SELECTOR

#include "Utils.h"

class GoodPointSelector
{
public:
	GoodPointSelector();
	virtual ~GoodPointSelector();

	inline void init(const float& bg_ratio,
		const float& th_focus,
		const float& th_centrality)
	{
		this->m_bg_ratio = bg_ratio;
		this->m_th_focus = th_focus;
		this->m_th_centrality = th_centrality;
	}

	struct PARAM
	{
		float intensPercentTh = 0.8f;
		float focusScorePercentTh = 0.9f;
		float centroidLowTh = -0.1f;
		float centroidUpTh = 0.1f;
	} param;

	void nonMaximaSuppression(const cv::Mat& src, const int sz, cv::Mat& dst, const cv::Mat mask);

	/*@even: find peak points: cab be optimized by image scan way?*/
	inline static void findPeakPoints(const cv::Mat& img, std::vector<cv::Point>& peak_pts)
	{//presume: img.type() == CV_32F and input image's not empty.
		peak_pts.clear();
		const uint Y_UP_LIMIT = img.rows - 1
			, X_UP_LIMIT = img.cols - 1;
		for (uint y = 1; y < Y_UP_LIMIT; ++y)
		{
			uint x = 1;
			while (x < X_UP_LIMIT)
			{
				if (Utils::judgePeakPoint(img, cv::Point(x, y)))
				{
					peak_pts.push_back(cv::Point(x, y));
					x += 2;
				}
				else
					x += 1;
			}
		}
	}

	/*@even: find peak points: use back ground as threshold for coarse peak points*/
	inline static void findPeakPoints(const cv::Mat& img, const float& background, std::vector<cv::Point>& peak_pts)
	{//presume: img.type() == CV_32F and input image's not empty.
		peak_pts.clear();
		const uint Y_UP_LIMIT = img.rows - 1
			, X_UP_LIMIT = img.cols - 1;
		for (uint y = 1; y < Y_UP_LIMIT; ++y)
		{
			uint x = 1;
			while (x < X_UP_LIMIT)
			{
				if (Utils::judgePeakPoint(img, cv::Point(x, y), background))
				{
					peak_pts.push_back(cv::Point(x, y));
					x += 2;
				}
				else
					x += 1;
			}
		}
	}

	/*@even: find peak points: use background*/
	inline static void findPeakPtsVals(const cv::Mat& img, const float& background,
		std::vector<cv::Point>& peak_pts, std::vector<float>& peak_vals)
	{//presume: img.type() == CV_32F and input image's not empty.
		peak_pts.clear();
		peak_vals.clear();
		const uint Y_UP = img.rows - 1,
			X_UP = img.cols - 1;
		for (uint y = 1; y < Y_UP; ++y)
		{
			uint x = 1;
			while (x < X_UP)
			{
				if (Utils::judgePeakPoint(img, cv::Point(x, y), background))
				{
					peak_pts.push_back(cv::Point(x, y));
					peak_vals.push_back(img.at<float>(y, x));
					x += 2;
				}
				else
					x += 1;
			}
		}
	}

	/*@even: find peak points: use background*/
	inline static void findPeakPtsValsROI(const cv::Mat& img,
		const std::pair<cv::Point, cv::Point>& roi, const float& background,
		std::vector<cv::Point>& peak_pts, std::vector<float>& peak_vals)
	{//presume: img.type() == CV_32F and input image's not empty.
		peak_pts.clear();
		peak_vals.clear();
		const int Y_LOW = roi.first.y < 0 ? 1 : roi.first.y,
			X_LOW = roi.first.x < 0 ? 1 : roi.first.x,
			Y_UP = roi.second.y >= img.rows ? img.rows - 2 : roi.second.y,
			X_UP = roi.second.x >= img.cols ? img.cols - 2 : roi.second.x;
		uint y = Y_LOW;
		while (y <= Y_UP)
		{
			bool is_y_good = false;
			uint x = X_LOW;
			while (x <= X_UP)
			{
				if (Utils::judgePeakPoint(img, x, y, background))
				{
					is_y_good = true;
					peak_pts.push_back(cv::Point(x, y));
					peak_vals.push_back(img.at<float>(y, x));
					x += 2;
				}
				else x += 1;
			}
			if (is_y_good) y += 2;
			else y += 1;
		}
	}

	/*@even: add preprocess to accelerate filtering*/
	inline static void getImgBackground(const cv::Mat& img, float& background)
	{
		//printf("zero count: %d\n", cv::countNonZero(img));
		const uint COUNT_LIMIT = 5;
		const uint Y_LOW_LIMIT = 500, X_LOW_LIMIT = 500,
			Y_UP_LIMIT = img.rows - 500, X_UP_LIMIT = img.cols - 500;

		uint count = 0;
		float val, vals_sum = 0.0f;
		for (uint y = Y_LOW_LIMIT; y < Y_UP_LIMIT; ++y)
		{
			uint x = X_LOW_LIMIT;
			while (x < X_UP_LIMIT)
			{
				if (Utils::judgePeakPoint(img, cv::Point(x, y)))
				{
					if (count < COUNT_LIMIT)
					{
						val = Utils::getPeakPtBackground(img, cv::Point(x, y));
						if (val)
						{
							vals_sum += val;
							++count;
						}
						x += 2;
					}
					else break;
				}
				else x += 1;
			}
			if (x < X_UP_LIMIT) break;
		}
		if (count)
			background = vals_sum / count;
		else background = 0;
	}

	/*@even: find peak points: cab be optimized by image scan way?*/
	inline static void findPeakPtsVals(const cv::Mat& img,
		std::vector<cv::Point>& peak_pts, std::vector<float>& peak_vals)
	{//presume: img.type() == CV_32F and input image's not empty.
		peak_pts.clear();
		peak_vals.clear();
		const uint y_up_limit = img.rows - 1
			, x_up_limit = img.cols - 1;
		for (uint y = 1; y < y_up_limit; ++y)
		{
			uint x = 1;
			while (x < x_up_limit)
			{
				if (Utils::judgePeakPoint(img, cv::Point(x, y)))
				{
					peak_pts.push_back(cv::Point(x, y));
					peak_vals.push_back(img.at<float>(y, x));
					x += 2;
				}
				else
					x += 1;
			}
		}
	}

	/*@even: filter peak points by intensity*/
	inline static void filterPeakPtsByIntensity(const cv::Mat& img,
		const std::vector<cv::Point>& pts_in, const float& ratio, std::vector<cv::Point>& pts_out)
	{
		PtIntensityComparator comprator;
		comprator.m_img = img;
		auto min_max = std::minmax_element(pts_in.begin(), pts_in.end(), comprator);//min_max: a pair of points
		const float THRESHOLD = img.at<float>((*min_max.first).y, (*min_max.first).x)
			+ (img.at<float>((*min_max.second).y, (*min_max.second).x)
			- img.at<float>((*min_max.first).y, (*min_max.first).x)) * ratio;
		pts_out.clear();
		const uint SIZE = (uint)pts_in.size();
		for (uint i = 0; i < SIZE; ++i)
		{
			if (img.at<float>(pts_in[i].y, pts_in[i].x) > THRESHOLD)
				pts_out.push_back(pts_in[i]);
		}
	}

	/*@even: filter peak points by intensity*/
	inline static void filterByIntensity(const cv::Mat& img, const float& ratio,
		const std::vector<cv::Point>& peak_pts, std::vector<float>& peak_vals, std::vector<cv::Point>& pts_out)
	{
		ComparatorL2S<float> comparator;
		const uint nth = uint((1.0f - ratio) * peak_pts.size());
		std::nth_element(peak_vals.begin(), peak_vals.begin() + nth, peak_vals.end(), comparator);
		const float THRESHOLD = peak_vals[nth];
		pts_out.clear();
		const uint SIZE = (uint)peak_pts.size();
		for (uint i = 0; i < SIZE; ++i)
		{
			if (img.at<float>(peak_pts[i].y, peak_pts[i].x) > THRESHOLD)
				pts_out.push_back(peak_pts[i]);
		}
	}

	/*@even: filter peak points by focus*/
	inline static void filterByFocus(const cv::Mat& img,
		const float& ratio, const std::vector<cv::Point>& pts_in, std::vector<cv::Point>& pts_out)
	{
		pts_out.clear();
		const uint SIZE = (uint)pts_in.size();
		for (uint i = 0; i < SIZE; ++i)
		{
			if (Utils::judgeFocus(img, pts_in[i], ratio))
			{
				pts_out.push_back(pts_in[i]);
			}
		}
	}

	/*@even: filter peak points by centrality*/
	inline static void filterByCentrality(const cv::Mat& img, const std::vector<cv::Point>& pts_in,
		const float& THRESHOLD, std::vector<cv::Point>& pts_out)
	{
		pts_out.clear();
		const uint SIZE = (uint)pts_in.size();
		for (uint i = 0; i < SIZE; ++i)
		{
			if (Utils::judgeCentralityX(img, pts_in[i], THRESHOLD)
				&& Utils::judgeCentralityY(img, pts_in[i], THRESHOLD))
			{
				pts_out.push_back(pts_in[i]);
			}
		}
	}

	/*calculate good point centroid*/
	inline static void getCentroid(const cv::Mat& img,
		const std::vector<cv::Point>& pts_in, std::vector<cv::Point2f>& pts_out)
	{
		const uint SIZE = (uint)pts_in.size();
		pts_out.resize(SIZE);
		for (uint i = 0; i < SIZE; ++i)
		{
			Utils::getDNBGravity(img, pts_in[i], pts_out[i]);
			//Utils::getGoodPtCentroid(img, pts_in[i], pts_out[i]);
		}
	}

	/*@even: find good points wrapper*/
	inline void findGoodPoints(const cv::Mat& img, std::vector<cv::Point>& good_pts)
	{//presume: img.type() == CV32FC1
		float background;
		GoodPointSelector::getImgBackground(img, background);
		background *= m_bg_ratio;
		std::vector<float> peak_vals;
		std::vector<cv::Point> peak_pts, pts_intensity, pts_focus;
		GoodPointSelector::findPeakPtsVals(img, background, peak_pts, peak_vals);
		if (peak_pts.size() == 0)
			return;
		GoodPointSelector::filterByFocus(img,
			m_th_focus, peak_pts, pts_focus);
		GoodPointSelector::filterByCentrality(img, pts_focus, m_th_centrality, good_pts);
	}

	/*@even: find good points wrapper*/
	inline static void findGoodPtsROI(const cv::Mat& img,
		const std::pair<cv::Point, cv::Point>& roi,
		std::vector<cv::Point2f>& good_pts)
	{//presume: img.type() == CV32FC1
		float background;
		GoodPointSelector::getImgBackground(img, background);
		background *= Utils::bg_ratio;
		std::vector<float> peak_vals;
		std::vector<cv::Point> peak_pts, pts_intensity, pts_focus, pts_centrality;
		GoodPointSelector::findPeakPtsValsROI(img, roi, background, peak_pts, peak_vals);
		if (peak_pts.size() == 0)
		{
			printf("[Error]: peak points are empty.\n");
			return;
		}
		GoodPointSelector::filterByIntensity(img,
			Utils::th_intensity, peak_pts, peak_vals, pts_intensity);
		GoodPointSelector::filterByFocus(img, Utils::th_focus, pts_intensity, pts_focus);
		GoodPointSelector::filterByCentrality(img,
			pts_focus, Utils::th_centrality, pts_centrality);
		GoodPointSelector::getCentroid(img, pts_centrality, good_pts);
	}

	float getThreshold(const cv::Mat& src, const int thIdx);

	void findGoodPointsSL(const std::string& imgFn, const std::string& gpMaskFn, const PARAM& param);

	//int matchGoodPoints(const std::string& gpMaskFn, const std::string& trackCrossFn,
	//	const std::string& gpOutputFn, const std::string& gpTemplFn);

	/*test...*/
	//inline void test(const std::string& in_path, const std::string& out_path)
	//{
	//	findGoodPointsSL(in_path, out_path, param);
	//	matchGoodPoints("./gpMask_S001_C001R034_A_196.tif", "./C001R034TrackCross0_S001.txt", "./goodPoints_S001_C001R034_A_196.txt", "./goodPointsOnTemplate_S001_C001R034_A_196.txt");
	//	printf("Good point tested done.\n");
	//}

	private:
		float m_bg_ratio;
		float m_th_intensity;
		float m_th_focus;
		float m_th_centrality;
};

#endif
