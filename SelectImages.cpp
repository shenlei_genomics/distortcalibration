#pragma once
#include "SelectImages.h"
#include "SelectGoodPoints.h"
#include "BlockDC.h"
#include "BlockForAddress.h"
#include <limits>
#include <iostream>
#include "DistCal.h"
//#define _DEBUG

using namespace cv;
using namespace std;


void Addlist(vector<IMGINFO>& imginfolist, const IMGINFO& info, const size_t& TIF_COUNT)
{
	imginfolist.push_back(info);	
	sort(imginfolist.begin(), imginfolist.end(), [](const IMGINFO& info1, const IMGINFO& info2)->bool {return info1.rms4sel < info2.rms4sel; });
	if (imginfolist.size() < TIF_COUNT + 1)
	{
		return;
	}
	imginfolist.pop_back();
}

int initBlocksForAddress(vector<BlockForAddress> &blocks, const vector<Point2f> &trackcross, const size_t& BLOCKROW, const size_t& BLOCKCOL)
{
	for (size_t row = 0; row < BLOCKROW; row++)
	{
		for (size_t col = 0; col < BLOCKCOL; col++)
		{
			vector<Point2f> vers(4);
			size_t topleftVerIndex = row * (BLOCKCOL + 1) + col;
			//P0-P4  clockwise
			vers[0] = trackcross[topleftVerIndex]; vers[1] = trackcross[topleftVerIndex + 1];
			vers[2] = trackcross[topleftVerIndex + 1 + BLOCKCOL + 1]; vers[3] = trackcross[topleftVerIndex + BLOCKCOL + 1];
			blocks.emplace_back(col, row, vers);
			if (blocks[row*BLOCKCOL + col].initBlockForAddress() < 0)
			{
				std::cout << "[Error:] calculate block info wrong!Block id : " << row*BLOCKCOL + col << std::endl;
				//cerr << "Error:calculate block info wrong! Block id:" << row*BLOCKCOL + col << endl;
				return -1;
			}
		}
	}
	return 0;
}

int ProcessOneBlock(const Mat &goodPointsMask, const Mat(&centroid)[2], const bool if_outside_blk,BlockForAddress &block, IMGINFO& info)
{	
	//cout << if_outside_blk <<"\t";
	if (!goodPointsMask.empty())
	{
		auto MAX_INT = [](const float x, const int y)->int {return max(static_cast<int>(x), y); };
		auto MIN_INT = [](const float x, const int y)->int {return min(static_cast<int>(x), y); };
		const vector<Point2f>& vertices = block.getVertex();
		if (vertices.empty() || vertices.size() < 4)
		{
			std::cout <<  "[Error:]Vertex num is not 4!" <<std::endl;
			//cerr << "Vertex num is not 4!" << endl;
			return -1;
		}
		//topleft, topright, bottom left, bottom right point
		const Point2f &tl = vertices[0], &tr = vertices[1], &bl = vertices[3], &br = vertices[2];
		float block_rms = 0.0f, tmp_rms4sel = 0.0f;
			
		for (int row = MAX_INT(max(tl.y, tr.y) + 4.0f, 0); row < MIN_INT(min(bl.y, br.y) - 2.0f, goodPointsMask.rows); row++)
		{
			const uchar* gpm = goodPointsMask.ptr<uchar>(row);
			for (int col = MAX_INT(max(tl.x, bl.x) + 4.0f, 0); col < MIN_INT(min(tr.x, br.x) - 2.0f, goodPointsMask.cols); col++)
			{
				if (gpm[col] == 255)
				{
					Point2f tmpPoint(static_cast<float>(col), static_cast<float>(row));
					Point2f centroid_offset(centroid[0].at<float>(tmpPoint), centroid[1].at<float>(tmpPoint));
					tmpPoint.x = tmpPoint.x + centroid_offset.x;
					tmpPoint.y = tmpPoint.y + centroid_offset.y;
					Point2f nn,p_tc;
					float rms = 0.0f;
					if (!block.addressOnTempl(tmpPoint, nn, rms))
					{
#ifdef _DEBUG
						
						ofstream fs("./goodpoints_blk.txt", iostream::app);
						if (fs)
						{
							fs << tmpPoint.x << "\t" << tmpPoint.y << "\n";

						}
						fs.close();
						ofstream fs2("./goodpoints_tc.txt", iostream::app);
						if (fs2)
						{
							fs2 << p_tc.x << "\t" << p_tc.y << "\n";

						}
						fs2.close();					

#endif // _DEBUG

						if (if_outside_blk)
						{
							info.points4comp_img.push_back(tmpPoint);
							info.points4comp_templ.push_back(nn);
							info.rms4comp += rms*rms;
						}					
						
						if (rms < RMSTH)
						{
							//p4cal_img.push_back(tmpPoint);
							//p4cal_templ.push_back(nn);							
							info.points4cal_img.push_back(tmpPoint);
							info.points4cal_templ.push_back(nn);
						}

						info.rms4sel += rms;
						++info.n4sel;
					}
				}
			}
		}
		
		/*cv::RNG rng(888);		
		if (p4cal_img.size() > COUNTTH)
		{
			float part = static_cast<float>(COUNTTH) / static_cast<float>(p4cal_img.size());
			for (auto it1 = p4cal_img.cbegin(), it2 = p4cal_templ.cbegin(); it1 != p4cal_img.cend(), it2 != p4cal_templ.cend(); ++it1, ++it2)
			{
				double rn = rng.uniform(0.0f, 1.0f);
				if (rn < part)
				{
					info.points4cal_img.push_back(*it1);
					info.points4cal_templ.push_back(*it2);					
				}				
			}
		}
		else
		{
			for (auto it1 = p4cal_img.cbegin(), it2 = p4cal_templ.cbegin(); it1 != p4cal_img.cend(), it2 != p4cal_templ.cend(); ++it1, ++it2)
			{
				info.points4cal_img.push_back(*it1);
				info.points4cal_templ.push_back(*it2);
			}
		}*/
		return 0;
	}
	else
	{
		return -1;
	}
}



int getImgInfo(const Mat& img, const vector<size_t>& outside_idx, const TCOutput& trackcross, IMGINFO& info)
{	
	Mat goodPointsMask;
	Mat centroid[2];

	const size_t BLOCKROW = BlockForAddress::getDnbnumY().size();
	const size_t BLOCKCOL = BlockForAddress::getDnbnumX().size();
	vector<BlockForAddress> blocks; blocks.reserve(BLOCKROW*BLOCKCOL);

	if (findGoodPoints(img, goodPointsMask, centroid) < 0)
	{
		std::cout <<  "[Error:] Find good points error!\n" << std::endl;
		//cerr << "Find good points error! image:" << imgFn << endl;
		return -1;
	}

	if (initBlocksForAddress(blocks, trackcross.m_tracks_subpixel, BLOCKROW, BLOCKCOL) < 0)
	{
		std::cout << "[Error:] Init Block error!\n" << std::endl;
		//cerr << "Init Block error! Image:" << imgFn << endl;
		return -1;
	}

	//Removed this step - when there are too much good points in one block, then radomly choose part of good points.  
		
	for (size_t block_id = 0; block_id < BLOCKROW*BLOCKCOL; block_id++)
	{
		bool if_outside = false;
		if (find(outside_idx.cbegin(), outside_idx.cend(), block_id) != outside_idx.cend())
		{
			if_outside = true;
		}

		if (ProcessOneBlock(goodPointsMask, centroid, if_outside, blocks[block_id], info) < 0)
		{
			std::cout << "[Error:] Process block " << block_id << " failed!\n" << std::endl;
			//cerr << "Process block " << block_id << " error!" << endl;
			return -1;
		}
	}
	if (info.n4sel == 0)
	{
		info.rms4sel = numeric_limits<float>::max();
	}
	else
	{
		info.rms4sel = (info.rms4sel + 0.0f) / info.n4sel;		
	}
	
	return 0;
}



