#ifndef DIST_CAL_H
#define DIST_CAL_H

#include <sstream>
#include <vector>
#include <fstream>
#include <iostream>
#include <string>
#include "BlockForAddress.h"
#include "TrackDetector.h"

struct IMGINFO
{
	std::string imgFn;
	float rms4sel;//rms for select images which tc is accurate	
	float rms4comp;//rms for compare of Even's TC and calibrated TC
	int n4sel;// goodpoints num for select image

	//points for calibration on img, only use good block's(judged by block's rms) good points
	std::vector<cv::Point2f> points4cal_img;
	std::vector<cv::Point2f> points4cal_templ;

	//points for compare on img, only use outside block for compare.
	std::vector<cv::Point2f> points4comp_img;
	std::vector<cv::Point2f> points4comp_templ;

	IMGINFO(const std::string& imgfn, const float r, const float part_r, const int n) :imgFn(imgfn), rms4sel(r), rms4comp(part_r), n4sel(n) {};
	IMGINFO() :imgFn(""), rms4sel(0.0f), rms4comp(0.0f), n4sel(0) {};
	IMGINFO(const std::string& imgfn, const float r, const float part_r, const int n, const std::vector<cv::Point2f>& pi, const std::vector<cv::Point2f>& pt, \
		const std::vector<cv::Point2f>& pi4c, const std::vector<cv::Point2f>& pt4c) : \
		imgFn(imgfn), rms4sel(r), rms4comp(part_r), n4sel(n), points4cal_img(pi), points4cal_templ(pt), points4comp_img(pi4c), points4comp_templ(pt4c) {};
	~IMGINFO()
	{
		points4cal_img.clear();
		points4cal_templ.clear();
		points4comp_img.clear();
		points4comp_templ.clear();
	}
};

typedef std::vector<IMGINFO> INFOLIST;
class DistCal
{
public:
	static int init(const std::string &strDnbnumX, const std::string &strDnbnumY, const float pitchSize);
	//static int getImgInfolist(const cv::Mat& img, const TCOutput& trackcross);
	//static int calDCMap(::CGI::OSIIP::DCMap& dcmap);
	static int getImgInfolist(const cv::Mat& img, const TCOutput& trackcross, const int camera_index);
	static int calDCMap(const std::string& fn, cv::Mat& cameraMatrix, cv::Mat& distCoeffs, bool& ifuse, float& avg_dev);
	static bool initFlag;
	static void clear();
	static cv::Size imgSize;

private: 	
	static std::vector<INFOLIST> imginfolistVec;
	static std::vector<std::size_t> outsideBlkIndex;	
	static void getOutsideBlkIdx(const std::size_t BLOCKROW, const std::size_t BLOCKCOL);
};

#endif