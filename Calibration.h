#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "BlockForAddress.h"
#include "SelectGoodPoints.h"
#include <opencv2/calib3d/calib3d.hpp>
#include <iostream>
#include "SelectImages.h"
//#include "DistCal.h"

int calReprojErr(const cv::Mat &cameraMatrix, const cv::Mat &distCoeffs, const std::vector<IMGINFO>& imginfos, bool& ifuse);
int runCalibrationOnImgs(const std::vector<IMGINFO>& imginfos, const cv::Size& imgSize, cv::Mat& cameraMatrix, cv::Mat& distCoeffs);
int calDistortedDev(const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, const cv::Size& imgSize, float& avg_dev);
int writeIni(const std::string& fn, const cv::Mat& cameraMatrix, const cv::Mat& distCoeffs, const bool& ifuse, const float avg_dev);

#endif
