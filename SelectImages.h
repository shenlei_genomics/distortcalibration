#ifndef SELECT_IMAGES_H
#define SELECT_IMAGES_H

#include <sstream>
#include <vector>
//#include <io.h>
#include "opencv2/core/core.hpp"
#include <fstream>
#include <iostream>
#include "TrackDetector.h"

#include "DistCal.h"

const float RMSTH = 1.0f;
const unsigned int COUNTTH = 200;
const float BLOCKRMSTH = 0.09f;
const size_t CAL_TIF_COUNT = 5;

void Addlist(std::vector<IMGINFO>& imginfolist, const IMGINFO& info, const std::size_t& TIF_COUNT = 25);
int getImgInfo(const cv::Mat& img, const std::vector<std::size_t>& outside_idx, const TCOutput& trackcross, IMGINFO& info);

#endif