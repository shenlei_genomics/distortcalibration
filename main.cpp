#pragma once
#include "TrackDetector.h"
#include "SelectImages.h"
#include "Calibration.h"
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include "GetConfig.h"
#include <iterator>

using std::vector;
using std::string;
using cv::Point2f;
using cv::Mat;
string chanels[4] = { ".A.",".C.",".G.",".T." };

int getFiles(const std::string& path, const std::string& ext, std::vector<std::string>& files)
{
	long long file_handle = 0;
	struct _finddata_t file_info;
	std::string path_name, ext_name;
	if (strcmp(ext.c_str(), "") != 0)//if not empty
		ext_name = "\\*" + ext;
	else
		ext_name = "\\*";

	if ((file_handle = _findfirst(path_name.assign(path).append(ext_name).c_str(), &file_info)) != -1)
	{
		do
		{
			if (file_info.attrib & _A_SUBDIR)
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					getFiles(path_name.assign(path).append("\\").append(file_info.name), ext, files);
				}
			}
			else
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					files.push_back(path_name.assign(path).append("\\").append(file_info.name));
					//files.push_back(file_info.name);
				}
			}
		} while (_findnext(file_handle, &file_info) == 0);
		_findclose(file_handle);
	}
	return 0;
}

using vec_str = vector<string>;
/** @brief Extract chanel from filename.

@param img_fn        input image filename
@param intscore      output dnb intensity related scores
@param config_fn     intput  setting config filename
*/
int extractChanel(const vector<string>& fns, vec_str (&single_chanel_fns)[4])
{	
	for each (string fn in fns)
	{
		for (size_t i = 0; i < 4; i++)
		{
			if (fn.find(chanels[i],0)!=string::npos)
			{
				single_chanel_fns[i].push_back(fn);
				break;
			}
		}
	}
	return 1;
}

int initParameters(const string& configFn, std::map<std::string, std::string>& config_map, string& strDnbnumX, string& strDnbnumY, float& pitch_size)
{
	if (!ReadConfig(configFn, config_map))
	{
		std::cerr << "Read config file:" << configFn << " error!" << std::endl;
		return -1;
	}
	string strChippitch, strImgscale;
	bool ret = getParameter(config_map, "modparam.basecall.ImageTrackPattern_gridSizeX[]", strDnbnumX) && \
		getParameter(config_map, "modparam.basecall.ImageTrackPattern_gridSizeY[]", strDnbnumY) && \
		getParameter(config_map, "modparam.basecall.chip_pitch", strChippitch) && \
		getParameter(config_map, "modparam.basecall.img_scale", strImgscale);
	if (!ret)
	{
		return -1;
	}	
	pitch_size = atof(strChippitch.c_str()) / atof(strImgscale.c_str());
	return 0;
}

static void help(char* progName)
{
	cout << endl
		<< "This program use images to calculate the distortion coefficients." << endl
		<< "The images under inputImgPath are taken and the outputINI are written to outputINIPath." << endl
		<< "Usage:" << endl
		<< progName << " inputImgPath outputINIPath [settingsFn]" << endl << endl;
}

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		help(argv[0]);
		return -1;
	}	
	const string tifpath = argv[1];
	const string distort_ini_path = argv[2];
	string configFn = "./settingsV8.config";	
	if(argc > 3) configFn = argv[3];
		
	if (_access(configFn.c_str(), 0) == -1)
	{
		cerr << "Input param: basecalling config file not exist!" << configFn << endl;
		return -1;
	}
	//const string outputFn = (*--outputPath.cend() == 0x5C) ? outputPath + "DistortCalibration.ini" : outputPath + "\\DistortCalibration.ini";
	string outputFn;	

	std::map<std::string, std::string> config_map;
	string strDnbnumX, strDnbnumY;
	float pitch_size = 0.0f;
	if (initParameters(configFn, config_map, strDnbnumX, strDnbnumY, pitch_size) < 0)
	{
		std::cerr << "Init parameters from config file: " << configFn << " error!" << std::endl;
		return -1;
	}
	std::vector<std::string> files;
	getFiles(tifpath, ".tif", files);
	if (files.size() > 0)
	{
		cv::Size img_size = cv::imread(files[0], CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH).size();
		DistCal::imgSize = img_size;
		DistCal::init(strDnbnumX, strDnbnumY, pitch_size);
	}

	vector<int> dnbnumX_tc, dnbnumY_tc;
	parseStr(strDnbnumX, dnbnumX_tc);
	parseStr(strDnbnumY, dnbnumY_tc);
	TrackDetector detector(dnbnumX_tc, dnbnumY_tc, pitch_size);
	vec_str single_chanel_fns[4];
	extractChanel(files, single_chanel_fns);
	for (size_t camera_index = 0; camera_index < 4; camera_index++)
	{
		for each (auto file in single_chanel_fns[camera_index])
		{
			cv::Mat image = cv::imread(file, CV_LOAD_IMAGE_ANYCOLOR | CV_LOAD_IMAGE_ANYDEPTH);
			TCOutput trackcross;
			if (detector.detectTracks(image, trackcross, true, 0.27) < 0)
			{
				std::cerr << "calculate trackcross error! Img:" << file << std::endl;
				return -1;
			}
			DistCal::getImgInfolist(image, trackcross, camera_index);
		}					
	}
	cv::Mat cameraMatrix, distCoeffs;
	bool ifuse = false;
	float avg_dev = -10.f;
	DistCal::calDCMap(distort_ini_path, cameraMatrix, distCoeffs, ifuse, avg_dev);
}